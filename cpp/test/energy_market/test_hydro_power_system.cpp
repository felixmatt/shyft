
#include <doctest/doctest.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/power_station.h>
#include <shyft/energy_market/hydro_power/water_route.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>

#include <serialize_loop.h>
#include <shyft/core/core_archive.h>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>

using namespace shyft::energy_market::hydro_power;
using test::serialize_loop;
namespace test_serial {
    
    namespace v0 {
        struct agg { // original version
            int id;
            string name;
            agg()=default;
            agg(int id,string name):id(id),name(name){}
            bool operator==(agg const&o) const {return id==o.id && name==o.name;}
            bool operator!=(agg const&o) const {return !operator==(o);}
            x_serialize_decl();
        };
        
        struct plant {
            int id;
            string name;
            vector<shared_ptr<agg>> aggs;
            bool operator==(plant const&o) const {return id==o.id && name==o.name && aggs==o.aggs;}
            bool operator!=(plant const&o) const {return !operator==(o);}

            x_serialize_decl();
        };
    }
    
    inline namespace v1 {
        struct unit {// new version, same layout
            int id;
            string name;
            bool operator==(unit const&o) const {return id==o.id && name==o.name;}
            bool operator!=(unit const&o) const {return !operator==(o);}

            x_serialize_decl();
        };
        
        struct plant {
            int id;
            string name;
            vector<shared_ptr<unit>> units;
            bool operator==(plant const&o) const {return id==o.id && name==o.name && units==o.units;}
            bool operator!=(plant const&o) const {return !operator==(o);}

            x_serialize_decl();
        };
    }
    

}
x_serialize_export_key(test_serial::v0::agg);
x_serialize_export_key(test_serial::v1::unit);
x_serialize_export_key(test_serial::v0::plant);
x_serialize_export_key(test_serial::v1::plant);
BOOST_CLASS_VERSION(test_serial::v1::unit, 1);
BOOST_CLASS_VERSION(test_serial::v1::plant, 1);

using boost::serialization::make_nvp;

template<class Archive>
void test_serial::v1::unit::serialize(Archive & ar, const unsigned int version) {
	ar 
	& make_nvp("id",id)
    & make_nvp("name",name)
    ;
}

template<class Archive>
void test_serial::v1::plant::serialize(Archive & ar, const unsigned int version) {
	ar 
	& make_nvp("id",id)
    & make_nvp("name",name)
    & make_nvp("units",units)
    ;
}


template<class Archive>
void test_serial::v0::agg::serialize(Archive & ar, const unsigned int version) {
	ar 
	& make_nvp("id",id)
    & make_nvp("name",name)
    ;
}

template<class Archive>
void test_serial::v0::plant::serialize(Archive & ar, const unsigned int version) {
	ar 
	& make_nvp("id",id)
    & make_nvp("name",name)
    & make_nvp("aggs",aggs)
    ;
}


#define xxx_arch(T) x_serialize_archive(T,boost::archive::binary_oarchive,boost::archive::binary_iarchive)
xxx_arch(test_serial::v0::agg);
xxx_arch(test_serial::v1::unit);
xxx_arch(test_serial::v1::plant);
xxx_arch(test_serial::v0::plant);

namespace test {
template <class T,class R>
static R serialize_loop2(const T& o) {
    using namespace std;
    ostringstream xmls;
	/* scope this, ensure archive is flushed/destroyd */{
        boost::archive::binary_oarchive oa(xmls);
        oa << BOOST_SERIALIZATION_NVP(o); 
	}
    xmls.flush();
    string ss = xmls.str();
   
    R o2;
	istringstream xmli(ss);
	{
		boost::archive::binary_iarchive ia(xmli);
	    ia >> BOOST_SERIALIZATION_NVP(o2);
	}
    return o2;
}
}
using std::make_shared;
using std::vector;
using std::string;
using std::shared_ptr;

TEST_SUITE("serialize_version") {
    TEST_CASE("serialize_read_old_version") {
        test_serial::v0::agg a{1,"a1"};
        auto u=test::serialize_loop2<test_serial::v0::agg,test_serial::v1::unit>(a);
        FAST_CHECK_EQ(a.id,u.id);
        FAST_CHECK_EQ(a.name,u.name);
    }
    TEST_CASE("serialize_read_old_plant_version") {
        test_serial::v0::plant p0{1,"a1"};
        p0.aggs.push_back(make_shared<test_serial::v0::agg>(1,string("a")));
        auto p1=test::serialize_loop2<test_serial::v0::plant,test_serial::v1::plant>(p0);
        FAST_CHECK_EQ(p0.id,p1.id);
        FAST_CHECK_EQ(p0.name,p1.name);
        FAST_CHECK_EQ(p1.units.size(),1);
        FAST_CHECK_EQ(p1.units[0]->id,1);
        FAST_CHECK_EQ(p1.units[0]->name,"a");
        
    }
}

TEST_SUITE("hps") {

TEST_CASE("aggregate basics") {
    using namespace shyft::energy_market::hydro_power;
    // note have to .. shared, because we are using
    // shared_from this, and that requires a heap alloc block
    auto p0 = make_shared<unit>();
    auto p0c= make_shared<unit>(0,"","",nullptr);
    CHECK(*p0 == *p0c);
    p0c->name = "abc";
    CHECK(*p0 != *p0c);

    auto p1 = make_shared<unit>(1,"kvilldal", "xx", nullptr);

    auto p1_s = serialize_loop(p1);
    CHECK(*p1 == *p1_s);

}
TEST_CASE("power_station basics") {
    using namespace shyft::energy_market::hydro_power;
    // note have to .. shared, because we are using
    // shared_from this, and that requires a heap alloc block
    auto p0 = make_shared<power_plant>();
    auto p0c= make_shared<power_plant>(0,"","",nullptr);
    CHECK(*p0 == *p0c);
    p0c->id = 123;
    CHECK(*p0 != *p0c);
    p0c->id=p0->id;
    CHECK(*p0 == *p0c);
    auto a1 = make_shared<unit>(1,"kvilldal", "G1", nullptr);
    auto a2 = make_shared<unit>(1,"kvilldal", "G1", nullptr);

        power_plant::add_unit(p0,a1);
    CHECK(a1->pwr_station_()==p0);
    CHECK(p0->units.size()==1);
    
    CHECK(*p0 != *p0c);
    
        power_plant::add_unit(p0c,a2);
    CHECK(*p0 == *p0c);
    a1->id=123;
    CHECK(*p0 != *p0c);
    p0->remove_unit(a1);
    CHECK(p0->units.size()==0);
    CHECK(a1->pwr_station_()==nullptr);
    auto p1 = make_shared<power_plant>(1,"kvilldal", "xx", nullptr);

    auto p1_s = serialize_loop(p1);
    CHECK(*p1 == *p1_s);

}

TEST_CASE("reservoir basics") {
    using namespace shyft::energy_market::hydro_power;
    auto r0 = make_shared<reservoir>();
    auto r0c = make_shared<reservoir>(0,"", "", nullptr);
    CHECK(*r0 == *r0c);
    r0c->json = "somestuff";
    CHECK(*r0 != *r0c);
    auto r1 = make_shared<reservoir>(1,"bl�sj�", "reservoir_data(1000, 1100, 1400.0)", nullptr);
    auto r1_s = serialize_loop(r1);
    CHECK(*r1 == *r1_s);
    r1_s->json = "0.3";
    CHECK(*r1 != *r1_s);
}

TEST_CASE("water_route basics") {
    using namespace shyft::energy_market::hydro_power;
    auto w0 = make_shared<waterway>();
    auto w0c = make_shared<waterway>(0,"", "", nullptr);
    CHECK(*w0 == *w0c);
    w0->id = 90;
    CHECK(*w0 != *w0c);
    auto w1 = make_shared<waterway>(2,"abc", "json", nullptr);
    CHECK(*w0 != *w1);
	    waterway::add_gate(w1, make_shared<gate>(0, "1", "2"));// ensure it's a full structure
    auto w1_s = serialize_loop(w1);
    CHECK(*w1 == *w1_s);
	REQUIRE(w1_s->gates.size() == 1u);
	CHECK(w1_s->gates[0]->wtr_() == w1_s);// ensure that uplink survived serialization
    w1->name = "z3";
    CHECK(*w1 != *w1_s);
	w1->name = w1_s->name;
	CHECK(*w1 == *w1_s);//ensure they are equal before.
	    waterway::add_gate(w1, make_shared<gate>(1, "2", "3"));
	CHECK(*w1 != *w1_s);//now different
	    waterway::add_gate(w1_s, make_shared<gate>(1, "2", "3"));
	CHECK(*w1 == *w1_s);//now equal again
	auto s0 = w1->gates.size();
	w1->remove_gate(w1->gates[0]);
	CHECK(w1->gates.size() == s0-1u);
	CHECK(*w1 != *w1_s);//now different
}

TEST_CASE("gate basics") {
	using namespace shyft::energy_market::hydro_power;
	auto g0 = make_shared<gate>();
	auto g0c = make_shared<gate>(0, "", string(""));
	CHECK(*g0 == *g0c);
	auto g1 = make_shared<gate>(1, "2", string("3"));
	CHECK(*g0 != *g1);
	auto g1_s = serialize_loop(g1);
	CHECK(*g1 == *g1_s);
	g1->id++;
	CHECK(*g1 != *g1_s);
}

TEST_CASE("hps basics") {
    using namespace shyft::energy_market::hydro_power;
    auto s0 = make_shared<hydro_power_system>();
    CHECK(s0->id == 0);
    CHECK(s0->name == "");
    CHECK(s0->created == no_utctime);
    auto s1 = make_shared<hydro_power_system>("s1");
    CHECK(s1->name == "s1");
    s1->id = 33;
    CHECK(s0->equal_structure(*s1) == true);
}
}

namespace test {
    using namespace shyft::energy_market::hydro_power;
    using namespace std;
    hydro_power_system_ create_hydro_power_system() {
        /*
        Demonstrates how to build an inmemory representation of  HydroPowerSystem
        corresponding to the term 'detailed-hydro' in EMPS,
        using part of the bl�sj�/ulla-f�rre systems

        Returns
        ------ -
        HydroPowerSystem with reservoirs, tunnels, powerplants, including pumps
        */
        auto sorland = make_shared<hydro_power_system>(1,"s�rland");
        hydro_power_system_builder hps(sorland);
        auto blasjo = hps.create_reservoir(1,"bl�sj�",string("json{max_vol:3200Mm3"));
        auto saurdal = hps.create_unit (2,"saurdal","json{max_eff:600MW}");
        auto saurdal_ps= hps.create_power_plant (2000,"saurdal","json{}");
        power_plant::add_unit(saurdal_ps,saurdal);
        auto tunx = hps.create_tunnel(12,"bl�sj�-saurdal","json{}");
        connect(tunx)
            .input_from(blasjo)
            .output_to(saurdal);
        auto sandsavatn = hps.create_reservoir(3,"sandsvatn", "reservoir_data(560.0, 605.0, 230.0)");
        auto lauvastolsvatn = hps.create_reservoir(4,"lauvast�lsvatn", "reservoir_data(590.0, 605.0, 8.3)");
        auto kvilldal1 = hps.create_unit (51,"kvilldal_1","{Ek:1.3,outlet_level_masl:70");
        auto kvilldal2 = hps.create_unit (52,"kvilldal_2","{Ek:1.3,outlet_level_masl:70");
        auto kvilldal = hps.create_power_plant (5000,"kvilldal","json{}");
        power_plant::add_unit(kvilldal,kvilldal1);
        power_plant::add_unit(kvilldal,kvilldal2);
        auto t_kvilldal = hps.create_tunnel(510,"kvilldal hovedtunnel", "{alpha:0.000053}");
        auto t_kvill_penstock_1 = hps.create_tunnel(5101,"kvilldal_1_penstock","json{}");
        auto t_kvill_penstock_2 = hps.create_tunnel(5102,"kvilldal_2_penstock","json{}");
        auto t_saur_kvill = hps.create_tunnel(6,"saurdal-kvilldal-hoved-tunnel","json{}");
        auto t_sandsa_kvill = hps.create_tunnel(7,"sandsavatn-til-kvilldal", "json{}");
		waterway::add_gate(t_sandsa_kvill,make_shared<gate>(1,"gate1","open or closed"));
		auto t_lauvas_kvill = hps.create_tunnel(8,"lauvast�lsvatn-til-kvilldal", "json{}");
        connect(saurdal).output_to(t_saur_kvill);
        connect(t_saur_kvill).output_to(t_kvilldal);
        connect(t_kvilldal).output_to(t_kvill_penstock_1);
        connect(t_kvill_penstock_1).output_to(kvilldal1);
        connect(t_kvilldal).output_to(t_kvill_penstock_2);
        connect(t_kvill_penstock_2).output_to(kvilldal2);

        connect(t_sandsa_kvill).input_from(sandsavatn).output_to(t_kvilldal);
        connect(t_lauvas_kvill).input_from(lauvastolsvatn).output_to(t_kvilldal);

        auto vassbotvatn = hps.create_reservoir(9,"vassbotvatn", "json{}");
        auto stoelsdal_pumpe = hps.create_unit (10,"st�lsdal pumpe");
        auto stoelsdal = hps.create_power_plant (1,"st�lsdal","json{}");
        power_plant::add_unit(stoelsdal,stoelsdal_pumpe);
        auto above_vassbotvatn = hps.create_tunnel(11,"st�lsdals kraftstasjon(pumpe) til vassbotvatn det pumpes fra", "json{}");
        connect(above_vassbotvatn)
            .input_from(stoelsdal_pumpe);
        connect(vassbotvatn).input_from(above_vassbotvatn);
        auto tun_sandsa_stolsdal = hps.create_tunnel(120, "fra sandsvatn til st�lsdal pump", "json{}");
        connect(tun_sandsa_stolsdal).output_to(stoelsdal_pumpe);
        connect(sandsavatn).output_to(tun_sandsa_stolsdal, connection_role::main);

        auto suldalsvatn = hps.create_reservoir(13,"suldalsvatn","json{}");
        auto hylen = hps.create_unit (14,"hylen","json{}");
        auto hylen_ps = hps.create_power_plant (1400,"hylen","json{}");
        power_plant::add_unit(hylen_ps,hylen);
        connect(hps.create_river(15,"fra kvilldal til suldalsvatn", "json{}"))
            .input_from(kvilldal1)
            .input_from(kvilldal2)
            .output_to(suldalsvatn);
            
        connect(hps.create_tunnel(16,"hylen-tunnel", "json{}"))
            .input_from(suldalsvatn)
            .output_to(hylen);

        auto havet = hps.create_reservoir(17, "havet","json{}");

        connect(hps.create_river(18,"utl�p hylen","json{}"))
            .input_from(hylen)
            .output_to(havet);

        connect(hps.create_river(19,"bypass suldal til havet","json{}"))
            .input_from(suldalsvatn, connection_role::bypass)
            .output_to(havet);

        connect(hps.create_river(20,"flom suldal til havet","json{}"))
            .input_from(suldalsvatn, connection_role::flood)
            .output_to(havet);
        return sorland;
    }
}

TEST_SUITE("hps") {
TEST_CASE("hydro_power_system functionality") {
    using namespace shyft::energy_market::hydro_power;
    auto a = test::create_hydro_power_system();
    auto b = test::create_hydro_power_system();
    CHECK(a != nullptr);
    CHECK(b != nullptr);
    CHECK(a->equal_structure(*b) == true);
    SUBCASE("serialization") {
        auto b_as_blob = hydro_power_system::to_blob(b);
        CHECK(b_as_blob.size() > 0);
        auto b_from_blob = hydro_power_system::from_blob(b_as_blob);
        CHECK(b_from_blob->equal_structure(*b));
    }
    a->clear();
    b->clear();
}

TEST_CASE("hydro_power_system_topology") {
    using namespace shyft::energy_market::hydro_power;
    auto a = test::create_hydro_power_system();
    {
        auto blasjo = a->find_reservoir_by_name("bl�sj�");
        CHECK(blasjo != nullptr);
        auto blasjo_ds = blasjo->downstream_powerstations();
        auto blasjo_us = blasjo->upstream_powerstations();
        CHECK(blasjo_ds.size() == 1);
        CHECK(blasjo_ds[0]->name == "saurdal");
        CHECK(blasjo_us.size() == 0);
        auto vassbotvatn = a->find_reservoir_by_name("vassbotvatn");
        CHECK(vassbotvatn != nullptr);
        auto kvilldal_ps =a->find_power_plant_by_name ("kvilldal");
        CHECK(kvilldal_ps!=nullptr);
        CHECK(kvilldal_ps->name =="kvilldal");
        CHECK(kvilldal_ps->units.size()==2);
        auto vassbotvatn_us = vassbotvatn->upstream_powerstations();
        auto vassbotvatn_ds = vassbotvatn->downstream_powerstations();
        CHECK(vassbotvatn_us.size() == 1);
        CHECK(vassbotvatn_us[0]->name == "st�lsdal pumpe");
        CHECK(vassbotvatn_ds.size() == 0);
        SUBCASE("remove connection to water-route") {
            hydro_component::disconnect(vassbotvatn, vassbotvatn->upstreams[0].target_());
            CHECK(vassbotvatn->upstream_powerstations().size() == 0);
        }
        SUBCASE("power_stations up and down streams") {
            auto saurdal = a->find_unit_by_name ("saurdal");
            CHECK(saurdal != nullptr);
            auto rsv_us = saurdal->upstream_reservoirs();
            CHECK(rsv_us.size() == 1);
            CHECK(rsv_us[0]->name == "bl�sj�");

            // NOTE, junction is not yet supported auto rsv_ds = saurdal->downstream_reservoir();
            auto kvilldal = a->find_unit_by_name ("kvilldal_1");
            auto rsv_ds = kvilldal->downstream_reservoir();
            CHECK(rsv_ds != nullptr);
            CHECK(rsv_ds->name == "suldalsvatn");
        }
		SUBCASE("find_by_xx") {
			CHECK(a->find_power_plant_by_name ("saurdal")->id == 2000);
			CHECK(a->find_power_plant_by_id (2000)->name=="saurdal");
			CHECK(a->find_unit_by_name ("saurdal")->id == 2);
			CHECK(a->find_unit_by_id (2)->name == "saurdal");
			CHECK(a->find_reservoir_by_name("bl�sj�")->id == 1);
			CHECK(a->find_reservoir_by_id(1)->name == "bl�sj�");
			CHECK(a->find_waterway_by_name("bl�sj�-saurdal")->id == 12);
			CHECK(a->find_waterway_by_id(12)->name == "bl�sj�-saurdal");
		}
    }
    a->clear();
    a = nullptr;
}
}
