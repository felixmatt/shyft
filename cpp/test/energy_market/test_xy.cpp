#include <doctest/doctest.h>
#include <vector>
#include <cmath>
#include <stdexcept>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>

using std::vector;
using std::isfinite; 
using std::runtime_error;
using namespace shyft::energy_market::hydro_power;

TEST_SUITE("xy") {
    TEST_CASE("xy_point") {
        point a(0.0,1.0);
        point b(0.0,1.0);
        FAST_CHECK_EQ(a,b);
        FAST_CHECK_UNARY_FALSE(a!=b);
        b.y=2.0;
        FAST_CHECK_LT(a,b);
    }
    TEST_CASE("xy_basics") {
        xy_point_curve a;
        xy_point_curve b;
        FAST_CHECK_EQ(a,b);
        FAST_CHECK_EQ(isfinite(a.calculate_x(1.0)),false);
        FAST_CHECK_EQ(isfinite(a.calculate_y(1.0)),false);
        a.points.push_back(point(0.0,1.0));
        FAST_CHECK_NE(a,b);
        FAST_CHECK_EQ(a.calculate_x(1.0),doctest::Approx(0.0));
        FAST_CHECK_EQ(a.calculate_y(0.0),doctest::Approx(1.0));
        FAST_CHECK_EQ(false,a.is_xy_mono_increasing());
        a.points.push_back(point(1.0,2.0));
        FAST_CHECK_EQ(true,a.is_xy_mono_increasing());
        a.points.push_back(point(2.0,1.9));
        FAST_CHECK_EQ(false,a.is_xy_mono_increasing());
        xy_point_curve c(vector<point>{point(0.0,1.0),point(1.0,2.0)});
        xy_point_curve d(c);
        FAST_CHECK_EQ(c,d);
        xy_point_curve e(vector<double>{0.0,1.0},vector<double>{0.0,2.0});
        FAST_CHECK_EQ(e.points.size(),2u);
        try {
            xy_point_curve e(vector<double>{0.0,1.0},vector<double>{0.0});
            FAST_CHECK_UNARY(false);
        } catch(const runtime_error&) {
            FAST_CHECK_UNARY(true);
        }
    }
    TEST_CASE("xyz_basics") {
        xy_point_curve_with_z a;
        xy_point_curve_with_z b;
        FAST_CHECK_EQ(a,b);
        b.z=1.0;
        FAST_CHECK_NE(a,b);
        xy_point_curve_with_z c(xy_point_curve(vector<point>{point(0.0,0.0),point(1.0,1.0)}),3.0);
        FAST_CHECK_EQ(c.z,doctest::Approx(3.0));
        FAST_CHECK_EQ(c.xy_curve.calculate_x(0.5),doctest::Approx(0.5));
        FAST_CHECK_EQ(c.xy_curve.calculate_y(0.5),doctest::Approx(0.5));
        FAST_CHECK_EQ(c.xy_curve.calculate_y(1.5),doctest::Approx(1.5));
        FAST_CHECK_EQ(c.xy_curve.calculate_y(-1.5),doctest::Approx(-1.5));
        FAST_CHECK_EQ(c.xy_curve.calculate_x(1.5),doctest::Approx(1.5));
        FAST_CHECK_EQ(c.xy_curve.calculate_x(-1.5),doctest::Approx(-1.5));
        
    }
}
