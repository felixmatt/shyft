#include <doctest/doctest.h>
#include <vector>
#include <string>
#include <shyft/energy_market/id_base.h>


static std::vector<void *> obj_list; 

static void test_destroy(void *obj){
    obj_list.push_back(obj);
}
 
using namespace shyft::energy_market;

TEST_SUITE("pyobj") {
    TEST_CASE("em_handle") {
        em_handle::destroy = test_destroy;
        {em_handle a;}
        FAST_CHECK_EQ(0,obj_list.size());
        int i;
        {em_handle a;
            a.obj = &i;
        }
        FAST_REQUIRE_EQ(1,obj_list.size());
        FAST_CHECK_EQ(&i, obj_list[0]);
        
        em_handle a{&i};
        em_handle b(std::move(a));
        FAST_CHECK_EQ(&i, b.obj);
        
        em_handle c;
        c = std::move(b);
        FAST_CHECK_EQ(&i, c.obj);
        FAST_CHECK_EQ(nullptr, b.obj);
        FAST_CHECK_EQ(nullptr, a.obj);
    }
    
    TEST_CASE("id_base") {
        id_base a;
        id_base b;
        a = std::move(b);        
    }
}
