#include <build_test_model.h>
#include <doctest/doctest.h>
#include <serialize_loop.h>
#include <shyft/energy_market/srv/db.h>

#include <fstream>
using test::serialize_loop;


TEST_SUITE("complete_models") {
TEST_CASE("model_with_no_hps") {
    using namespace shyft::energy_market::market;
    auto m1 = test::build_model();
    auto m2 = test::build_model();
    CHECK(m1->equal_structure(*m2) == true);
    model_builder(m2).create_model_area(4,"extra","j");
    CHECK(m1->equal_structure(*m2) == false);
    auto m1_xml = m1->to_blob();
    auto m1_s = model::from_blob(m1_xml);
    CHECK(m1->equal_structure(*m1_s));
}

TEST_CASE("hydro power system") {
    using namespace shyft::energy_market::hydro_power;
    auto hps1 = test::build_hps("sørland");
    auto hps2 = test::build_hps("sørland");
    CHECK(hps1->equal_structure(*hps2) == true);
    auto r1 = hps2->find_reservoir_by_name("r1");
    auto o = hps2->find_reservoir_by_name("ocean");
    CHECK(r1 != nullptr);
    hydro_power_system_builder hpsb(hps2);
    connect(hpsb.create_river(100,"extra bypass",R"(water_route_data(10000, "uid:1234", "r1 to ocean"))"))
        .input_from(r1, connection_role::flood)
        .output_to(o);

    CHECK(hps1->equal_structure(*hps2) == false);
    CHECK(hps2->equal_structure(*hps1) == false);
    auto hps2_s = serialize_loop(hps2);
    CHECK(hps2->equal_structure(*hps2_s));
}

TEST_CASE("full model with hps roundtrip") {
    //using namespace shyft::energy_market::core;
    auto m1 = test::build_model();
    m1->area[1]->detailed_hydro = test::build_hps("a1.hydro_power_system");

    auto m1_s = serialize_loop(m1);

    CHECK(m1_s->equal_structure(*m1) == true);

}
#if 0
NO_TEST_CASE_HERE("ltm_model") {
    using namespace shyft::energy_market::market;
    shared_ptr<model> a;
    shared_ptr<model> b;
    {
        std::ifstream m1848("/home/u33573@energycorp.com/projects/core_mdl/1848.m.db",std::ios_base::binary);
        shyft::core::core_iarchive is(m1848,core_arch_flags);

        is>>a;
    }
    {
        std::ifstream m1848("/home/u33573@energycorp.com/projects/core_mdl/1848.m.db",std::ios_base::binary);
        shyft::core::core_iarchive is(m1848,core_arch_flags);

        is>>b;
    }
    FAST_CHECK_EQ(a->equal_structure(*b),true);
    
}
#endif
}
