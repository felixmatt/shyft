#include <doctest/doctest.h>

#include<vector>
#include<map>
#include<memory>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>

using namespace shyft::energy_market::hydro_power;
using std::make_shared;

TEST_SUITE("turb_eff_descr") {
    TEST_CASE("turbine_description") {
		xy_point_curve_with_z a;
		a.z = 100;
		a.xy_curve.points.push_back(point(0.0, 0.71));
		a.xy_curve.points.push_back(point(20.0, 0.8));
		a.xy_curve.points.push_back(point(20.0, 0.75));
		CHECK(a.xy_curve.points.size() == 3u);
		turbine_efficiency b;
		b.efficiency_curves.push_back(a);
		CHECK(b.efficiency_curves.size() == 1u);
		turbine_description c;
		c.efficiencies.push_back(b);
		CHECK(c.efficiencies.size() == 1u);
		//TODO: serialization loop, equality
    }
}
