#include <doctest/doctest.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/aggregate.h>
#include <shyft/energy_market/stm/water_route.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/market_ds.h>
#include <shyft/energy_market/stm/hps_ds.h>

#include <serialize_loop.h>

using namespace shyft::energy_market::hydro_power;
using test::serialize_loop;

namespace test {
    using namespace shyft::energy_market::stm;
    using namespace std;
    stm_hps_ create_stm_hps(int id=1,string name="sørland") {
        using shyft::energy_market::hydro_power::connect;
        using shyft::energy_market::hydro_power::connection_role;
        /*
        Demonstrates how to build an in memory representation of  stm HydroPowerSystem
        using part of the blåsjø/ulla-førre systems
        */
        auto sorland = make_shared<stm_hps>(id,name);
        stm_hps_builder hps(sorland);
        auto blasjo = hps.create_reservoir(1,"blåsjø",string("json{max_vol:3200Mm3"));
        auto saurdal = hps.create_unit(2,"saurdal","json{max_eff:600MW}");
        auto tunx = hps.create_tunnel(12,"blåsjø-saurdal","json{}");
        connect(tunx)
            .input_from(blasjo)
            .output_to(saurdal);
        auto sandsavatn = hps.create_reservoir(3,"sandsvatn", "reservoir_data(560.0, 605.0, 230.0)");
        auto lauvastolsvatn = hps.create_reservoir(4,"lauvastølsvatn", "reservoir_data(590.0, 605.0, 8.3)");
        auto kvilldal1 = hps.create_unit(51,"kvilldal_1","{Ek:1.3,outlet_level_masl:70");
        auto kvilldal2 = hps.create_unit(52,"kvilldal_2","{Ek:1.3,outlet_level_masl:70");
        auto t_kvilldal = hps.create_tunnel(510,"kvilldal hovedtunnel", "{alpha:0.000053}");
        auto t_kvill_penstock_1 = hps.create_tunnel(5101,"kvilldal_1_penstock","json{}");
        auto t_kvill_penstock_2 = hps.create_tunnel(5102,"kvilldal_2_penstock","json{}");
        auto t_saur_kvill = hps.create_tunnel(6,"saurdal-kvilldal-hoved-tunnel","json{}");
        auto t_sandsa_kvill = hps.create_tunnel(7,"sandsavatn-til-kvilldal", "json{}");
        auto t_lauvas_kvill = hps.create_tunnel(8,"lauvastølsvatn-til-kvilldal", "json{}");
		shyft::energy_market::stm::waterway::add_gate(t_lauvas_kvill, 1, "L1", "");
		auto g = dynamic_pointer_cast<shyft::energy_market::stm::gate>(t_lauvas_kvill->gates[0]);
		g->discharge_schedule.set(apoint_ts("shyft://stm/rid/oid/aid"));
        connect(saurdal).output_to(t_saur_kvill);
        connect(t_saur_kvill).output_to(t_kvilldal);
        connect(t_kvilldal).output_to(t_kvill_penstock_1);
        connect(t_kvill_penstock_1).output_to(kvilldal1);
        connect(t_kvilldal).output_to(t_kvill_penstock_2);
        connect(t_kvill_penstock_2).output_to(kvilldal2);

        connect(t_sandsa_kvill).input_from(sandsavatn).output_to(t_kvilldal);
        connect(t_lauvas_kvill).input_from(lauvastolsvatn).output_to(t_kvilldal);

        auto vassbotvatn = hps.create_reservoir(9,"vassbotvatn", "json{}");
        auto stoelsdal_pumpe = hps.create_unit(10,"stølsdal pumpe","json{}");
        auto above_vassbotvatn = hps.create_tunnel(11,"stølsdals kraftstasjon(pumpe) til vassbotvatn det pumpes fra", "json{}");
        connect(above_vassbotvatn)
            .input_from(stoelsdal_pumpe);
        connect(vassbotvatn).input_from(above_vassbotvatn);
        auto tun_sandsa_stolsdal = hps.create_tunnel(120, "fra sandsvatn til stølsdal pump", "json{}");
        connect(tun_sandsa_stolsdal).output_to(stoelsdal_pumpe);
        connect(sandsavatn).output_to(tun_sandsa_stolsdal, connection_role::main);

        auto suldalsvatn = hps.create_reservoir(13,"suldalsvatn","json{}");
        auto hylen = hps.create_unit(14,"hylen","json{}");

        connect(hps.create_river(15,"fra kvilldal til suldalsvatn", "json{}"))
            .input_from(kvilldal1)
            .input_from(kvilldal2)
            .output_to(suldalsvatn);
            
        connect(hps.create_tunnel(16,"hylen-tunnel", "json{}"))
            .input_from(suldalsvatn)
            .output_to(hylen);

        auto havet = hps.create_reservoir(17, "havet","json{}");

        connect(hps.create_river(18,"utløp hylen","json{}"))
            .input_from(hylen)
            .output_to(havet);

        connect(hps.create_river(19,"bypass suldal til havet","json{}"))
            .input_from(suldalsvatn, connection_role::bypass)
            .output_to(havet);

        connect(hps.create_river(20,"flom suldal til havet","json{}"))
            .input_from(suldalsvatn, connection_role::flood)
            .output_to(havet);
        return sorland;
    }
}

TEST_SUITE("stm_hps") {
TEST_CASE("stm_hydro_power_system serialization") {
    using namespace shyft::energy_market::stm;
    auto a = test::create_stm_hps();
	// just verify that we got a complete hooked up model:
	auto t=a->find_waterway_by_name("lauvastølsvatn-til-kvilldal");
	CHECK(t != nullptr);
	CHECK(t->gates.size() == 1u);// should be one here..
	CHECK(t->gates[0]->wtr_() == t);
	auto t_stm = std::dynamic_pointer_cast<shyft::energy_market::stm::waterway>(t);
	CHECK(t_stm != nullptr);
	auto t_hps = t->hps_();
	CHECK(t_hps != nullptr);
	auto s_stm = std::dynamic_pointer_cast<shyft::energy_market::stm::stm_hps>(t_hps);
	CHECK(s_stm != nullptr);
	//
    auto b = test::create_stm_hps();
    CHECK(a != nullptr);
    CHECK(b != nullptr);
    CHECK(a->equal_structure(*b) == true);
    SUBCASE("serialization") {
        auto b_as_blob = stm_hps::to_blob(b);
        CHECK(b_as_blob.size() > 0);
        auto b_from_blob = stm_hps::from_blob(b_as_blob);
        CHECK(b_from_blob->equal_structure(*b));
    }
    a->clear();
    b->clear();
}

TEST_CASE("stm_hydro_power_system_topology") {
    using namespace shyft::energy_market::hydro_power;
    auto a = test::create_stm_hps();
    {
        auto blasjo = a->find_reservoir_by_name("blåsjø");
        CHECK(blasjo != nullptr);
        auto blasjo_ds = blasjo->downstream_powerstations();
        auto blasjo_us = blasjo->upstream_powerstations();
        CHECK(blasjo_ds.size() == 1);
        CHECK(blasjo_ds[0]->name == "saurdal");
        CHECK(blasjo_us.size() == 0);
        auto vassbotvatn = a->find_reservoir_by_name("vassbotvatn");
        CHECK(vassbotvatn != nullptr);
        auto vassbotvatn_us = vassbotvatn->upstream_powerstations();
        auto vassbotvatn_ds = vassbotvatn->downstream_powerstations();
        CHECK(vassbotvatn_us.size() == 1);
        CHECK(vassbotvatn_us[0]->name == "stølsdal pumpe");
        CHECK(vassbotvatn_ds.size() == 0);
        SUBCASE("remove connection to water-route") {
            hydro_component::disconnect(vassbotvatn, vassbotvatn->upstreams[0].target_());
            CHECK(vassbotvatn->upstream_powerstations().size() == 0);
        }
        SUBCASE("power_stations up and down streams") {
            auto saurdal = a->find_unit_by_name ("saurdal");
            CHECK(saurdal != nullptr);
            auto rsv_us = saurdal->upstream_reservoirs();
            CHECK(rsv_us.size() == 1);
            CHECK(rsv_us[0]->name == "blåsjø");

            // NOTE, junction is not yet supported auto rsv_ds = saurdal->downstream_reservoir();
            auto kvilldal = a->find_unit_by_name ("kvilldal_1");
            auto rsv_ds = kvilldal->downstream_reservoir();
            CHECK(rsv_ds != nullptr);
            CHECK(rsv_ds->name == "suldalsvatn");
        }
    }
    a->clear();
    a = nullptr;
}

TEST_CASE("stm_system serialization") {
	using namespace shyft::energy_market::stm;
	auto a = make_shared<stm_system>(1, "ull", "aførre");
	a->market.push_back(make_shared<energy_market_area>(1, "NO1", "xx", a));
	a->market.push_back(make_shared<energy_market_area>(2, "NO2", "xx",a));
	a->market.push_back(make_shared<energy_market_area>(3, "NO3", "xx", a));
	a->market.push_back(make_shared<energy_market_area>(4, "NO4", "xx", a));
	a->hps.push_back(test::create_stm_hps(2,"sørland"));
	a->hps.push_back(test::create_stm_hps(1, "oslo"));
	auto a_blob = stm_system::to_blob(a);
	auto b = stm_system::from_blob(a_blob);
	CHECK(b != nullptr);
	FAST_CHECK_EQ(b->name , a->name);
	FAST_CHECK_EQ(b->id, a->id);
	FAST_CHECK_EQ(b->json, a->json);
	FAST_CHECK_EQ(b->market.size(), a->market.size());
	FAST_CHECK_EQ(b->hps.size(), a->hps.size());
}
}


