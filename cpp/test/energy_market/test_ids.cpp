

#include <type_traits>
#include <vector>
#include <map>
#include <tuple>
#include <utility>
#include <stdexcept>
#include <string>
#include <memory>
#include <iostream> // just for the experiment/debug purposes

#include <shyft/energy_market/dataset.h>
#include <shyft/energy_market/proxy_attr.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/water_route.h>
#include <shyft/energy_market/stm/aggregate.h>
#include <shyft/energy_market/stm/hps_ds.h>
#include <shyft/energy_market/stm/market_ds.h>

# include <doctest/doctest.h>

TEST_SUITE("ids") {
    TEST_CASE("get_set") {
        using namespace shyft::energy_market;
        using namespace shyft::energy_market::stm;
        using std::runtime_error;
		using shyft::core::utctime;
		using t_double = map<utctime, double>;
		utctime t0{ 0 };
        //--arrange the test
        auto sys=make_shared<stm_system>(1,"first","");
        auto ull=make_shared<stm_hps>(1,"Ulla-førre");
        ull->ids = make_shared<hps_ds>();// attach empty ids
        sys->hps.push_back(ull);
        sys->market.push_back(make_shared<energy_market_area>(1,"einar aas område","json-here",sys));
        auto b = make_shared<reservoir>(16606,"blåsjø","",ull);
        ull->reservoirs.push_back(b);
        //-- act
        FAST_CHECK_EQ(b->hrl.exists(),false);// none set yet.
		auto t_d = make_shared<t_double>();
		(*t_d)[t0] = 1233.0;
		b->hrl = t_d;
        t_double_ y=b->hrl;
        //-- assert
        FAST_CHECK_EQ((*y)[t0],doctest::Approx(1233.0));
		(*t_d)[t0] = 1000.0;
		b->hrl = t_d; // yes, it can act as an ordinary property.
        y=b->hrl;
        FAST_CHECK_EQ((*y)[t0],doctest::Approx(1000.0));
        auto a = make_shared<reservoir>(16600,"lauv","",ull);
        ull->reservoirs.push_back(a);
        FAST_CHECK_EQ(a->lrl,b->lrl);
		auto l = make_shared<t_double>();
		(*l)[t0] = 100.0;
        a->lrl.set(l);
        FAST_CHECK_NE(a->lrl,b->lrl);
		auto h = make_shared<t_double>();
		(*h)[t0] = 1010.0;
        a->hrl=h;
        FAST_CHECK_NE(a->hrl,b->hrl);
        b->hrl.get()->at(t0)=1010.0;
        FAST_CHECK_EQ(b->hrl.get()->at(t0),doctest::Approx(b->hrl.get()->at(t0)));
        b->hrl.remove();
        FAST_CHECK_NE(a->hrl,b->hrl);
        //-- test what happen if we try to read a not yet-set apptribute
        CHECK_THROWS_AS(b->hrl.get(),runtime_error);
    }
}
