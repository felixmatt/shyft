#pragma once

#include <string>
#include <vector>
#include <memory>
#include <boost/beast/version.hpp>
#if BOOST_BEAST_VERSION>=248
#include <boost/beast/core/flat_buffer.hpp>
#else
#include <boost/beast/core/multi_buffer.hpp>
#endif
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_series_dd.h>

namespace shyft::dtss { // fwd decl. only
    template <class CD>
    struct server;//fwd template server class
    struct standard_dtss_dispatcher;
}

namespace shyft::web_api {
    using std::string;
    using std::vector;
    using std::shared_ptr;
    using shyft::core::utctime;
    using shyft::core::utcperiod;
#if BOOST_BEAST_VERSION>=248
    using response_buffer=boost::beast::flat_buffer;
#else
    using response_buffer=boost::beast::multi_buffer;
#endif

    using dtss_server=dtss::server<dtss::standard_dtss_dispatcher>;
    //using dtss_server_=shared_ptr<dtss_server>;

    // incoming request messages
    /** evaluate request */
    struct read_ts_request {
        string request_id;///< client side request id, that is echoed back with the response
        utcperiod read_period;///< the read_period, to use for reading unbound terminals
        utcperiod clip_period;///< the period to clip the resulting time-series against(they could be longer)
        bool cache;///< if true use cache, any missing fragments will be read and stashed to cache
        vector<string> ts_ids;///< ts_ids, but later, we could allow expressions
        bool operator==(read_ts_request const&o) const {return cache==o.cache && read_period==o.read_period && clip_period==o.clip_period && ts_ids==o.ts_ids;}
        bool operator!=(read_ts_request const&o) const {return !operator==(o);}
    };
       /** request all info about the webaapi
     *
     * Provides static information
     */
    struct info_request {
        std::string request_id;
        bool operator==(info_request const&o) const noexcept {return request_id==o.request_id;}
        bool operator!=(info_request const&o) const {return !operator==(o);}
    };

    struct info_reply {
        std::string request_id;
        std::string ex_info;///< if empty, no exception otherwise ok.
        bool operator==(info_reply const&o) const noexcept {return request_id==o.request_id&& ex_info==o.ex_info;}
        bool operator!=(info_reply const&o) const {return !operator==(o);}
    };

    struct request_reply {
        std::string request_id;
        std::string ex_info;///< if empty, all ok, otherwise explanation in programmer-readable text
        bool operator==(request_reply const&o) const noexcept {return request_id==o.request_id&& ex_info==o.ex_info;}
        bool operator!=(request_reply const&o) const {return !operator==(o);}
    };


    /** find time-series request
     *
     * providing a search pattern, and
     * returning back a list of time-series
     *
     */
    struct find_ts_request {
        std::string request_id;
        std::string find_pattern;
        bool operator==(find_ts_request const&o) const {return request_id==o.request_id && find_pattern== o.find_pattern;}
        bool operator!=(find_ts_request const&o) const {return !operator==(o);}
    };

    /** @brief message for the average ts request
    *
    * The average ts-request do average of a read-request data before returning
    * the result back to the client.
    * This way it's easy to do some data-reduction and alignment on the server side.
    * E.g. If you where to plot 100 years of hourly data, you could choose to use the
    * to plot the month averages to get a reasonable amount of data back for plotting.
    * If you need to provide more accurate information, also consider the percentile
    * function that do percentiles of the read-result (reducing the amount of data),
    * and providing statistical information for each time-step.
    */
    struct average_ts_request {
        string request_id;///< client side request id, that is echoed back with the response
        utcperiod read_period;///< the read_period, to use for reading unbound terminals
        shyft::time_axis::generic_dt ta;///< time-axis for average calculations in the post-processsing step.
        bool cache;///< if true use cache, any missing fragments will be read and stashed to cache
        vector<string> ts_ids;///< ts_ids, but later, we could allow expressions
        bool operator==(average_ts_request const&o) const {return cache==o.cache && read_period==o.read_period && ta==o.ta && ts_ids==o.ts_ids;}
        bool operator!=(average_ts_request const&o) const {return !operator==(o);}
    };

        /** @brief The message for the percentile of ts request
    *
    * Contains enough information to implement the evaluate/read time-series,
    * then on that result, evaluate the percentiles over the specified time-axis.
    *
    */
    struct percentile_ts_request {
        string request_id;///< client side request id, that is echoed back with the response
        utcperiod read_period;///< the read_period, to use for reading unbound terminals
        shyft::time_axis::generic_dt ta;///< time-axis for average calculations in the post-processsing step.
        std::vector<int> percentiles;///< spec for percentiles, or statistical properties we could rather say
        bool cache;///< if true use cache, any missing fragments will be read and stashed to cache
        vector<string> ts_ids;///< ts_ids, but later, we could allow expressions
        bool operator==(percentile_ts_request const&o) const {return percentiles==o.percentiles && cache==o.cache && read_period==o.read_period && ta==o.ta && ts_ids==o.ts_ids;}
        bool operator!=(percentile_ts_request const&o) const {return !operator==(o);}
    };


    using shyft::time_series::dd::ats_vector; // or should we rather use vector<apoint_ts> in this context ?

    /** @brief message for storing time-series
    *
    * Used to store, update-existing(possibly merge points), create if missing, or entirely rewrite
    * a set of time-series.
    *
    * The response? request-id plus diagnostics if something did not work out
    *
    */
    struct store_ts_request {
        string request_id;///< client side request id, that is echoed back with the response
        bool merge_store; ///< true: merge points only, that is, keep all existing points, also in affected periods, update/insert those who are update/new.
                          ///< --false: store as if first wipe-out supplied ts.total_period(), then insert points(if any).
                          ///< note: that the supplied ts has to match the existing ts with respect to time-axis/linear,stair defs.
        bool recreate_ts; ///< if true, any existing time-series will be wiped out entirely, and replaced with the definitions of the passed ts.
        bool cache;       ///< if true, stash to cache while writing
        ats_vector tsv;   ///< stuff to stash to some-storage
    };
    // responses
    // -- they are usually TsVector or TsInfo

    // background service that
    // have a shared_ptr<dtss_server>
    // the boost beast foreground io-service receives ws messages
    // posts those to the bg thread for processing
    // the bg worker does folloing:
    // parses the request using boost.spirit.qi
    // forward the request to the dtss  dtss classical results
    // using boost.spirit.karma, generate the json-like response
    // emit the result to the output buffer
    // forward it to the boost beast io-services
    struct request_handler {
        dtss_server* srv{nullptr}; ///< not owning, and the lifetime of srv outlives the request-handler
        response_buffer do_the_work(string request);
    };

    int start_web_server(request_handler& bg_server, string address_s,unsigned short port,shared_ptr<string const> doc_root,int threads,int bg_threads);
}
