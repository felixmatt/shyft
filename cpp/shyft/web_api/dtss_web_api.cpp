#include "targetver.h"
#include <boost/asio/buffer.hpp>
#include <shyft/web_api/dtss_web_api.h>
#include <shyft/dtss/dtss.h>
#include <shyft/web_api/web_api_grammar.h>
#include <shyft/web_api/web_api_generator.h>


namespace shyft::web_api {
    using shyft::time_series::dd::apoint_ts;
    using namespace shyft::time_series::dd;
    using namespace shyft;
    using generator::atsv_generator;
    using generator::ts_info_vector_generator;
    using boost::spirit::karma::generate;
    using std::vector;
    using namespace shyft::dtss;
    namespace grammar {
    extern template struct web_request_grammar<const char*>;
    }
    /** @brief variant visitor dispatcher for messages
     *
     */
    struct message_dispatch {
        using return_type=std::string;
        dtss_server *srv{nullptr};//<< non-null dtss_server pointer.

        std::string gen_ok_response(std::string const&req_id, std::string diag) {
            return "{ \"request_id\" : \""+req_id + "\",\"diagnostics\": \""+ diag +"\" }";
        }

        std::string operator()(find_ts_request const&fts) {
            auto r=srv->do_find_ts(fts.find_pattern);
            std::string response=string("{\"request_id\":\"")+fts.request_id+string("\",\"result\":");
            auto  sink=std::back_inserter(response);
            if(!generate(sink,ts_info_vector_generator<decltype(sink)>(),r)) {
                response= "failed to genereate response for " +fts.request_id;
            } else {
                response +="}";
            }
            return response;
        }

        std::string operator()(info_request const&ir) {
            return gen_ok_response(ir.request_id,"not implemented");
        }

        std::string operator()(read_ts_request const&rts) {
            ts_vector_t rtsv;
            for(auto const& sym_ts:rts.ts_ids)
                rtsv.emplace_back(apoint_ts(sym_ts));

            auto r=srv->do_evaluate_ts_vector(rts.read_period,rtsv,rts.cache,rts.cache,rts.clip_period);
            return gen_tsv_response(rts.request_id,r);
        }

        std::string gen_tsv_response(std::string const&req_id, ts_vector_t const &r) {
            std::string response=string("{\"request_id\":\"")+req_id+string("\",\"tsv\":");
            auto  sink=std::back_inserter(response);
            if(!generate(sink,atsv_generator<decltype(sink)>(),r)) {
                response= "failed to genereate response for " +req_id;
            } else {
                response +="}";
            }
            return response;
        }

        std::string operator()(average_ts_request const& rts) {
            ts_vector_t rtsv;
            for(auto const& sym_ts:rts.ts_ids)
                rtsv.emplace_back(apoint_ts(sym_ts));

            auto raw=srv->do_evaluate_ts_vector(rts.read_period,rtsv,rts.cache,rts.cache,utcperiod{});
            auto r=raw.average(rts.ta);
            return gen_tsv_response(rts.request_id,r);
        }

        std::string operator()(percentile_ts_request const& rts) {
            ts_vector_t rtsv;
            for(auto const& sym_ts:rts.ts_ids)
                rtsv.emplace_back(apoint_ts(sym_ts));
            vector<int64_t> p;p.reserve(rts.percentiles.size());for(auto const i:rts.percentiles) p.emplace_back(i);
            auto r=srv->do_evaluate_percentiles(rts.read_period, rtsv, rts.ta, p, rts.cache,rts.cache);
            return gen_tsv_response(rts.request_id,r);
        }

        std::string operator()(store_ts_request const& r) {
            if(r.merge_store) {
                srv->do_merge_store_ts(r.tsv,r.cache);
            } else {
                srv->do_store_ts(r.tsv,r.recreate_ts,r.cache);
            }
            return gen_ok_response(r.request_id,"");
        }


    };


    template <typename P,typename V>
    bool phrase_parser(char const* input, P const& p,V &v, bool full_match = true){
        using boost::spirit::qi::phrase_parse;
        using boost::spirit::qi::ascii::space;
        char const* f(input);
        char const* l(f + strlen(f));
        return phrase_parse(f, l, p,space,v) && (!full_match || (f == l));
    }

    response_buffer request_handler::do_the_work(string request) {

        response_buffer b;
        grammar::web_request wr;

        grammar::web_request_grammar<const char*> web_req_;
        message_dispatch msg_dispatch{srv};
        bool ok_parse=false;
         std::string response;
        try {
           ok_parse=phrase_parser( request.c_str(),web_req_,wr);
            if(ok_parse) {
                response = boost::apply_visitor(msg_dispatch,wr);
            } else {
                response= "not understood:" + request;
            }
        } catch (std::runtime_error const &re) {
            response = string("request_parse:")+ re.what();
        }
        auto n=boost::asio::buffer_copy(b.prepare(response.size()),boost::asio::buffer(response));
        b.commit(n);
        return b;
    }
}
