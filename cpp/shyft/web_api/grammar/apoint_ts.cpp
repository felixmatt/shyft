#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {

    inline apoint_ts mk_apoint_ts(std::string const&id,bool stair_case,shyft::time_axis::generic_dt const&ta,std::vector<double> const&v) {
        apoint_ts ts(ta,v,stair_case?shyft::time_series::POINT_AVERAGE_VALUE:shyft::time_series::POINT_INSTANT_VALUE);
        return id.size()==0?ts:apoint_ts(id,ts);
    }


    template<typename Iterator,typename Skipper>
    apoint_ts_grammar<Iterator,Skipper>::apoint_ts_grammar():apoint_ts_grammar::base_type(start,"apoint_ts") {

            start = (lit('{')>>
                     lit("\"id\"")>':'> quoted_string >','>
                     lit("\"pfx\"")>':'>bool_>','>
                     lit("\"time_axis\"")>':'>time_axis_>','>
                     lit("\"values\"")>':'>values_ >
                    lit('}')) [_val=phx::bind(mk_apoint_ts,_1,_2,_3,_4)]
            ;
            start.name("apoint_ts");
            on_error<fail>(start, error_handler(_4, _3, _2));
        }

        template struct apoint_ts_grammar<request_iterator_t,request_skipper_t>;
}
