#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {

    inline utctime mk_utctime(unsigned Y,unsigned M, unsigned D, unsigned h, unsigned m,unsigned s) {
        static calendar utc;
        return utc.time(int(Y),int(M),int(D),int(h),int(m),int(s));
    }
    
    inline utctime mk_utctime_from_double(double d) {
        return from_seconds(d);
    }

    
    template<typename Iterator>
    utctime_grammar<Iterator>::utctime_grammar() : utctime_grammar::base_type(start,"utctime") {
        // > = expectation point
        start = lexeme[( '"'> (d4_ >> '-' )> d2_>'-'>d2_>'T'>d2_>':'>d2_>':'>d2_>'Z'>lit('"')) [_val = phx::bind(mk_utctime,_1,_2,_3,_4,_5,_6) ] ]
                |
                double_ [ _val=phx::bind(mk_utctime_from_double,_1) ]
                ;
        start.name("utctime");
        on_error<fail>(start, error_handler(_4, _3, _2));
    }
    
    template struct utctime_grammar<request_iterator_t>;
}
