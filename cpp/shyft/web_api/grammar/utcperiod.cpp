#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {

    inline utcperiod mk_period(utctime s,utctime e) {return utcperiod(s,e);}

    
    template<typename Iterator,typename Skipper>
    utcperiod_grammar<Iterator,Skipper>::utcperiod_grammar() : utcperiod_grammar::base_type(start,"utcperiod") {

        start = (
            '['>t_>','>t_>']'
        )
        [ _val = phx::bind(mk_period,_1,_2) ];
        start.name("utcperiod");
        on_error<fail>(start, error_handler(_4, _3, _2));
    }

    template struct utcperiod_grammar<request_iterator_t,request_skipper_t>;

}
