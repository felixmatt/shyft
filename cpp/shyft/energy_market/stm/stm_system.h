#pragma once
#include <string>
#include <memory>
#include <vector>
#include <shyft/core/utctime_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/stm/hps_ds.h>
#include <shyft/energy_market/stm/market_ds.h>

namespace shyft::energy_market::stm {
    using std::shared_ptr;
    using std::weak_ptr;
    using std::vector;
    using std::string;
    using std::logic_error;
    using shyft::core::utctime;
    using shyft::core::utctimespan;
    using shyft::core::utcperiod;
    using shyft::time_axis::generic_dt;
    using std::static_pointer_cast;
 
    // --
    // fwd enough of the sub-components so that the top level is well defained.
    // this file should be possible to include from the sub-components,
    // so we have to forward decl. types here, 
    struct energy_market_area;
    using energy_market_area_=shared_ptr<energy_market_area>;
    struct reservoir;
    using reservoir_=shared_ptr<reservoir>;
    struct unit;
    using unit_=shared_ptr<unit>;
    struct waterway;
    using waterway_=shared_ptr<waterway>;
    struct catchment;
    using catchment_=shared_ptr<catchment>;
    struct power_plant;
    using power_plant_=shared_ptr<power_plant>;

    /** @brief stm hydro_power_system 
     *
     * Is the same as the core hydro_power_system, 
     * but with added attributes suitable for the stm.
     * 
     * The attributes are attached to the objects (as ususal),
     * but stored in the hps_ds, the indata-set for hydro power system (a bit more sophisitcated).
     * 
     */
    struct stm_hps:hydro_power::hydro_power_system {
        using super = hydro_power::hydro_power_system;

        hps_ds_ ids;///< reference to the input data-sets for the hydro-power-system (we keep the attribute storage there)
        hps_ds_ rds;///< reference to the result data-sets for the hydro-power-system (we keep the attribute storage there)

        stm_hps();
        stm_hps(int id, const string&name);
        static string  to_blob(const shared_ptr<stm_hps>&s);
        static shared_ptr<stm_hps> from_blob(const string &xmls);
        x_serialize_decl();
    };
    using stm_hps_ = shared_ptr<stm_hps>;
    using stm_hps__ = weak_ptr<stm_hps>;

    /** @brief input data-set container for stm hydro power system
    *
    * Contains needed code to get the correct attribute(data-set) container from
    * a basic object like water_route,aggregate etc, 
    * by navigating up to the hydro-power-system, and then pull out the reference
    * or const reference to the data-set container for the type (e.g. water_routes have it's own section)
    *
    * @tparam T type like reservoir, aggregate,water_route
    * @see hps_rds, proxy_attr
    *
    */
    template<class T>
    struct hps_ids {
        static typename T::ds_collection_t& ds(T*o) {
            return  static_pointer_cast<stm_hps>(o->hps_())->ids->get(o);
        }
        static typename T::ds_collection_t const & ds(const T*o) {
            return  static_pointer_cast<stm_hps>(o->hps_())->ids->get(o);
        }
    };

    /** @brief result data-set container for stm hydro power system
    *
    * Contains needed code to get the correct attribute(data-set) container from
    * a basic object like water_route,aggregate etc,
    * by navigating up to the hydro-power-system, and then pull out the reference
    * or const reference to the data-set container for the type (e.g. water_routes have it's own section)
    *
    * @tparam T type like reservoir, aggregate,water_route
    * @see hps_ids, proxy_attr
    *
    */
    template<class T>
    struct hps_rds {
        static typename T::ds_collection_t& ds(T*o) {
            return  static_pointer_cast<stm_hps>(o->hps_())->rds->get(o);
        }
        static typename T::ds_collection_t const & ds(const T*o) {
            return  static_pointer_cast<stm_hps>(o->hps_())->rds->get(o);
        }
    };

    struct stm_rule_exception:logic_error {
        stm_rule_exception(const string& why):logic_error(why){}
    };

    /** @brief A builder that ensure building rules are followed 
     * 
     * The idea here is to provid functions that build a system that
     * is verified as it is built, including uniqueue identifiers and naming
     * for each individual component.
     * 
     * Primary use is on the python-api -side, but it could be useful on
     * c++ side since it enforces one set of rules.
     */
    struct stm_hps_builder {
        stm_hps_ s;
        explicit stm_hps_builder(const stm_hps_&s):s{s}{}
        catchment_ create_catchment(int id,const string&name,const string &json);
      	reservoir_ create_reservoir(int id,const string&name,const string &json);
        unit_ create_unit(int id,const string&name,const string &json);
        waterway_ create_waterway(int id,const string&name,const string &json);
        power_plant_ create_power_station(int id,const string&name,const string &json);
        waterway_ create_tunnel(int id,const string&name,const string &json) {return create_waterway(id,name,json);}
        waterway_ create_river(int id,const string&name,const string &json) {return create_waterway(id,name,json);}
    };

    /** @brief stm system
     *
     * Contains all needed components to describe a stm system.
     * This is basically the energy_market_model, but tuned to the
     * short term optimization processes and models.
     * 
     * The stm system model contains 
     * 
     *  * (stm) hydro-power-systems (one or more)
     *  * (energy) market-price-areas that contains the hydro-power-systems
     *  * other groups, or kind of market, like frequency or frequency-reserve markets
     *  
     */
    struct stm_system:id_base {
        using super=id_base;

        vector<stm_hps_> hps;
        vector<energy_market_area_> market;
        market_ds_ ids;
        market_ds_ rds;

        stm_system();
        stm_system(int id,string name, string json);

        static string  to_blob(const shared_ptr<stm_system>&s);
        static shared_ptr<stm_system> from_blob(const string &xmls);

        x_serialize_decl();
    };
    using stm_system_=shared_ptr<stm_system>;
    using stm_system__=weak_ptr<stm_system>;

    /** @brief input data-set container for stm system
    *
    * Contains needed code to get the correct attribute(data-set) container from
    * a basic object like market etc. 
    * by navigating up to the stm_system, and then pull out the reference
    * or const reference to the data-set container for the type
    *
    * @tparam T type like market
    * @see hps_rds, proxy_attr
    *
    */
    template<class T>
    struct sys_ids {
        static typename T::ds_collection_t& ds(T*o) {
            return  o->sys_()->ids->get(o);
        }
        static typename T::ds_collection_t const & ds(const T*o) {
            return  o->sys_()->ids->get(o);
        }
    };

    template<class T>
    struct sys_rds {
        static typename T::ds_collection_t& ds(T*o) {
            return  o->sys_()->rds->get(o);
        }
        static typename T::ds_collection_t const & ds(const T*o) {
            return  o->sys_()->rds->get(o);
        }
    };

}
x_serialize_export_key(shyft::energy_market::stm::stm_hps);
x_serialize_export_key(shyft::energy_market::stm::stm_system);
