#pragma once
#include <memory>
#include <shyft/energy_market/stm/attribute_types.h>

namespace shyft::energy_market::stm {
    using std::shared_ptr;
    struct reservoir;
    struct waterway;
    struct power_plant;
    struct gate;
    struct catchment;
    struct unit;

    /** @brief hydro-power (in)dataset 
    *
    * container for the values (if any) associated with 
    * the objects in a hydro power system.
    * 
    * The attributes are related to it's objects using type-unique object id and strongly typed attribute-id
    * 
    */
    struct hps_ds {
        hps_ds()=default;
        
        reservoir_ds rsv;
        waterway_ds wtr;
        unit_ds unitds;
        catchment_ds ctchm;
        power_plant_ds pwr_plant;
        gate_ids gt;
        
        // sih: typemap (consider tuple of the above instead (sukk.))
        reservoir_ds& get(reservoir* = nullptr) { return rsv; }
        reservoir_ds const& get(const reservoir* = nullptr) const { return rsv; }

        waterway_ds& get(waterway*  = nullptr) { return wtr; }
        waterway_ds const& get(const waterway* = nullptr) const { return wtr; }

        unit_ds& get(unit* = nullptr) { return unitds; }
        unit_ds const& get(const unit* = nullptr) const { return unitds; }

        catchment_ds& get(catchment* = nullptr) { return ctchm; }
        catchment_ds const& get(const catchment * = nullptr) const { return ctchm; }

        power_plant_ds& get(power_plant* = nullptr) { return pwr_plant; }
        power_plant_ds const& get(const power_plant* = nullptr) const { return pwr_plant; }

        gate_ids& get(gate* = nullptr) { return gt; }
        gate_ids const& get(const gate* = nullptr) const { return gt; };
        x_serialize_decl();
    };
    using hps_ds_ = shared_ptr<hps_ds>;

}

x_serialize_export_key(shyft::energy_market::stm::hps_ds);
