#pragma once
#include <map>
#include <string>
#include <memory>
#include <shyft/core/utctime_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/core/time_series_dd.h>
#include <shyft/energy_market/dataset.h>
#include <shyft/energy_market/proxy_attr.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/attribute_types.h>

namespace shyft::energy_market::stm {
	using std::string;
	using std::shared_ptr;
	using std::weak_ptr;
	using std::make_shared;
	using std::static_pointer_cast;
	using std::true_type;
	using std::false_type;
	using shyft::time_series::dd::apoint_ts;
	using std::map;
	using shyft::core::utctime;
	using core::proxy_attr;

	struct stm_system;

	/* @brief energy market area
	*
	* suited for the stm optimization, keeps attributes for an area where the price/load and
	* other parameters controlling the optimization problem can be kept
	*
	*
	*/
	struct energy_market_area:id_base {
		using super = id_base;
		using ds_collection_t = energy_market_area_ds;
		using ids = sys_ids<energy_market_area>;
		using rds = sys_rds<energy_market_area>;
        using e_attr=energy_market_area_attr; // enum attr type
        using e_attr_seq_t=energy_market_area_attr_seq_t;// the full sequence of attr type
		energy_market_area();
		energy_market_area(int id, const string& name, const string&json, const stm_system_& sys);
		
		stm_system_ sys_() const { return sys.lock(); }
		stm_system__ sys; ///< reference up to the 'owning' optimization system.

		proxy_attr<energy_market_area, apoint_ts, energy_market_area_attr, energy_market_area_attr::price, ids> price{*this};
		proxy_attr<energy_market_area, apoint_ts, energy_market_area_attr, energy_market_area_attr::load, ids> load{*this};
		proxy_attr<energy_market_area, apoint_ts, energy_market_area_attr, energy_market_area_attr::max_buy, ids> max_buy{*this};
		proxy_attr<energy_market_area, apoint_ts, energy_market_area_attr, energy_market_area_attr::max_sale, ids> max_sale{*this};

		proxy_attr<energy_market_area, apoint_ts, energy_market_area_attr, energy_market_area_attr::buy, rds> buy{*this};
		proxy_attr<energy_market_area, apoint_ts, energy_market_area_attr, energy_market_area_attr::sale, rds> sale{*this};

        // meta-programming support, list all mappings of the above proxy attributes here
        // note: later using more clever enums, we can use boost::hana adapt instead
        struct a_map {
            using proxy_container=energy_market_area;
            //static constexpr auto ref(rsv_attr_c<rsv_attr::lrl>) noexcept { return &proxy_container::lrl;}
            def_proxy_map(energy_market_area_attr,price)
            def_proxy_map(energy_market_area_attr,load)
            def_proxy_map(energy_market_area_attr,max_buy)
            def_proxy_map(energy_market_area_attr,max_sale)
            def_proxy_map(energy_market_area_attr,buy)
            def_proxy_map(energy_market_area_attr,sale)
        };

		x_serialize_decl();
	};
	using energy_market_area_ = shared_ptr<energy_market_area>;


}

x_serialize_export_key(shyft::energy_market::stm::energy_market_area);
