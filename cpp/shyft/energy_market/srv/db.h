/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <memory>
#include <string>
#include <cstdint>
#include <exception>
#include <regex>
#include <atomic>
#include <fstream>
#include <unordered_map>
#include <list>
#include <mutex>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <shyft/core/core_archive.h>
#include <shyft/core/utctime_utilities.h>

#include <boost/filesystem.hpp>
#include <shyft/energy_market/srv/model_info.h>
#include <shyft/core/lru_cache.h>
namespace shyft::energy_market::srv {

    namespace fs=boost::filesystem;
    using std::string;
    using std::to_string;    
    using std::stoi;
    using std::vector;
    using std::exception;
    using std::runtime_error;
    using std::shared_ptr;
    using std::ifstream;
    using std::ofstream;
    using std::unordered_map;
    using std::list;
    using std::pair;
    using std::mutex;
    using std::lock_guard;

    using shyft::core::core_iarchive;
    using shyft::core::core_oarchive;
    using shyft::core::lru_cache;


    /** @brief db-storage for models of type M
    *
    * This class is responsible for storing models to a backing-store 
    * file-system, along with model-info, that contains useful, shorthand
    * information about the models.
    * 
    * Assumptions: 
    *    1. one server for each root-directory.
    *    2. the server is responsible for ALL updates/changes to models (so no file-copy without restart server is supported)    
    * 
    * @note : the assumption about one server pr. root_dir can/should 
    *         be enforced using lock file. .e.g. boost::interprocess::file_lock
    * 
    * (model-info,models) are stored with filenames ( <id>.m.db, <id>.i.db)
    * 
    * @tparam M
    *   A serializable model type
    */
    template<class M>
    struct db {
        using model_t=M;// export model-type
        string root_dir;///< the root-directory for models
        mutable std::atomic_int64_t uid{0};//
        
        db(string const&root_dir):root_dir{root_dir} {
            if (fs::is_directory(root_dir)) {
                uid=find_max_model_id(true);//initialize id-generator
                // should we also read and cache all model-infos?
                // .. and models ?
            } else {
                if (!fs::exists(root_dir)) {
                    if (!fs::create_directories(root_dir)) 
                        throw runtime_error(string("m_db: failed to create root directory :") + root_dir);
                } else 
                    throw runtime_error(string("m_db: designated root directory is not a directory:") + root_dir);
            } 
        }
        int64_t find_max_model_id(bool fill_cache=false) {
                fs::path root(root_dir);
                string match="\\d+\\.i\\.db";
                std::regex r_match(match, std::regex_constants::ECMAScript | std::regex_constants::icase);
                int64_t max_id=0;
                for (auto& x : fs::directory_iterator(root)) {
                    if (fs::is_regular(x.path())) {
                        string fn = x.path().lexically_relative(root).generic_string(); // x.path() except root-part
                        if (std::regex_search(fn, r_match)) {
                            int64_t f_id= stoi(fn);
                            if(f_id>0) {
                                max_id=std::max(max_id,f_id);
                                if(fill_cache)
                                    info_cache.add_item(f_id,read_model_info(fn));
                            }
                        }
                    }
                }
                return max_id;
        }
    private:
        using info_cache_t= lru_cache<int64_t, model_info, unordered_map>;
        mutable mutex mx; ///< mutex to protect access to c and cs
        mutable info_cache_t info_cache{100000};///<< we aim to keep all info items in cache, max 100k seems reasonable
        
        void add_info_item(int64_t mid,model_info const&mi) const {
            lock_guard<mutex> guard(mx);
            info_cache.add_item(mid,mi);
            if(mid > uid) uid=mid;//important! update the uid seed to max if needed.
        }
        
        bool try_get_info_item(int64_t mid,model_info &m) const {
            lock_guard<mutex> guard(mx);
            return info_cache.try_get_item(mid,m);
        }
        
        void remove_info_item(int64_t mid) const {
            lock_guard<mutex> guard(mx);
            info_cache.remove_item(mid);
        }
        
        int64_t mk_unique_model_id() {return ++uid;}
        
        model_info read_model_info(string const&fn) const {
            model_info r;
            auto fpath=(root_dir/fs::path(fn)).generic_string();
            ifstream ifs(fpath,std::ios_base::binary);
            core_iarchive ia(ifs,core_arch_flags);
            ia>>r;
            return r;
        }

        model_info read_model_info(int64_t mid) const {
            return read_model_info((fs::path(root_dir)/(to_string(mid)+".i.db")).generic_string());
        }
        
    public:
    
        vector<model_info> get_model_infos(vector<int64_t>const & mids) const {
            vector<model_info> r;
            vector<string> fns;
            if(!mids.size()) {
                info_cache.apply_to_items(
                    [&r](int64_t mid,model_info const&mi) {
                        r.push_back(mi);
                    }
                );
            } else {
                for(auto mid:mids) {
                    model_info mi;
                    if(try_get_info_item(mid,mi)) {
                        r.push_back(mi);
                    } else {
                        mi=read_model_info(mid);
                        r.push_back(mi);
                        add_info_item(mid,mi);
                    }
                }
            }
            return r;        
        }
        
        
        int64_t store_model( shared_ptr<M> const& m, model_info const &mi) {
            if(!m)  throw runtime_error("Storing null model is not allowed(resonable?)");
            int64_t mid=m->id;
            if(mid<=0) {
                mid=mk_unique_model_id();
            } else {
                if(mid != mi.id)
                    throw runtime_error("model and model-info ids are different:"+to_string(m->id)+"!="+to_string(mi.id));
            }
            {//TODO: consider mutable m, so that we can set the m.id to mid here
            auto fnm=(fs::path(root_dir)/(to_string(mid)+".m.db")).generic_string();
            ofstream ofm(fnm,std::ios::binary|std::ios::trunc);
            core_oarchive osm(ofm,core_arch_flags);
            osm<<m;
            }
            {
            auto fni=(fs::path(root_dir)/(to_string(mid)+".i.db")).generic_string();
            ofstream ofi(fni,std::ios::binary|std::ios::trunc);
            core_oarchive osi(ofi,core_arch_flags);
            model_info tmi{mi};tmi.id=mid;// enforce same id
            osi<<tmi;
            add_info_item(mid,tmi);
            }
            return mid;
        }

        shared_ptr<M> read_model(int64_t mid) const {
            auto fn=(fs::path(root_dir)/(to_string(mid)+".m.db")).generic_string();
            
            if(!fs::exists(fn)) 
                throw runtime_error("read_model: missing file:"+fn);
            if(!fs::is_regular_file(fn))
                throw runtime_error("read_model: not a regular file:"+fn);

            shared_ptr<M> r;
            ifstream fm(fn,std::ios::binary);
            core_iarchive is(fm,core_arch_flags);
            is>>r;
            r->id=mid;// enforce, late model-id is equal to file/id and content, ref todo in store model above(also enforced on client read side!)
            return r;
        }
        // used to provide a raw blob for server-side read (avoid serialize cycle on server-side)
        string read_model_blob(int64_t mid) const {
            auto fn=(fs::path(root_dir)/(to_string(mid)+".m.db")).generic_string();
            
            if(!fs::exists(fn)) 
                throw runtime_error("read_model: missing file:"+fn);
            if(!fs::is_regular_file(fn))
                throw runtime_error("read_model: not a regular file:"+fn);
            
            ifstream input(fn,std::ios::binary);
            std::ostringstream buf;
            buf << input.rdbuf();
            return buf.str();
        }
        
        int64_t remove_model(int64_t mid) {
            vector<fs::path> fns {
                (fs::path(root_dir)/(to_string(mid)+".m.db")),
                (fs::path(root_dir)/(to_string(mid)+".i.db"))
            };
            for(auto const& fn:fns) {
                if(fs::exists(fn)) {
                    fs::remove(fn);
                }
            }
            remove_info_item(mid);
            return 0;//TODO: consider signature, should we say, remove succeed if model is gone after (regardless precondition) ?
        }
        
        bool update_model_info(int64_t mid,model_info const &mi) {
            if(mi.id != mid) {
                throw runtime_error("update_model_info: mid must equal mi.id (unfortunate design hmm.);"+to_string(mid)+"!="+to_string(mi.id));
            }
            vector<int64_t>mids;mids.push_back(mid);
            auto me=get_model_infos(mids);
            auto fni=(fs::path(root_dir)/fs::path((to_string(mid)+".i.db"))).generic_string();
            model_info tmi{mi};tmi.id=mid;// make sure to store correct id
            ofstream ofi(fni,std::ios::binary|std::ios::trunc);
            core_oarchive osi(ofi,core_arch_flags);
            osi<<tmi;
            add_info_item(mid,tmi);
            return true;
        }
    };
}
