/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <shyft/core/utctime_utilities.h>

namespace shyft::energy_market::srv {

    using std::string;
    using shyft::core::utctime;
    using shyft::core::no_utctime;

    /** @brief common model information
    *
    * The model information contains some mandatory fields
    * like 
    * id, a unique id for the model, and some other useful
    * or optional stuff
    * 
    * This can be shared between models, and the json part of it
    * is there to provide easy scripting flexibility.
    * 
    */
    struct model_info {
        int64_t id;///< the unique id of the model TODO: consider drop it,and return map<id,info> instead
        string name;///< optional, often useful name
        utctime created;///< or modified, we might be able to keep track of this
        string json;///< optional, for scripting support
        // py cts
        model_info():id{0},name{},created{no_utctime},json{}{}
        model_info(int64_t id, string const&name,utctime created, string json=string{}):id{id},name{name},created{created},json{json}{}
        bool operator==(model_info const& o) const noexcept {return name==o.name && id==o.id && created == o.created && json == o.json;}
        bool operator!=(model_info const&o) const noexcept {return !operator==(o);}
        x_serialize_decl();
    };
}

x_serialize_export_key(shyft::energy_market::srv::model_info);
