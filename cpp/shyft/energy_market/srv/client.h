/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <cstdint>
#include <exception>
#include <dlib/server.h>
#include <dlib/iosockstream.h>
#include <thread>
#include <shyft/energy_market/srv/msg_defs.h>
#include <shyft/core/core_archive.h>
#include <shyft/core/utctime_utilities.h>

namespace shyft::energy_market::srv {
    using std::vector;
    using std::string;
    using std::string_view;
    using std::to_string;
    using std::runtime_error;
    using std::exception;
    using std::chrono::seconds;
    using std::chrono::milliseconds;
    using shyft::core::core_iarchive;
    using shyft::core::core_oarchive;
    using std::unique_ptr;
    using std::shared_ptr;
    using std::make_unique;
    using std::max;
    using std::this_thread::sleep_for;

    /** @brief resilient server connection 
    *
    *  Helper class to connect/reconnect dlib::iosockstream connections
    *  that could break if server is rebootet, network is temporary out.
    * 
    */
    struct srv_connection {
        string host_port;///< like ip:port, or name:port
        int timeout_ms{1000};///< timout for operations, def.1s
        unique_ptr<dlib::iosockstream> io;///< the io stream 
        bool is_open{false};///< current observed/concluded state of the srv_connection
        srv_connection(string const&host_port,int timeout_ms=1000)
        :host_port{host_port},timeout_ms{timeout_ms}, io{make_unique<dlib::iosockstream>()}
        {}
        void open(int timeout_ms=1000){
            io->open(host_port, max(timeout_ms, this->timeout_ms));
            is_open=true;
        }
        void close(int timeout_ms=1000) {
            is_open=false; // if next line throws, consider closed anyway
            io->close(max(timeout_ms, this->timeout_ms));
        }
        void reopen(int timeout_ms=1000) {
            io->open(host_port, max(timeout_ms, this->timeout_ms));
            is_open=true;
        }
        // get rid of stuff we do not support (yet)
        srv_connection()=delete;
        srv_connection(srv_connection const&)=delete;
        srv_connection(srv_connection &&)=delete;
        srv_connection& operator=(srv_connection&)=delete;
        srv_connection& operator=(srv_connection&&)=delete;
        
    };



    /**@brief helper-class to enable 'autoconnect', lazy connect.
    *
    * Do some extra effort to establish connection, in case it's temporary down'
    * 
    */
    struct scoped_connect {
        srv_connection& sc;
        scoped_connect (srv_connection& srv_con):sc(srv_con){
            if (!sc.is_open ) { // auto-connect, and put some effort into succeeding
                bool rethrow=false;
                runtime_error rt_re("");
                    bool attempt_more=false;
                int retry_count=5;
                do {
                    try {
                        sc.open(); // either we succeed and finishes, or we get an exception
                        attempt_more=false;
                    } catch(const dlib::socket_error&se) {// capture socket error that might go away if we stay alive a little bit more
                        if(--retry_count>0 && strstr(se.what(),"unable to connect") ) {
                            attempt_more=true;
                            sleep_for(milliseconds(100));
                        } else {
                            rt_re=runtime_error(se.what());
                            rethrow=true;
                            attempt_more=false;
                        }
                    } catch(const exception&re) { // just give up this type of error,
                            rt_re=runtime_error(re.what());
                            rethrow=true;
                            attempt_more=false;
                    }
                } while(attempt_more);
                if(rethrow)
                    throw rt_re;
            }
        }
        ~scoped_connect() noexcept {
            // we don't disconnect, rather we try to fix broken connects.
        }
        scoped_connect (const scoped_connect&) = delete;
        scoped_connect ( scoped_connect&&) = delete;
        scoped_connect& operator=(const scoped_connect&) = delete;
        scoped_connect()=delete;
        scoped_connect& operator=(scoped_connect&&)=delete;
    };

    /** @brief utility to retry an io-operation that fails
    * 
    * The socket connection created from the client stays connected from the first use until explicitely 
    * closed by client.
    * The life time of the underlying socket *after* the close is ~ 120 seconds(windows, similar linux).
    * If the server is restarted we would enjoy that this layer 'auto-repair' with the server
    * if possible.
    * This routine ensures that this can happen for any io function sequence F.
    * 
    * @note that there is a requirement that f(sc) can be invoced several times 
    * without (unwanted) side-effects.
    * @tparam F a callable that accepts a srv_connection
    * 
    * @param sc srv_connection that's repairable
    * @param f the callable'
    */
    template <class F>
    void do_io_with_repair_and_retry(srv_connection&sc, F&&f) {
        for (int retry = 0; retry < 3; ++retry) {
            try {
                f(sc);
                return;
            } catch (const dlib::socket_error&) {
                sc.reopen();
            }
        }
        throw runtime_error("Failed to establish connection with " + sc.host_port);
    }

    /** @brief a client that matches the server for model type M 
    *
    * 
    * This class take care of message exchange to the remote server,
    * using the supplied connection parameters.
    * 
    * It implements the message protocol of the server, sending
    * message-prefix, arguments, waiting for response
    * deserialize the response and handle it back to the user.
    * 
    * @see server
    * 
    * @tparam M a serializeable model type
    *
    * 
    */
    template<class M>
    struct client {
        srv_connection c;
        client(){}
        client(string host_port,int timeout_ms=1000):c{host_port,timeout_ms}{}
        
        /** @brief provide model information
        *
        *  @param mids model-info identifiers of interest, if empty, return all
        *  @return a list of model-info
        */
        vector<model_info> get_model_infos(vector<int64_t>const & mids)  {
            // no validation needed on input args,
            // if mids.size()==0, it means give me all you have.
            // if >0, it will succeed all, or fail
            scoped_connect sc(c);
            vector<model_info> r;
            do_io_with_repair_and_retry(c,[this,&mids,&r](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::MODEL_INFO,io);
                core_oarchive oa(io, core_arch_flags);
                oa << mids;
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::MODEL_INFO) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return r;        
        }

        /** @brief store the model to backend with model-info
        *
        * Forward the model to the backend for storage.
        * 
        * @param m the model to store
        * @param mi the model-info to store
        * @return new model-id if m->id and m.id=0, otherwise returns the supplied model-id
        * 
        * @note We still consider to remove const, and set model-id to the obtained value
        */
        int64_t store_model( shared_ptr<M> const& m, model_info const &mi) {
            scoped_connect sc(c);
            int64_t r{0};
            do_io_with_repair_and_retry(c,[this,&m,&mi,&r](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::MODEL_STORE,io);
                core_oarchive oa(io, core_arch_flags);
                oa << m<<mi;
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::MODEL_STORE) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return r;
        }
        
        /** @brief read the model from server
        *
        * Read a model with specified model-id from server. The model with specified mid
        * must exists.
        * 
        * @param mid the model-id to read
        * @return new read model
        * 
        */
        shared_ptr<M> read_model(int64_t mid)  {
            return read_models(vector<int64_t>{mid})[0]; 
        }
        
        /** @brief read multiple models from server
        *
        * Read a models with specified model-id from server. The model with specified mid
        * must exists. This is done by first posting all requests to the server,
        * then reading back the responses.
        * 
        * @see read_model
        * 
        * @param mids the model-ids to read
        * @return list of read models
        * 
        */
        vector<shared_ptr<M>> read_models(vector<int64_t> mids) {
            if(mids.size()==0)
                throw runtime_error("List of model-ids must hold at least one element");
            for(auto const& m:mids)
                if(m<=0)
                    throw runtime_error("The supplied model-id must be >0");
            scoped_connect sc(c);
            vector<shared_ptr<M>> rr;
            do_io_with_repair_and_retry(c,[this,&mids,&rr](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                for(size_t i=0;i<mids.size();++i) { // first just send all requests
                    msg::write_type(message_type::MODEL_READ,io);
                    core_oarchive oa(io, core_arch_flags);
                    oa << mids[i];
                }
                for(size_t i=0; i<mids.size();++i) {// then read back each request
                    auto response_type = msg::read_type(io);
                    if (response_type == message_type::SERVER_EXCEPTION) {
                        auto re = msg::read_exception(io);
                        throw re;
                    } else if (response_type == message_type::MODEL_READ) {
                        core_iarchive ia(io, core_arch_flags);
                        shared_ptr<M> r;
                        ia >> r;
                        r->id=mids[i];//ensure consistency regardless content of file.
                        rr.emplace_back(r);
                    } else {
                        throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                    }
                }
            });
            return rr;            
        }
        /** @brief remove the model to backend with model-info
        *
        * Read a model with specified model-id from server. The model with specified mid
        * must exists.
        * 
        * @param mid the model-id to remove
        * @return error code ?
        * 
        */
        int64_t remove_model(int64_t mid) {
            if(mid<=0)
                throw runtime_error("remove_model require model-id arg mid >0");
            scoped_connect sc(c);
            int64_t r;
            do_io_with_repair_and_retry(c,[this,&mid,&r](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::MODEL_DELETE,io);
                core_oarchive oa(io, core_arch_flags);
                oa << mid;
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::MODEL_DELETE) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return r;
        }
        
        /** @brief update model-info
        *
        * Update the model-info of specified model, with id=mid.The model with specified mid
        * must exists.
        * 
        * @param mid the model-id to which we update the model-info on
        * @return true if succeeded (model exists, and was removed)
        * 
        */
        bool update_model_info(int64_t mid, model_info const &mi) {
            scoped_connect sc(c);
            bool r{false};
            do_io_with_repair_and_retry(c,[this,&mid,&mi,&r](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::MODEL_INFO_UPDATE,io);
                core_oarchive oa(io, core_arch_flags);
                oa << mid<<mi;
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::MODEL_INFO_UPDATE) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return r;
        }
        
        /** @brief close, until needed again, the server connection
        *
        */
        void close() {
            c.close();//just close-down connection, it will auto-open if needed
        }
    };
    
}
