/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <cstdint>
#include <exception>
#include <memory>
#include <regex>
#include <fstream>
#include <dlib/server.h>
#include <dlib/iosockstream.h>
#include <boost/filesystem.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <shyft/energy_market/srv/msg_defs.h>
#include <shyft/energy_market/srv/model_info.h>
#include <shyft/core/core_archive.h>
#include <shyft/core/utctime_utilities.h>

namespace shyft::energy_market::srv {

    namespace fs = boost::filesystem;
    using std::vector;
    using std::shared_ptr;
    using std::string;
    using std::to_string;
    using std::ifstream;
    using std::ofstream;
    using std::runtime_error;
    using shyft::core::utctime;
    using shyft::core::core_iarchive;
    using shyft::core::core_oarchive;


    /** @brief a server for serializable models, type M
    *
    * Currently using dlib server_iostream, 
    * and and utilizing the fact that ec-models,M, of any kind,
    * is serializable.
    * 
    * MI = model_info, is at least mode_id (int64_t), name json
    * 
    * The server keep a root_dir, so that models can be stored within
    * that directory, as ordninary files.
    *
    * The protocol provided is as simple as possible, matching the
    * typical repository for 'thing':
    * 
    * .get_model_infos(..some..criteria..model_id)->vector<model_info>
    * .store_model( MTYPE m, model_info)->int64_t (model_id)
    * .read_model(int64_t mid)->M
    * .remove_model(int64_t mid)->int64_t; remove result
    * .update_model_info(int64_t mdl_id,model_info)->bool;
    * 
    * The server delegates those calls to the underlying db-layer
    * 
    */
    //later like: template <class M,class MI> where M=stm_model, MI=stm_model_info
    template<class DB>
    struct server : dlib::server_iostream {
        using M=typename DB::model_t;
        DB db;///< the db-storage that this server provides
        // constructors
        server(string root_dir):db{root_dir} {}    
        server(server&&)=delete;
        server(const server&) =delete;
        server& operator=(const server&)=delete;
        server& operator=(server&&)=delete;
        ~server() =default;

        /** start the server in background, return the listening port used in case it was set unspecified */
        int start_server() {
            if(get_listening_port()==0) {
                start_async();
                while(is_running()&& get_listening_port()==0) //because dlib do not guarantee that listening port is set
                    std::this_thread::sleep_for(std::chrono::milliseconds(10)); // upon return, so we have to wait until it's done
            } else {
                start_async();
            }
            return get_listening_port();
        }

        /**@brief handle one client connection 
        *
        * Reads messages/requests from the clients,
        * - act and perform request,
        * - return response
        * for as long as the client keep the connection 
        * open.
        * 
        */
        void on_connect(
            std::istream & in,
            std::ostream & out,
            const std::string & /*foreign_ip*/,
            const std::string & /*local_ip*/,
            unsigned short /*foreign_port*/,
            unsigned short /*local_port*/,
            dlib::uint64 /*connection_id*/
        ) {

            using shyft::core::core_iarchive;
            using shyft::core::core_oarchive;
            
            while (in.peek() != EOF) {
                auto msg_type= msg::read_type(in);
                try { // scoping the binary-archive could be ok, since it forces destruction time (considerable) to taken immediately, reduce memory foot-print early
                    //  at the cost of early& fast response. I leave the commented scopes in there for now, and aim for fastest response-time
                    switch (msg_type) { // currently switch, later maybe table[msg_type]=msg_handler
                        case message_type::MODEL_INFO:{
                            core_iarchive ia(in,core_arch_flags);
                            vector<int64_t> mdl_ids;// empty or model-ids to read_string
                            ia>>mdl_ids;
                            auto result=db.get_model_infos(mdl_ids);// get result
                            msg::write_type(message_type::MODEL_INFO,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;
                        case message_type::MODEL_INFO_UPDATE:{
                            core_iarchive ia(in,core_arch_flags);
                            int64_t mid;model_info mi;
                            ia>>mid>>mi;
                            auto result=db.update_model_info(mid,mi);// get result
                            msg::write_type(message_type::MODEL_INFO_UPDATE,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;
                        case message_type::MODEL_STORE:{
                            core_iarchive ia(in,core_arch_flags);
                            model_info mi;
                            shared_ptr<M> m;
                            ia>>m>>mi;
                            auto result=db.store_model(m,mi);// get result
                            msg::write_type(message_type::MODEL_STORE,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;
                        case message_type::MODEL_READ:{
                            core_iarchive ia(in,core_arch_flags);
                            int64_t mid;
                            ia>>mid;
                            auto result=db.read_model_blob(mid);// get result
                            msg::write_type(message_type::MODEL_READ,out);// then send
                            out.write(result.data(),result.size());
                            //core_oarchive oa(out,core_arch_flags);
                            //oa<<result;
                        } break;
                        case message_type::MODEL_DELETE:{
                            core_iarchive ia(in,core_arch_flags);
                            int64_t mid;
                            ia>>mid;
                            auto result=db.remove_model(mid);// get result
                            msg::write_type(message_type::MODEL_DELETE,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;
                    
                        // other 
                        default:
                            throw std::runtime_error(std::string("Server got unknown message type:") + std::to_string((int)msg_type));
                    }
                } catch (std::exception const& e) {
                    msg::send_exception(e,out);
                }
            }
        }
    };
    
}
