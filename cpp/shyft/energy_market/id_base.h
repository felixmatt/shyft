/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <shyft/core/core_serialization.h>

namespace shyft::energy_market {

    using std::string;

    /**
     * @brief 
     */
    struct em_handle{
        typedef void(*destroy_t)(void*);
        void *obj{nullptr};
        
        em_handle(){};
        explicit em_handle(void*o):obj{o}{}
        static destroy_t destroy;
        
        em_handle(const em_handle&)=delete;
        em_handle& operator=(em_handle const&)=delete;
        
        em_handle(em_handle&& o) {
            obj=o.obj;
            o.obj=nullptr;
        }
        
        em_handle& operator=(em_handle&& o) {
            if(&o != this) {
                cleanup();
                obj=o.obj;o.obj=nullptr;
            }
            return *this;    
        }
        
        
        ~em_handle(){
            cleanup();
        }
    private:
        void cleanup() {
            if(obj && destroy){
                (*destroy)(obj);
                obj=nullptr;
            } 

        }
    };

    /**
    * @brief identity and common stuff for python enabled components
    * 
    * Usually the id is unique within the scope (e.g. area, hydro-power-system)
    * The name could be unique,
    * The json is for storing arbitrary data, that is primarly handled in python.
    */
    struct id_base {
        int64_t id{0};///< unique, in the scope of context (type/level) etc.
        string name;///< the name of the object
        string json;///< json, to be used by the python-side
        em_handle h;///< handle to external python objects
        bool operator==(const id_base&o) const {return id==o.id && name== o.name && json == o.json;}
        bool operator!=(const id_base&o) const {return !operator==(o);}
        bool operator <(const id_base&o) const {return name < o.name;}
        
        //--- python support
        int64_t get_id() const {return id;}
        void set_id(int64_t v) {id=v;}
        
        const string& get_name() const {return name;}
        void set_name(const string& v) {name=v;}
        
        const string& get_json() const {return json;}
        void set_json(const string& v) {json=v;}
        
        x_serialize_decl();
    };
    
};
x_serialize_export_key(shyft::energy_market::id_base);
