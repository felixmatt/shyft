/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
//
// 1. first include std stuff and the headers for
// files with serializeation support
//

#include <shyft/energy_market/id_base.h>

#include <shyft/energy_market/market/model.h>
#include <shyft/energy_market/market/model_area.h>
#include <shyft/energy_market/market/power_line.h>
#include <shyft/energy_market/market/power_module.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/power_station.h>
#include <shyft/energy_market/hydro_power/water_route.h>
#include <shyft/energy_market/hydro_power/catchment.h>
#include <shyft/energy_market/srv/model_info.h>
#include <shyft/energy_market/srv/run.h>
#include <shyft/core/core_archive.h>
#include <shyft/core/utctime_utilities.h>

// then include stuff you need like vector,shared, base_obj,nvp etc.
#include <boost/format.hpp>

#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/unique_ptr.hpp>
#include <boost/serialization/weak_ptr.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/variant.hpp>
#include <boost/serialization/nvp.hpp>

//
// 2. Then implement each class serialization support
//

namespace shyft::energy_market{
    
    em_handle::destroy_t em_handle::destroy=nullptr; 
    
}

using namespace boost::serialization;

template <class Archive>
void shyft::energy_market::srv::model_info::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("id",id)
    & make_nvp("name",name)
    & make_nvp("created",created)
    & make_nvp("json",json)
    ;
}

template <class Archive>
void shyft::energy_market::srv::run::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("id",id)
    & make_nvp("name",name)
    & make_nvp("created",created)
    & make_nvp("json",json)
    & make_nvp("mid",mid)
    & make_nvp("state",state)
    ;
}


template <class Archive>
void shyft::energy_market::id_base::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("id",id)
    & make_nvp("name",name)
    & make_nvp("json",json)
    ;
}


template <class Archive>
void shyft::energy_market::market::model::serialize(Archive & ar, const unsigned int file_version) {
    ar
        & make_nvp("id",id)
        & make_nvp("name",name)
        & make_nvp("json",json)
        & make_nvp("created",created)
        & make_nvp("area",area)
        & make_nvp("power_lines",power_lines)
        //& make_nvp("power_type_map",power_type_map)
        //& make_nvp("load_type_map",load_type_map)
        ;
}

template <class Archive>
void shyft::energy_market::market::model_area::serialize(Archive & ar, const unsigned int file_version) {
    ar
        & make_nvp("id", id)
        & make_nvp("name", name)
        & make_nvp("json",json)
        & make_nvp("power_modules", power_modules)
        & make_nvp("hps",detailed_hydro)
        ;
}

template <class Archive>
void shyft::energy_market::market::power_line::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("id", id)
    & make_nvp("name", name)
    & make_nvp("json",json)
    & make_nvp("area_1", area_1)
    & make_nvp("area_2", area_2)
    ;
}

template <class Archive>
void shyft::energy_market::market::power_module::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("id", id)
    & make_nvp("name", name)
    & make_nvp("json",json)
    ;
}


template <class Archive>
void shyft::energy_market::hydro_power::point::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("x", x)
    & make_nvp("y",y)
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::xy_point_curve::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("points", points)
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::xy_point_curve_with_z::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("z", z)
    & make_nvp("xy_curve", xy_curve)
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::turbine_efficiency::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("production_min", production_min)
    & make_nvp("production_max", production_max)
    & make_nvp("efficiency_curves", efficiency_curves)
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::turbine_description::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("efficiencies", efficiencies)
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::hydro_power_system::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("id", id)
    & make_nvp("name",name)
    & make_nvp("created",created)
    & make_nvp("reservoirs",reservoirs)
    & make_nvp("aggregates",units )
    & make_nvp("water_routes",waterways )
    & make_nvp("catchments", catchments)
    & make_nvp("power_stations",power_plants )
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::hydro_connection::serialize(Archive & ar, const unsigned int file_version) {
    ar
        & make_nvp("role", role)
        & make_nvp("target", target)
        ;
}

template <class Archive>
void shyft::energy_market::hydro_power::hydro_component::serialize(Archive & ar, const unsigned int file_version) {
    ar 
        & make_nvp("hps", hps)
        & make_nvp("id",id)
        & make_nvp("name", name)
        & make_nvp("ds", downstreams)
        & make_nvp("us", upstreams)
        ;
}

template <class Archive>
void shyft::energy_market::hydro_power::reservoir::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("hc", base_object<shyft::energy_market::hydro_power::hydro_component>(*this))
    & make_nvp("id", id)
    & make_nvp("name", name)
    & make_nvp("json", json)
    ;
}


template <class Archive>
void shyft::energy_market::hydro_power::unit::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("hc", base_object<shyft::energy_market::hydro_power::hydro_component>(*this))
    & make_nvp("id", id)
    & make_nvp("name", name)
    & make_nvp("json", json)
    & make_nvp("station",pwr_station)
    
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::power_plant::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("id_base",base_object<shyft::energy_market::id_base>(*this))
    & make_nvp("hps", hps)
//    & make_nvp("id", id)
//    & make_nvp("name", name)
//    & make_nvp("json", json)
    & make_nvp("aggregates",units)
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::waterway::serialize(Archive& ar, const unsigned int file_version) {
    ar
    & make_nvp("hc", base_object<shyft::energy_market::hydro_power::hydro_component>(*this))
    & make_nvp("id", id)
    & make_nvp("name", name)
    & make_nvp("json", json)
	& make_nvp("gates",gates)
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::gate::serialize(Archive& ar, const unsigned int file_version) {
	ar
		& make_nvp("wtr", wtr)
		& make_nvp("id", id)
		& make_nvp("name", name)
		& make_nvp("json", json)
		;
}

template <class Archive>
void shyft::energy_market::hydro_power::catchment::serialize(Archive& ar, const unsigned int file_version) {
	ar
    & make_nvp("id", id)
    & make_nvp("name", name)
    & make_nvp("json", json)
    & make_nvp("hps", hps)
    ;
}

//
// 3. Then export class serialization support
//
//x_serialize_implement(shyft::energy_market::core::utcperiod);
x_serialize_implement(shyft::energy_market::id_base);
x_serialize_implement(shyft::energy_market::srv::model_info);
x_serialize_implement(shyft::energy_market::srv::run);
x_serialize_implement(shyft::energy_market::market::model);
x_serialize_implement(shyft::energy_market::market::model_area);
x_serialize_implement(shyft::energy_market::market::power_line);
x_serialize_implement(shyft::energy_market::market::power_module);
x_serialize_implement(shyft::energy_market::hydro_power::point);
x_serialize_implement(shyft::energy_market::hydro_power::xy_point_curve);
x_serialize_implement(shyft::energy_market::hydro_power::xy_point_curve_with_z);
x_serialize_implement(shyft::energy_market::hydro_power::turbine_efficiency);
x_serialize_implement(shyft::energy_market::hydro_power::turbine_description);
x_serialize_implement(shyft::energy_market::hydro_power::reservoir);
x_serialize_implement(shyft::energy_market::hydro_power::hydro_connection);
x_serialize_implement(shyft::energy_market::hydro_power::hydro_component);
x_serialize_implement(shyft::energy_market::hydro_power::hydro_power_system);

x_serialize_implement(shyft::energy_market::hydro_power::unit);

x_serialize_implement(shyft::energy_market::hydro_power::power_plant );
x_serialize_implement(shyft::energy_market::hydro_power::waterway );
x_serialize_implement(shyft::energy_market::hydro_power::gate);
x_serialize_implement(shyft::energy_market::hydro_power::catchment);

//
// 4. Then include the archive supported
//
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

// repeat template instance for each archive class
#define xx_arch(T) x_serialize_archive(T,boost::archive::binary_oarchive,boost::archive::binary_iarchive)
xx_arch(shyft::energy_market::id_base);
xx_arch(shyft::energy_market::srv::model_info);
xx_arch(shyft::energy_market::srv::run);
xx_arch(shyft::energy_market::market::model);
xx_arch(shyft::energy_market::market::model_area);
xx_arch(shyft::energy_market::market::power_line);
xx_arch(shyft::energy_market::market::power_module);
xx_arch(shyft::energy_market::hydro_power::point);
xx_arch(shyft::energy_market::hydro_power::xy_point_curve);
xx_arch(shyft::energy_market::hydro_power::xy_point_curve_with_z);
xx_arch(shyft::energy_market::hydro_power::turbine_efficiency);
xx_arch(shyft::energy_market::hydro_power::turbine_description);
xx_arch(shyft::energy_market::hydro_power::reservoir);
xx_arch(shyft::energy_market::hydro_power::hydro_connection);
xx_arch(shyft::energy_market::hydro_power::hydro_component);
xx_arch(shyft::energy_market::hydro_power::hydro_power_system);
xx_arch(shyft::energy_market::hydro_power::unit);
xx_arch(shyft::energy_market::hydro_power::power_plant);
xx_arch(shyft::energy_market::hydro_power::waterway);
xx_arch(shyft::energy_market::hydro_power::gate);
xx_arch(shyft::energy_market::hydro_power::catchment);
