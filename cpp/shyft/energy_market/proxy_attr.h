/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <type_traits>

namespace shyft::energy_market::core {

    /** @brief a proxy class for attributes held in input-data-sets
    *
    * This class helps us keep attributes to a one-line declarative statement,
    * within the scope of the relevant class,
    * providing useful, type-safe, access-safe path to the values.
    *
    * -as if they where declared as simple 'pods'.
    *
    *
    * @tparam T the type of the owning object, like reservoir
    * @tparam V the value type of the attribute
    * @tparam A the attribute enum type as specified for the dataset specific enum.
    * @tparam a the  attribute id of type A
    * @tparam C the class that provides the attribute container that must have
    *           the methods:
    *           V .get_attr<V>(oid,aid)
    *             .set_attr(oid,aid,x)
    *             .exists(oid,aid)
    *  
    */
    template <typename T, typename V, typename A, A a, typename C>
    struct proxy_attr { 
        using value_type=V;// export this name so that we easily can use proxy_attr::value_type in the fu:: patrts.
        T* o={nullptr}; ///< reference to the owning object, like reservoir, aggregate, water_route etc.
        proxy_attr()=default;//python exposure needs it
        explicit proxy_attr(T&ro):o{&ro} {}
        /** return the Attribute Enum type constant for this proxy_attr */
        constexpr A ae() const {return a;}
        
        /** handle assignment from type V, forward to dataset, like rsv.hrv=123.2 */
        void operator=(const V & x) { 
                C::ds(o).set_attr(o->id, a, x); 
        }

        /** handel read/get, as in double v=rsv.hrv */
        operator V() const { 
            return C::ds(o).template get_attr<V>(o->id, a);
        }

        /** provide a probe to check if the attribute exists for this object in the dataset collection */
        bool exists() const { 
            return C::ds(o).template has_attr<V>(o->id, a); 
        }

        /**python set v*/
        void set(const V& x) { *this=x;}
        /**python get v*/
        V get() const {return (V)(*this);}
        
        /** removes the element, returns bool if the call resulted in a removal, false if element not found */
        bool remove() {
            return C::ds(o).template remove_attr<V>(o->id,a);
        }
        bool operator==(const proxy_attr&o) const {
            if(exists() == o.exists()) {
                if(!exists())
                    return true;// both are away/not set, evaluate to equal
                return get()==o.get();/// compare values
            }
            return false;
        }
        bool operator!=(const proxy_attr&o) const {return !operator==(o);}
    }; 

    /** @brief  Provide &C::proxy_attr member pointer from enum
    *
    * @detail 
    * Given attribute enum and containing class C provide the member pointer of a proxy_attr 
    * The class C should provide the nested struct, like
    * 
    * struct a_map {
    *    static constexpr auto ref(std::integral_constant<E,e>) noexept { return &C::member;}
    *    //... repeated for all exposed members and enums.
    * };
    *
    * Example: lets say we want a function that give class C and enum E e, want to 
    * check that the proxy attribute exists, then we can access the members like this:
    * 
    *  template<typename C, typename E, E  e>
    *  bool proxy_attr_exists(C *r) noexcept {
    *   return (r->*proxy_attr_m<C,E,e>::member()).exists();
    * }
    * 
    * Combining this with integer_sequence<...>,
    * we can roll out compile-time tables with funcion-pointers that allow runtime access
    * to compile-time functions using the attribute enum as key, like this:
    * 
    *  template<typename C,typename E, int...a> 
    *  constexpr std::array<bool(*)(C *),sizeof...(a)>
    *  cmake_attr_exists_table(std::integer_sequence<int,a...>) { 
    *     return {{&proxy_attr_exists<C,E,static_cast<E>(a)>...}};// generates a list of fx-pointers from the above exists template
    * }
    *
    * constexpr auto cattr_exists_table =cmake_attr_exists_table<stm::reservoir,stm:rsv_attr>( std::make_integer_sequence<int,int(stm::rsv_attr::size)>{});
    *
    * and then use it runtime like:
    * 
    * bool rsv_attr_exists(stm::reservoir *r, stm::rsv_attr a) {
    *   return cattr_exists_table[int(a)](r);
    * }
    *  
    * @note If we upgrade the attribute enum to a meta-class instead, provided as integral constants, then we could make this solution more elegant.
    * 
    * @tparam C the containing class like reservoir, water_route
    * @tparam E the enum type used for external enumerating the attributes,e.g. rsv_attr
    * @tparam E e the enum-member for which we would provide the member address, e.g. rsv_attr::volume_description
    *
    */
    template <typename C,typename E,E e>
        struct proxy_attr_m {
        static constexpr auto member() noexcept {
            return C::a_map::ref(std::integral_constant<E,e>{});//notice we instantiate integral_constant to select correct impl.
        }
    };


    
}
