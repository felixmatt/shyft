/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <vector>
#include <memory>
#include <string>

#include <shyft/core/utctime_utilities.h>
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/catchment.h>

namespace shyft::energy_market::hydro_power {

    using shyft::core::utctime;
    using shyft::core::no_utctime;
    using std::vector;
    using std::string;
    using std::shared_ptr;
    using std::weak_ptr;
    using std::make_shared;
            
    /** @brief A Hydro Power System, hydro-components and their relations 
    *
    * A Hydro power system consists of its components:
    *        Reservoirs(including creek/inlets/buffer-reservoirs etc.) 
    *        PowerStations(pure|pump|both) 
    *        WaterRoutes (tunnel|rivers) provide the hydrological interconnections between Reservoirs/PowerStations
    * 
    * From the above, we can conclude that it can have one or several *watercourses*.
    * 
    * A watercourse is a hydrological connected system (and a Hydro Power System could have several of those).
    * 
    * A *valid* watercourse consist of a minimum of a reservoir_ and a aggregate_(pure generator) interconnected by waterway_(tunnel).
    * 
    * Discussion: Should we allow a waterway_ to have 'null' output, or should we insist on connect to an 'ocean' reservoir_ object ?
    *             Inflow(regulated/unregulated) is the *input* to the system (injected at Reservoirs), at some point later, 
    *             it leaves our system model after passing through  Reservoirs, WaterRoutes 
    *             and hopefully also some PowerStations.
    *             Should we enforce that it leaves into the 'ocean' ?
    * 
    *        Associated with the hydro power system we also have the WatermarkSeries.
    */
    struct hydro_power_system {

        int64_t id=0; ///< unique id for the hps this instance try to model
        std::string name;///< it might have a meaningful name, like Ulla-førre, Nea-vassdraget
        utctime created=no_utctime;///< keep track of when it is created (or persisted first time)
        vector<reservoir_> reservoirs; ///< simply the reservoirs in the hps, they might or might not be connected
        vector<unit_> units;///< the units(generator,turbine) in the hps, 
        vector<waterway_> waterways;///< tunnels and rivers that have the water-flow
        vector<catchment_> catchments;///< represents the catchments, they are in turn connected to reservoirs
        vector<power_plant_> power_plants;///< represents the power-stations, -they have references to units
        hydro_power_system()=default;
        explicit hydro_power_system(string a_name);
        hydro_power_system(int id,const string&name):id{id},name{name}{}
        virtual ~hydro_power_system();
        void clear();
        reservoir_ find_reservoir_by_name(string name) const;
        unit_ find_unit_by_name(string name) const;
        power_plant_ find_power_plant_by_name(string name) const;
        waterway_ find_waterway_by_name(string name)const;
        catchment_ find_catchment_by_name(string name) const;

        reservoir_ find_reservoir_by_id(int64_t id) const;
        unit_ find_unit_by_id(int64_t id) const;
        power_plant_ find_power_plant_by_id(int64_t id) const;
        waterway_ find_waterway_by_id(int64_t id)const;
        catchment_ find_catchment_by_id(int64_t id) const;

        bool equal_structure(hydro_power_system const& other) const;

        static std::string to_blob(hydro_power_system_ const& me) ;
        static hydro_power_system_ from_blob(std::string xmls);
        x_serialize_decl();
    };

    /** @brief The hydro-power-system-builder helps building and modififying a hp-system
    *
    * It helps exposing the building blocks to python in a minimalistic and
    * safe way. 
    * The intention is ensure that the user (at python level), follows rules,
    * and capabilities present in the system.
    *
    * We *could* make semantic builders for different processes (like LTM/STM) to 
    * ensure only 'valid models within that context' is built. But it might be better
    * to rather put that effort into validator/transformators pr. process/domain,
    * since it is possible to transform detailed models into more simplified models using rules.
    * 
    */
    struct hydro_power_system_builder {
        hydro_power_system_ hps;
        hydro_power_system_builder(hydro_power_system_ & hps):hps(hps) {}
        reservoir_ create_reservoir(int id,const string& name, const string& json="");
        unit_ create_unit(int id,const string& name, const string& json="");
        waterway_ create_waterway(int id,const string& name, const string& json="");
        waterway_ create_tunnel(int id,const string& name, const string& json="");
        waterway_ create_river(int id,const string& name, const string& json="");
        catchment_ create_catchment(int id,const string& name, const string& json="");
        power_plant_ create_power_plant(int id,const string& name,const string& json="");
    };

}
x_serialize_export_key(shyft::energy_market::hydro_power::hydro_power_system);
