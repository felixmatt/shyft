/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/energy_market/hydro_power/water_route.h>
#include <shyft/energy_market/hydro_power/power_station.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/em_utils.h>
#include <iostream>

namespace shyft::energy_market::hydro_power {

    waterway_ waterway::input_from( waterway_ const&me,waterway_ const& w) {
        connect(w, me); 
        return me; 
    }
            
    waterway_ waterway::input_from( waterway_ const& me,unit_ const& p) {
        connect(p, me); 
        return me; 
    }
            
    waterway_ waterway::input_from( waterway_ const& me, reservoir_ const& r, connection_role role) {
        connect(r,role,me);
        return me; 
    }
            
    waterway_ waterway::input_from( waterway_ const& me, reservoir_ const& r) {
        connect(r,connection_role::main, me);
        return me;
    }
            
    waterway_ waterway::output_to( waterway_ const& me, waterway_ const& w) {
        connect(me, w);
        return me;
    }
            
    waterway_ waterway::output_to( waterway_ const& me, reservoir_ const& r) {
        connect(me, r); 
        return me; 
    }

    waterway_ waterway::output_to( waterway_ const& me, unit_ const& p) {
        connect(me, p);
        return me;
    }

    bool waterway::operator==( waterway const& o)const {
        if (!id_base::operator==(o))
            return false;
        return equal_vector_content(gates, o.gates);
    }

    void waterway::add_gate( waterway_ const &w, gate_ g) {
        if (g && g->wtr_()) {
            throw std::runtime_error("This gate is already part of '" + g->wtr_()->name + "', remove the gate from that oject first");
        }
        auto f = find(begin(w->gates), end(w->gates), g);
        if (f == end(w->gates)) {
            g->wtr = w;// uplink ref. here
            w->gates.emplace_back(move(g));
        }
    }
    void waterway::remove_gate_ptr(gate*g) {
        if (!g)return;
        auto f = find_if(begin(gates), end(gates), [g](const auto&x) {return x.get() == g; });
        if (f != end(gates)) {
            g->wtr.reset();// remove uplink ref. here.
            gates.erase(f);
        }
    }
    waterway::~waterway() {
        for (auto&g : gates) // remove uplink to this prior to vector delete
            g->wtr.reset();
    }

    gate::gate() = default;
    gate::~gate() {
    }

    gate::gate(int id, const string&name, const string&json) 
        :id_base{ id, name, json } {

    }
}
