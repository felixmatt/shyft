/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <stdexcept>
#include <shyft/energy_market/hydro_power/power_station.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/water_route.h>
#include <boost/format.hpp>
#include <shyft/energy_market/em_utils.h>

namespace shyft::energy_market::hydro_power {

    using std::runtime_error;
    using std::dynamic_pointer_cast;

    unit_ unit::input_from(unit_ const&me, const waterway_& w) {
        connect(w, me);
        return me;
    }
    unit_ unit::output_to(unit_ const&me, const waterway_& w) {
        connect(me, w);
        return me;
    }
    waterway_ unit::downstream() const {
        return downstreams.size() ? dynamic_pointer_cast<waterway>(downstreams.front().target_()) : nullptr;
    }
    waterway_ unit::upstream() const {
        return upstreams.size() ? dynamic_pointer_cast<waterway>(upstreams.front().target_()) : nullptr;
    }
    vector<reservoir_> unit::upstream_reservoirs() const {
        vector<reservoir_>r;
        return upstream_reservoir_closure(upstream(), r);
    }
    reservoir_ unit::downstream_reservoir() const {
        return downstream_reservoir_closure(downstream());
    }
    reservoir_ unit::downstream_reservoir_closure(const waterway_& w) const {
        if (w == nullptr || w->downstream() == nullptr || dynamic_pointer_cast<unit>(w->downstream()))
            return nullptr;
        if (dynamic_pointer_cast<reservoir>(w->downstream()))
            return dynamic_pointer_cast<reservoir>(w->downstream());
        return downstream_reservoir_closure(dynamic_pointer_cast<waterway>(w->downstream()));
    }
    vector<reservoir_> unit::upstream_reservoir_closure(const waterway_& w, vector<reservoir_> &r) const {
        if (w == nullptr)
            return r;
        for (const auto& us : w->upstreams) {
            if (dynamic_pointer_cast<waterway>(us.target_()))
                upstream_reservoir_closure(dynamic_pointer_cast<waterway>(us.target_()), r);
            if (dynamic_pointer_cast<reservoir>(us.target_()))
                r.push_back(dynamic_pointer_cast<reservoir>(us.target_()));
        }
        return r;
    }

    void power_plant::add_unit(const power_plant_& ps,const unit_ &a) {
        a->pwr_station=ps;
        ps->units.push_back(a);
    }
    void power_plant::remove_unit(const unit_& a) {
        auto f= find_if(begin(units),end(units),[&a](const auto&p)->bool {return a==p;});
        if(f !=end(units)) {
            (*f)->pwr_station.reset();
            units.erase(f);
        }
    }

    bool unit::operator==(unit const& o) const {
        return  id_base::operator==(o);
    }
    bool power_plant::operator==( power_plant const& o) const {
        if (!id_base::operator==(o))
            return false;
        return equal_vector_content(units,o.units);
    }

    power_plant::~power_plant(){
        for(auto&a:units)
            if(a) a->pwr_station.reset();
    }

}
