/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <sstream>
#include <stdexcept>

#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/power_station.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/water_route.h>
#include <shyft/energy_market/hydro_power/catchment.h>
#include <boost/format.hpp>
// serialization for hydro power system
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/weak_ptr.hpp>
#include <boost/serialization/nvp.hpp>
#include <shyft/energy_market/em_utils.h>



namespace shyft::energy_market::hydro_power {

    using std::runtime_error;
    using std::map;
    using std::istringstream;
    using std::ostringstream;
            
    hydro_power_system::hydro_power_system(string a_name) :name(a_name) {}
    void hydro_power_system::clear() {
        for (auto&c : reservoirs)
            c->clear();
        for (auto&c : waterways)
            c->clear();
        for (auto&c : units )
            c->clear();
        for (auto&c : catchments)
            c->clear();
        power_plants.clear();
        reservoirs.clear();
        waterways.clear();
        units.clear();
        catchments.clear();
    }
    hydro_power_system::~hydro_power_system() {
        clear();
    }
    reservoir_ hydro_power_system_builder::create_reservoir(int id,const string& name, const string& json) {
        if (any_of(begin(hps->reservoirs), end(hps->reservoirs), [&name](const auto&r)->bool {return r->name == name;}))
            throw runtime_error((boost::format("reservoir name must be unique within a hydro_power_system:%1%")%name).str());//is that reasonable ?
        auto r = make_shared<reservoir>(id,name,json,hps);
        hps->reservoirs.push_back(r);
        return r;
    }

    unit_ hydro_power_system_builder::create_unit (int id,const string& name, const string& json) {
        if (any_of(begin(hps->units ), end(hps->units ), [&name](const auto& p)->bool {return p->name == name;}))
            throw runtime_error((boost::format("aggregate_ name must be unique within a HydroPowerSystem:%1%")% name).str());//is that reasonable ?
        auto p = make_shared<unit>(id,name, json,hps);
        hps->units.push_back(p);
        return p;
    }
    power_plant_ hydro_power_system_builder::create_power_plant (int id,const string& name, const string& json) {
        if (any_of(begin(hps->power_plants ), end(hps->power_plants ), [&name](const auto& p)->bool {return p->name == name;}))
            throw runtime_error((boost::format("power_station name must be unique within a HydroPowerSystem:%1%")% name).str());//is that reasonable ?
        auto p = make_shared<power_plant>(id,name, json,hps);
        hps->power_plants.push_back(p);
        return p;
    }

    waterway_ hydro_power_system_builder::create_waterway(int id,const string& name, const string& json) {
        if (any_of(begin(hps->waterways), end(hps->waterways), [&name](const auto&w)->bool {return w->name == name;}))
            throw runtime_error((boost::format("waterway_ name must be unique within a HydroPowerSystem:%1%")% name).str());//is that reasonable ?
        auto w = make_shared<waterway>(id,name, json,hps);
        hps->waterways.push_back(w);
        return w;
    }

    catchment_ hydro_power_system_builder::create_catchment(int id,const string& name, const string& json) {
        if (any_of(begin(hps->catchments), end(hps->catchments), [&name](const auto&w)->bool {return w->name == name; }))
            throw runtime_error((boost::format("catchment name must be unique within a HydroPowerSystem:%1%") % name).str());//is that reasonable ?
        auto w = make_shared<catchment>(id,name, json, hps);
        hps->catchments.push_back(w);
        return w;
    }

    waterway_ hydro_power_system_builder::create_tunnel(int id,const string& name, const string& json) {
        return create_waterway(id,name,json);
    }

    waterway_ hydro_power_system_builder::create_river(int id,const string& name, const string& json) {
        return create_waterway(id,name,json);
    }

    template<class T>
    static shared_ptr<T> find_by_name(vector<shared_ptr<T>>const &v, string name) {
        auto f = find_if(begin(v), end(v), [name](auto const&r)->bool {return r->name == name; });
        return f != v.end() ? *f : nullptr;
    }
    template<class T>
    static shared_ptr<T> find_by_id(vector<shared_ptr<T>>const &v, int64_t id) {
        auto f = find_if(begin(v), end(v), [id](auto const&r)->bool {return r->id == id; });
        return f != v.end() ? *f : nullptr;
    }

    reservoir_ hydro_power_system::find_reservoir_by_name(string name) const {return find_by_name(reservoirs, name);}
    unit_ hydro_power_system::find_unit_by_name (string name) const {return find_by_name( units, name);}
    power_plant_ hydro_power_system::find_power_plant_by_name (string name) const {return find_by_name( power_plants, name);}
    waterway_ hydro_power_system::find_waterway_by_name(string name) const {return find_by_name(waterways, name);}
    catchment_ hydro_power_system::find_catchment_by_name(string name) const {return find_by_name(catchments, name);}

    reservoir_ hydro_power_system::find_reservoir_by_id(int64_t id) const { return find_by_id(reservoirs, id); }
    unit_ hydro_power_system::find_unit_by_id (int64_t id) const { return find_by_id( units, id); }
    power_plant_ hydro_power_system::find_power_plant_by_id (int64_t id) const { return find_by_id( power_plants, id); }
    waterway_ hydro_power_system::find_waterway_by_id(int64_t id) const { return find_by_id(waterways, id); }
    catchment_ hydro_power_system::find_catchment_by_id(int64_t id) const { return find_by_id(catchments, id); }


    /* Verify that system a and b are structurally 
    * equal.
    * This means almost all attributes& relations, 
    * exceptions are typically db-identifiers/created/modified time.
    */
    bool hydro_power_system::equal_structure(hydro_power_system const & b) const {
        const hydro_power_system& a = *this;
        // skip/ignore the name for now

        if (a.units.size() != b.units.size())
            return false;
        if (a.reservoirs.size() != b.reservoirs.size())
            return false;
        if (a.waterways.size() != b.waterways.size())
            return false;
        if (a.catchments.size() != b.catchments.size())
            return false;
        if (a.power_plants.size() != b.power_plants.size())
            return false;

        
        if(!equal_vector_content( power_plants,b.power_plants ))
            return false;

        if(!equal_vector_content( units,b.units ))
            return false;
        
        if(!equal_vector_content(reservoirs,b.reservoirs))
            return false;

        if(!equal_vector_content(waterways,b.waterways))
            return false;

        if(!equal_vector_content(catchments,b.catchments))
            return false;
        
        return true;
    }

    std::string hydro_power_system::to_blob(hydro_power_system_ const &hps) {
        using namespace std;
        ostringstream xmls;
        {
            boost::archive::binary_oarchive oa(xmls);
            oa << boost::serialization::make_nvp("hps", hps);
        }
        xmls.flush();
        return xmls.str();
    }

    hydro_power_system_ hydro_power_system::from_blob(std::string xmls) {
        shared_ptr<hydro_power_system> hps;
        istringstream xmli(xmls); {
        boost::archive::binary_iarchive ia(xmli);
        ia >> boost::serialization::make_nvp("hps", hps);
        }
        return hps;
    }

}
