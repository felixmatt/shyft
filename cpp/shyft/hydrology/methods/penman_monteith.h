/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/hydrology/hydro_functions.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::core::penman_monteith {
    using namespace std;
    using namespace shyft::core::hydro_functions;

    /** @brief Evapotranspiration models
    *
    * Penman-Monteith, ref.: ASCE-EWRI The ASCE Standardized Reference Evapotranspiration Equation Environmental and
    * Water Resources Institute (EWRI) of the American Society of Civil Engineers Task Com- mittee on Standardization of
    * Reference Evapotranspiration Calculation, ASCE, Washington, DC, Environmental and Water Resources Institute (EWRI) of
    * the American Society of Civil Engineers Task Committee on Standardization of Reference Evapotranspiration Calculation,
    * ASCE, Washington, DC, 2005
    */

    /** ASCE-EWRI hourly based model
    * it is stated taht this model can be used with the smaller periods as well
    */
    struct parameter{
        double height_ws = 2.0; // [m], height of windspeed measurements
        double height_t = 2.0; // [m], height of temperature and relative humidity measurements
        double height_veg = 0.15; // vegetation height, [m] (grass from MORECS)
        double rl = 72.0; // effective stomatal resistance, [s/m] (based on asce-ewri constants for short crop rs(50)*24*hveg(0.12))
        bool full_model = false;
        parameter(double height_veg=0.15, double height_ws = 2.0, double height_t = 2.0,  double rl=72.0, bool full_model=false) : height_ws(height_ws), height_t(height_t),height_veg(height_veg), rl(rl), full_model(full_model) {}
    };

    struct response {
        double et_ref = 0.0; // reference evapotranspiration [mm/h]
        double soil_heat = 0.0;
    };

    template<class P, class R>
    struct calculator {
        P param;

        explicit calculator(const P &p) : param(p) {}

        /** @brief computes ASCE PENMAN-MONTEITH reference evapotranspiation, [mm/h] (standard or full based on full_model boolean)
        * @param response -- updating response structure
        * @param net_radiation, [MJ/m2/h] -- net radiation
        * @param temperature [degC] -- air temperature
        * @param rhumidity [-] -- relative humidity
        * @param elevation [m]
        * @param windspeed [m/s at height_ws]
        */
        void reference_evapotranspiration(R& response,  utctimespan dt, double net_radiation, double tempmax,double tempmin, double rhumidity, double elevation=0.1, double windspeed=0.1)const noexcept {
            reference_evapotranspiration_asce_st(response,dt,net_radiation,tempmax,tempmin,rhumidity,elevation,windspeed);
            if (param.full_model){
                reference_evapotranspiration_asce_full(response,dt,net_radiation,tempmax,tempmin,rhumidity,elevation,windspeed);
            }
            return;
        }

        /** @brief computes ASCE PENMAN-MONTEITH reference evapotranspiation, [mm/h] based on eq. B.1 from ASCE-EWRI
        * @param response -- updating response structure
        * @param net_radiation, [MJ/m2/h] -- net radiation
        * @param temperature [degC] -- air temperature
        * @param rhumidity [-] -- relative humidity
        * @param elevation [m]
        * @param windspeed [m/s at height_ws]
        */
        void reference_evapotranspiration_asce_full(R& response,  utctimespan dt, double net_radiation, double tempmax,double tempmin, double rhumidity, double elevation=0.1, double windspeed=0.1)const noexcept {

            double pressure = atm_pressure(elevation, 293.15);// recommended value for T0 during growing season is 20 gradC, see eq. B.8
            double temperature = 0.5*(tempmax+tempmin);
            double lambda_rho = vaporization_latent_heat(temperature)*rho_w;
            double delta = svp_slope(temperature);
            double avp = actual_vp(temperature, rhumidity);
            double G = soil_heat_flux(net_radiation);
            const double step = to_seconds(dt)/to_seconds(calendar::HOUR);
            G = G*(step-24)/(-23.0);
            response.soil_heat = G;
            double sat_vp = svp_daily(tempmax,tempmin);
            double nominator = delta * (net_radiation - G) *step +
                                ktime*step*density_air(pressure, temperature, avp)*cp/resistance_aerodynamic(param.height_ws,windspeed, param.height_t)*(sat_vp-avp);
            double denominator = delta + gamma_pm(pressure, temperature)*(1+resistance_surface(net_radiation,param.height_veg)/resistance_aerodynamic(param.height_ws,windspeed,param.height_t));
            response.et_ref = nominator/denominator/lambda_rho;
        }

        /** @brief computes ASCE PENMAN-MONTEITH reference evapotranspiation, [mm/h] Standartised eq.1 from ASCE-EWRI
        * @param response -- updating response structure
        * @param net_radiation, [MJ/m2/h] -- net radiation
        * @param temperature [degC] -- air temperature
        * @param rhumidity [-] -- relative humidity
        * @param elevation [m]
        * @param windspeed [m/s at height_ws]
        */
        void reference_evapotranspiration_asce_st(R& response, utctimespan dt, double net_radiation, double tempmax,double tempmin, double rhumidity, double elevation=0.1, double windspeed=0.1) const noexcept {

            //                     double pressure = atm_pressure(elevation, 293.15);// recommended value for T0 during growing season is 20 gradC, see eq. B.8
            double pressure = 101.3*pow((293.0-0.0065*elevation)/293,5.26); // eq.34
            double gamma = 0.000665*pressure;
            double temperature = 0.5*(tempmax+tempmin);
            double delta = 2503.0*exp(17.27*temperature/(temperature+237.3))/(temperature+237.3)/(temperature+237.3);
            double avp = actual_vp(temperature, rhumidity);
            // short crop
            double hveg0 = 0.12;
            double Cn_sc_hourly_daytime = 37;
    //        double Cn_tc_hourly_daytime = 66;
            double Cd_sc_hourly_daytime = 0.24;
    //        double Cd_tc_hourly_daytime = 0.25;
            const double step  = to_seconds(dt)/to_seconds(calendar::HOUR);
    //        double step = 1;
            double Cd_sc_hourly_night = 0.96;


            double Cn_veg = Cn_sc_hourly_daytime+(param.height_veg-hveg0)*76.32; //vegetation dependency for 1h timestep
            double Cd_veg = Cd_sc_hourly_daytime+(param.height_veg-hveg0)*0.0263;

            double Cd_veg_night = Cd_sc_hourly_night+(param.height_veg-hveg0)*1.9473;


    //        double Cn_step = Cn_veg + (step-1) * 37.52*param.height_veg/hveg0*0.427;
            double Cn_step = Cn_veg + (step-1) * 76.776*(param.height_veg+0.3687) ; //should be 900 for 24h timestep and short crop
            double Cd_step = Cd_veg + (step-1) * 0.003432*(param.height_veg+1.1467) ; // should be 0.34 for 24h timestep and short crop

            double Cd_step_night = Cd_veg_night - (step-1) * 0.08*(param.height_veg+0.21657) ; // should be 0.34 for 24h timestep and short crop

    //        double Cn = 37.0;
    //        double Cd = 0.24;
            double Cn = Cn_step;
            double Cd = Cd_step;
            double G = 0.1*net_radiation;
            if (net_radiation<0.0) {
                G = 0.5 * net_radiation;
                Cd = Cd_step_night;
            }

    //        // tall reference:
            if (param.height_veg>0.12){
                G = 0.04*net_radiation;
    //            Cn = 66.0;
    //            Cd = 0.25;
                if (net_radiation<0.0) {
                    G = 0.2 * net_radiation;
    //                Cd = 1.7;
                }
            }

            G =  G*(step-24)/(-23.0);
            response.soil_heat = G;

    //        double sat_vp = svp(temperature);
            double sat_vp = svp_daily(tempmax,tempmin); //this will work fine for 1hour as well

            double nominator = 0.408* delta * (net_radiation - G) * step +
                                gamma*Cn*ws_adjustment(param.height_ws,windspeed)*(sat_vp-avp)/(temperature+273);
            double denominator = delta + gamma*(1+Cd*ws_adjustment(param.height_ws,windspeed));
            response.et_ref = std::max(-0.01,nominator/denominator/step);
            return;
        };

    private:
        // constants for asce penman-monteith method
        const double rho_w = 1;//[Mg/m^3]
        const double cp = 1.013*0.001; // specific heat of moist air, [MJ/kg/gradC]
    //    const double cp = 1.013;
    //    const double ktime = 3600; // unit conversion for ET in mm/h
        const double ktime = 3600; // unit conversion for MJ to W/s
        double d =  0.67 * param.height_veg; // zero displacement height, [m]
        double zom = 0.123*param.height_veg; // roughness length governing momentum transfer, [m]
        double zoh =  0.0123*param.height_veg;// roughness height for transfer of heat and vapor, [m]
        double kappa = 0.41; // von Karman's constant

        /** @brief aerodynamic resistance, [s/m], eq. B.2
        * @param hws -- wind speed measurements height, [m]
        * @param ws -- wind speed at height hws, [m/s]
        * @param ht [m] -- height of humidity and or temperature measurements
        */
        double resistance_aerodynamic(double hws, double ws, double ht=2.0) const noexcept {
            using std::log;
            return log((hws - d)/zom)*log((ht-d)/zoh)/(kappa*kappa*std::max(ws,0.01));
        }

        /** @brief active leaf-area index (LAI), eq.B.4
        * @param hveg -- vegetation height, [m]
         * @return leaf area index active
        */
        double lai_a(double hveg) const noexcept {
            using std::log;
            double lai = 24.0*hveg; // for clipped grass
            if (hveg>0.12) {
                lai = 5.5 + 1.5 * log(hveg);// for alfalfa
            }
            return 0.5*lai;
        }

        /** @brief bulk surface resistance, [s/m], eq. B.3
        * @param hveg -- vegetation height,[m]
         * @return surface resistanse
        */
        double resistance_surface(double rnet, double hveg=0.1) const noexcept { /// TODO discuss the incorporation of MORECS model here, is it worth to account for seasons?
            double rl =param.rl;
            if (rnet<=0.0){
                rl = 4*param.rl;
            }
            return rl/lai_a(hveg);
        }

        /// TODO: move all to the hydro_functions???

        /** @brief Latent heat of vaporization, eq. B.7; ASCE-EWRI
        * @param temperature [degC]
        */
        double vaporization_latent_heat(double temperature = 20.0) const noexcept {
            return 2.501 - (2.361*0.001)*temperature;//with default value gives 2.45;
        }

        //                 /**@brief atmospheric pressure, ASCE-EWRI, eq.B.8
        //                  * */
        //                 double pressure_air(double elevation){
        //                     const double p0 = 101.325;// pressure at sea level, [kPa]
        //                     const double T0 = 293.15; // temperature at sea level for growing season, [K]
        //                     const double alpha1 = 0.0065; // lapse rate, [K/m]
        //                     const double z0 = 0; // sea level, [m]
        //                     const double Rgas = 287; // universal gas constant, [J/kg/K]
        //                     const double g = 9.807; // gravitational acceleration, [m/s2]
        //                     return p0*pow((T0-alpha1*(elevation-z0))/T0,g/alpha1/Rgas);
        //                 }

        /** @brief atmospheric air density, ASCE-EWRI, eq. B.10
        * @param pressure [kPa]
        * @param temperature [degC]
        * @param actual vapor pressure, [kPa]
        */
        double density_air(double pressure, double temperature, double ea) const noexcept {
            double Tk = 273.16+temperature;
            double Tkv = Tk/(1-0.378*ea/pressure); // B.11
            return 3.486*pressure/Tkv; // B.10
        }

        /** @brief psychrometric constant, [kPa/gradC]
        * @param atmospheric pressure, [kPa]
        * @param air temperature, [degC]
        */
        double gamma_pm(double pressure, double temperature=20.0) const noexcept {

            double lambda = vaporization_latent_heat(temperature);
            const double epsilon = 0.622; // ratio of molecular weight of water vapor/dry air
            return cp*pressure/epsilon/lambda;
        }

        /** @brief soil heat flux density (G) for hourly periods, eq.B.13
        * @param net_radiation should be either MJ/m2/h or Wt/m2
        */
        double soil_heat_flux(double net_radiation) const noexcept {
            double kg = 0.4;
            if (net_radiation <= 0.0)
                kg = 2.0;
            return kg*exp(-lai_a(param.height_veg))*net_radiation; // Kg exp(-0.5LAI)Rnet, 0.5LAI = lai_active
        }

        /** @brief wind speed adjustment for measurement height, eq.B.14*/
        double ws_adjustment(double height_measure, double ws_measure) const noexcept {
            using std::log;
            return ws_measure*log((2-d)/zom)/log((height_measure-d)/zom);
        }
    };

}
