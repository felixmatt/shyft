/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/#pragma once
#pragma once
#include <shyft/time/utctime_utilities.h>
#include <shyft/hydrology/geo_cell_data.h>
#include <armadillo>
#include <vector>
#include <algorithm>
#include <cmath>
#include <shyft/hydrology/hydro_functions.h>

namespace shyft::core::radiation { // TODO use rasputin namespace
    using namespace std;
    using namespace hydro_functions;

    struct parameter {
        double as = 1.25;// coefficient for cloud cover correction
        double bs = 0.25;//  coefficient for cloud cover correction
        double albedo = 0.25; // average albedo of the surrounding ground surface:0.15-0.25 -- grass, 0.10-0.15 -- coniferous forest, 0.15 - 0.25 -- deciduous forest, 0.04-0.08 -- open water, 0.15-0.35 -- bare soil
        double turbidity = 1.0; // 1.0 - clean, 0.5 -- dusty
        // calibration coefficients for longwave radiation, added baes on 2009.theorapplclimatol.98.Kjaersgaard.et.alComparison-of-the-performance-of-net-radiation-calculation-models, initial values set based on 2005.ASCE=EWRI
        double al = 0.34;
        double bl = 0.14;
        double ac= 1.35;
        double bc = 0.35;
        parameter(double albedo = 0.25, double turbidity = 1.0,  double al=0.34, double bl=0.14, double ac=1.35, double bc=0.35,double as = 1.25, double bs = 0.25) : as(as), bs(bs), albedo(albedo), turbidity(turbidity), al(al), bl(bl), ac(ac), bc(bc)  {}
    };

    struct response {
        double net_sw = 0.0; // net short-wave radiation
        double net_lw = 0.0; // net long-wave radiation [W/m^2]
        double net = 0.0;
        double sw_cs_p = 0.0; // predicted clear-sky sw radiation [W/m^2]
        double sw_t = 0.0; // translated  sw radiation (either predicted clear-sky or measured) on a sloping surface based on measured horizontal radiation [W/m^2]
        double ra = 0.0; // extraterrestrial radiation
    };

    template<class P, class R>
    struct calculator {
        P param;

        explicit calculator(const P &p) : param(p) {}

        double latitude() const noexcept {
            return phi_ * rad2deg;
        }// latitude, [deg] should be available from cell?/// TODO: add PROJ4 for conversion from cartesian to wgs84

        double ra_radiation() const noexcept { return ra_; } // extraterrestrial solar radiation for inclined surface[W/m2]
        double ra_radiation_hor() const noexcept { return rahor_; } // extraterrestrial solar radiation for horizontal surfaces

        double sun_rise() const noexcept { return omega1_24_ * rad2deg; }

        double sun_set() const noexcept { return omega2_24_ * rad2deg; }

        /** @brief computes instantaneous net radiation, [W/m2]
        * * ref.: Allen, R. G.; Trezza, R. & Tasumi, M. Analytical integrated functions for daily solar radiation on slopes Agricultural and Forest Meteorology, 2006, 139, 55-73
        * @param latitude, [deg]
        * @param utctime t,
        * @param slope, [deg]
        * @param aspect, [deg]
        * @param temperature, [degC]
        * @param rhumidity, [percent]
        * @param elevation, [m]
        * @param rsm,[W/m^2] -- measured solar radiation*/
        void net_radiation(R &response, double latitude, utctime t, double slope=0.0, double aspect = 0.0,
                            double temperature = 0.0, double rhumidity = 40.0, double elevation = 0.0,
                            double rsm = 0.0){

            response.sw_cs_p = psw_radiation(latitude, t, slope, aspect, temperature, rhumidity, elevation);
            response.sw_t = tsw_radiation(latitude, t, slope, aspect, temperature, rhumidity, elevation,rsm);
            response.net_sw = (1-param.albedo)*tsw_radiation(latitude, t, slope, aspect, temperature, rhumidity, elevation,rsm);
            response.net_lw = lw_radiation_asce_st(temperature, rhumidity);   // changed to asce-ewri, based on 2009.Kjaersgaard
            response.net = response.net_sw-response.net_lw;
            response.ra = ra_radiation();
        }
        /** @brief computes net radiation over a period (tested with 1h,3h and 24h time-steps), [W/m2]
        * * ref.: Allen, R. G.; Trezza, R. & Tasumi, M. Analytical integrated functions for daily solar radiation on slopes Agricultural and Forest Meteorology, 2006, 139, 55-73
        * @param latitude, [deg]
        * @param utctime tstart, start of period
        * @param utctimespan dt, step
        * @param slope, [deg]
        * @param aspect, [deg]
        * @param temperature, [degC]
        * @param rhumidity, [percent]
        * @param elevation, [m]
        * @param rsm,[W/m^2] -- measured solar radiation
        * */
        void net_radiation_step(R &response, double latitude, utctime t1, utctimespan dt, double slope=0.0, double aspect = 0.0,
                            double temperature = 0.0, double rhumidity = 40.0, double elevation = 0.0,
                            double rsm = 0.0){
            response.sw_cs_p = psw_radiation_step(latitude, t1,dt, slope, aspect, temperature, rhumidity, elevation);
            response.sw_t = tsw_radiation_step(latitude, t1,dt, slope, aspect, temperature, rhumidity, elevation,rsm);
            response.net_sw = (1-param.albedo)*tsw_radiation_step(latitude, t1,dt, slope, aspect, temperature, rhumidity, elevation,rsm);
            response.net_lw = lw_radiation_step(temperature, rhumidity);
            response.net = response.net_sw-response.net_lw;
            response.ra = ra_radiation();
        }
        /** @brief computes net radiation over a period (tested with 1h,3h and 24h time-steps), [W/m2]
        * * ref.: Allen, R. G.; Trezza, R. & Tasumi, M. Analytical integrated functions for daily solar radiation on slopes Agricultural and Forest Meteorology, 2006, 139, 55-73
        * @param latitude, [deg]
        * @param utctime tstart, start of period
        * @param utctimespan dt, step
        * @param slope, [deg]
        * @param aspect, [deg]
        * @param temperature, [degC]
        * @param rhumidity, [percent]
        * @param elevation, [m]
        * @param rsm,[W/m^2] -- measured solar radiation
        * */
        void net_radiation_step_asce_st(R &response, double latitude, utctime t1, utctimespan dt, double slope=0.0, double aspect = 0.0,
                                double temperature = 0.0, double rhumidity = 40.0, double elevation = 0.0,
                                double rsm = 0.0){

            response.sw_cs_p = psw_radiation_step(latitude, t1,dt, slope, aspect, temperature, rhumidity, elevation);
            response.sw_t = tsw_radiation_step(latitude, t1,dt, slope, aspect, temperature, rhumidity, elevation,rsm);
            response.net_sw = (1-param.albedo)*tsw_radiation_step(latitude, t1,dt, slope, aspect, temperature, rhumidity, elevation,rsm);
            response.net_lw = lw_radiation_asce_st(temperature, rhumidity,fcd_);
            response.net = response.net_sw-response.net_lw;
            response.ra = ra_radiation();
        }


    private:
        double delta_;
        double omega_;
        double phi_;
        double slope_;
        double aspect_;

        double ra_ = 0.0; // extraterrestrial solar radiation for inclined surface[W/m2]
        double rahor_ = 0.0; // extraterrestrial solar radiation for horizontal surfaces
        double omega1_;
        double omega2_;
        double omega1b_=0.0;
        double omega2b_=0.0;

        calendar utc;
        double doy_; // day of the yearI
        double lt_; // local time
        double costt_, costthor_;
        double a_, b_, c_,g_,h_;
        double f1_, f2_,f3_,f4_,f5_;
        double omega1_24_, omega2_24_;
        double omega1_24b_= 0.0;
        double omega2_24b_= 0.0; //integration limits, actual periods of sun

        double fb_;
        double fcd_= 0.05;
        double sin_beta_;
        double step_=1;
        bool twoperiods_ = false;
        bool devide_ = false;

        /** @brief computes necessary geometric parameters
        * @param omega, [rad] -- hour angle
        * @return cos(theta) -- theta -- angle of incidence             * */
        double costt(double omega) const noexcept {
            return -a_ + b_ * cos(omega) + c_ * sin(omega); // eq.14
        }

        /** @brief computes necessary geometric parameters
        * @param phi, [rad] -- latitude
        * @param s, [rad] -- slope angle
        * @param gamma, [rad] -- aspect angle
        * @return cos(theta) -- theta -- angle of incidence             * */
        void compute_abc(double delta, double phi, double s = 0.0, double gamma = 0.0) noexcept {
            a_ = sin(delta) * cos(phi) * sin(s) * cos(gamma) - sin(delta) * sin(phi) * cos(s); // eq.11a
            b_ = cos(delta) * cos(phi) * cos(s) + cos(delta) * sin(phi) * sin(s) * cos(gamma);//eq 11b
            c_ = cos(delta) * sin(s) * sin(gamma);
            g_ = sin(delta) * sin(phi);
            h_ = cos(delta) * cos(phi);
        }

        /** @brief computes necessary geometric parameters
        * @param omega1, [rad] -- tstart
        * @param omega2, [rad] -- tend
        * @return f1,f2,f3,f4,f5             * */
        void compute_fs(double omega1, double omega2, double omega1b=0.0, double omega2b=0.0) noexcept {
            f1_ = sin(omega2)-sin(omega1)+sin(omega2b)-sin(omega1b); // eq.11a
            f2_ = cos(omega2)-cos(omega1)+cos(omega2b)-cos(omega1b);//eq 11b
            f3_ = omega2-omega1+omega2b-omega1b;
            f4_ = sin(2*omega2)-sin(2*omega1)+sin(2*omega2b)-sin(2*omega1b);
            f5_ = sin(omega2)*sin(omega2)-sin(omega1)*sin(omega1)+sin(omega2b)*sin(omega2b)-sin(omega1b)*sin(omega1b);

        }
        /** @brief computes necessary geometric parameters
        * @param phi, [rad] -- latitude
        * @param s, [rad] -- slope angle
        * @param gamma, [rad] -- aspect angle
        * @return cos(theta) -- theta -- angle of incidence             * */
        double compute_beta_step() const noexcept {
            double nominator = (b_*g_-a_*h_)*f1_-c_*g_*f2_+(0.5*b_*h_-a_*g_)*f3_+0.25*b_*h_*f4_+0.5*c_*h_*f5_;
            double denominator = b_*f1_-c_*f2_-a_*f3_>0?b_*f1_-c_*f2_-a_*f3_:0.0001;
            return nominator/denominator;
        }

        /** @brief compute sun rise and sun set values
        *
        * calculates  local variables omega1_24_, omega2_24_, omega1_24b_, omega2_24b_
        * @param delta,[rad] - earrh declination angle
        * @param phi,[rad] -- latitude
        * @param slope,[rad]
        * @param aspect,[rad]
        */
        void compute_sun_rise_set(double delta, double phi, double slope, double aspect) noexcept {
            ///TODO get info about hemisphere from data, don't see anything in geopoint, but it is inside netcdf file -- add geopoint_with_crs to interface
            double omega_s; // omega_s -- time of potential horizontal sunset, -omega_s --time of horizontal sunrize
            // this solar noon and sunrise taken from ref.: Lawrence Dingman Physical Hydrology, Third Edition, 2015, p.575
            if (abs(phi - delta) >= pi / 2) { omega_s = pi; }
            if (abs(phi - delta) < pi / 2 && (abs(phi + delta) >= pi / 2)) { omega_s = 0; }
            if (abs(phi - delta) < pi / 2 && (abs(phi + delta) < pi / 2)) {
                omega_s = acos(-tan(delta) * tan(phi));
            } // for horizontal surface

    //                    compute_abc(delta, phi, slope, aspect);
            double costt_sunset = costt(omega_s);
            double costt_sunrise = costt(-omega_s);

            // Lower integration limit
            double bbcc = b_ * b_ + c_ * c_ > 0.0 ? b_ * b_ + c_ * c_ : 0.0001;
            double sqrt_bca = bbcc - a_ * a_ > 0.0 ? bbcc - a_ * a_ : 0.0; // the authors suggest here 0.0001???
            double sin_omega1 = std::min(1.0, std::max(-1.0, (a_ * c_ - b_ * std::pow(sqrt_bca, 0.5)) / bbcc));//eq.(13a)
            double omega1 = asin(sin_omega1);
            omega1_24_ = omega1;
            double costt_omega1 = costt(omega1);
            if ((costt_sunrise <= costt_omega1) && (costt_omega1 < 0.001)) {omega1_24_ = omega1;}
            else {
                omega1 = -pi - omega1;
                if (costt(omega1) > 0.001) {omega1_24_ = -omega_s;}
                else {
                    if (omega1 <= -omega_s) {omega1_24_ = -omega_s;}
                    else {omega1_24_ = omega1;}
                }
            }
            omega1_24_ = std::max(-omega_s,omega1_24_);

            // Upper integration limit
            double sin_omega2 = std::min(1.0, std::max(-1.0, (a_ * c_ + b_ * std::pow(sqrt_bca, 0.5)) / bbcc));//eq.(13b)
            double omega2 = asin(sin_omega2);
            omega2_24_ = omega2;
            double costt_omega2 = costt(omega2);
            if (costt_sunset <= costt_omega2 && costt_omega2 < 0.001) {omega2_24_ = omega2;}
            else {
                omega2 = pi - omega2;
                if (costt(omega2) > 0.001) {omega2_24_ = omega_s;}
                else {
                    if (omega2 >= omega_s) {omega2_24_ = omega_s;}
                    else {omega2_24_ = omega2;}
                }
            }
            omega2_24_ = std::min(omega_s,omega2_24_);

            if (omega1_24_>omega2_24_){omega1_24_=omega2_24_;}// slope is always shaded

            // two periods of direct beam radiation (eq.7)
            if (sin(slope) > sin(phi) * cos(delta) + cos(phi) * sin(delta)) {
                double sinA = std::min(1.0, std::max(-1.0, (a_ * c_ + b_ * std::pow(sqrt_bca, 0.5)) / bbcc));
                double A = asin(sinA);
                double sinB = std::min(1.0, std::max(-1.0, (a_ * c_ - b_ * std::pow(sqrt_bca, 0.5)) / bbcc));
                double B = std::asin(sinB);
                omega2_24b_ = std::min(A, B);
                omega1_24b_ = std::max(A, B);
                double costt_omega2_24b = costt(omega2_24b_);
                if (costt_omega2_24b < -0.001 || costt_omega2_24b > 0.001) { omega2_24b_ = -pi - omega2_24b_; }
                double costt_omega1_24b = costt(omega1_24b_);
                if ((costt_omega1_24b < -0.001 || costt_omega1_24b > 0.001)) { omega1_24b_ = pi - omega1_24b_; }
                omega2_24b_ = std::max(omega2_24b_,omega1_24_);
                omega1_24b_ = std::min(omega1_24b_,omega2_24_);
                if ((omega2_24b_>=omega1_24_) && (omega1_24b_<=omega2_24_)) {
                    if (costt_step(omega2_24b_, omega1_24b_) < 0.0)
                        twoperiods_ = true;
                    else {
                        twoperiods_ = false;
                        omega1_24b_=omega2_24b_;
                    }
                } else {
                    twoperiods_ = false;
                    omega1_24b_=omega2_24b_;
                }
            } else {
                twoperiods_=false;
                omega1_24b_=omega2_24b_;
            }
        }

        /** @brief computes solar hour angle from local time
        * ref.: https://en.wikipedia.org/wiki/Equation_of_time
        * ref.: Lawrence Dingman Physical Hydrology, Third Edition, 2015, p.574, no EOT correction provided
        * @param lt -- local time (LT) [h]
        * @param longitute = (0 for UTC time)
        * @param tz = 0 -- time zone [h], difference of LT from UTC
        * we use UTC, so longitude = 0.0, tz = 0.0
        * @return HRA, [rad] -- Earth hour angle
        */
        double hour_angle(double lt, double longitude = 0.0, double tz = 0.0) const noexcept {
            //TODO:
            //double LSTM = 15 * tz; // local standard time meridian
            //                double B = 360/365*(doy)*(-81);
            //                double EoT = 9.87*sin(2*B) - 7.3*cos(B) - 1.5*sin(B);// equation of time
            //double M = 2 * pi / 365.2596358 * doy_ * pi / 180;
            //double EoT = -7.659 * sin(M) + 9.863 * sin(2 * M + 3.5932);//https://en.wikipedia.org/wiki/Equation_of_time*/
            //double TC = 4 * (longitude - LSTM) +  EoT; //time correction factor including EoT correction for Earth eccentricity
            //double LST = lt - TC / 60; // local solar time
            return 15 * (lt - 12) * deg2rad; ///TODO: find right EOT and data for validation, so it should return: 15*(LST-12)
        }

        /** @brief computes earth declination angle
        * // ref.: Lawrence Dingman Physical Hydrology, Third Edition, 2015, p.574, eq.(D.5)
        * @param doy -- day of the year
        * @return declination, [rad] */
        double compute_earth_declination(double doy) const noexcept {
            double G = 2 * pi / 365.0 * (doy - 1);
            return 0.006918 - 0.399912 * cos(G) + 0.070257 * sin(G) - 0.006758 * cos(2 * G) +
                    0.000907 * sin(2 * G) - 0.002697 * cos(3 * G) + 0.00148 * sin(3 * G);
        }

        /** @brief computes extraterrestrial solar radiation
            * @param cos(theta)
            * @param doy -- day of the year
            * return ra, [W/m^2]*/
        double compute_ra(double cos_theta, double doy) const noexcept {
            return gsc * cos_theta * (1 + 0.0033 * cos(doy * 2 * pi / 365)); // eq.(1)
        }

        double compute_ra_step(double cos_theta, double doy) const noexcept {
            return gsc * cos_theta * (1 + 0.0033 * cos(doy * 2 * pi / 365))/pi/2; // eq.(1)
        }

        double fi(double slope) const noexcept { return 0.75 + 0.25 * cos(slope) - 0.5 / pi * slope;/*eq.(32)*/}

        double fia(double kb, double kd, double slope, double fb) const noexcept {
            return (1 - kb) *
                    (1 + pow(kb / (abs(kb + kd) > 0.0 ? (kb + kd) : 0.0001*(kb>=0?1:-1)), 0.5) * pow(sin(slope / 2), 3)) *
                    fi(slope) + fb * kb; /*eq.(33)*/
        }

        /** @brief compute 24h parameter*/
        double costt_step(double omega1, double omega2) const noexcept {
            return -a_*(omega2-omega1) + b_ * (sin(omega2)-sin(omega1))+c_*(cos(omega2)-cos(omega1));
        }

        /** @brief computes instantaneous predicted short-wave clear-sky radiation (direct, diffuse, reflected) for inclined surfaces
        *
        * ref.: Allen, R. G.; Trezza, R. & Tasumi, M. Analytical integrated functions for daily solar radiation on slopes Agricultural and Forest Meteorology, 2006, 139, 55-73
        * @param latitude, [deg]
        * @param utctime,
        * @param slope, [deg]
        * @param aspect, [deg]
        * @param temperature, [degC]
        * @param rhumidity, [percent]
        * @param elevation
        * @return */
        double psw_radiation(double latitude, utctime t, double slope=0.0, double aspect = 0.0,
                            double temperature = 0.0, double rhumidity = 40.0, double elevation = 0.0) {
            doy_ = utc.day_of_year(t);
            lt_ = utc.calendar_units(t).hour + utc.calendar_units(t).minute / 60.0;
            delta_ = compute_earth_declination(doy_);
            omega_ = hour_angle(lt_); // earth hour angle

            slope_ = slope*pi/180.0;
            aspect_ = aspect*pi/180;
            phi_ = latitude * pi / 180;


            compute_abc(delta_, phi_, 0.0, 0.0);
            compute_sun_rise_set(delta_, phi_, 0.0, 0.0); // for horizontal surface
            costthor_ = costt(omega_);

            if (omega_ >= omega1_24_ && omega_ <= omega2_24_) {
                rahor_ = std::max(0.0, compute_ra(costthor_, doy_)); // eq.(1) with cos(theta)hor
            } else {
                rahor_ = 0.0;
            };


            compute_abc(delta_, phi_, slope_, aspect_);
            compute_sun_rise_set(delta_, phi_, slope_, aspect_);
            costt_ = costt(omega_); // eq.(14)

            if ((!(twoperiods_ ) && omega_ >= omega1_24_ && omega_ <= omega2_24_) || (twoperiods_ && ((omega_ >= omega1_24_ && omega_ <= omega2_24b_) || (omega_ >= omega1_24b_ && omega_ <= omega2_24_)))){
                ra_ = std::max(0.0, compute_ra(costt_, doy_)); // eq.(1)
            } else {
                ra_ = 0.0;
            };

            double sin_betahor = std::min(1.0,std::max(0.01,costthor_)); // eq.(20) equal to (4), cos_tthor = sin_betahor
            sin_beta_ = sin_betahor;

            double eatm = atm_pressure(elevation); // [kPa] atmospheric pressure as a function of elevation
            double ea = actual_vp(temperature, rhumidity); //[kPa] actual vapor pressure
            double W = 0.14 * ea * eatm + 2.1; // eq.(18) //equivalent depth of precipitable water in the atmosphere[mm]

            // clearness index for direct beam radiation
            double Kbo = std::min(1.0, std::max(0.001, 0.98 * exp(-0.00146 * eatm / param.turbidity / sin_beta_ -
                                                0.075 * pow((W / sin_beta_), 0.4)))); // eq.(17)
            double Kbohor = std::min(1.0, std::max(0.001, 0.98 * exp(-0.00146 * eatm / param.turbidity / sin_betahor -
                                                            0.075 * pow((W / sin_betahor), 0.4)))); // eq.(17)

            double Kdo; // transmissivity of diffuse radiation, eq.(19)a,b,c
            if (Kbo >= 0.15) { Kdo = 0.35 - 0.36 * Kbo; }
            else if (Kbo < 0.15 && Kbo > 0.065) { Kdo = 0.18 + 0.82 * Kbo; }
            else { Kdo = 0.10 + 2.08 * Kbo; }

            double Kdohor;
            if (Kbohor >= 0.15) { Kdohor = 0.35 - 0.36 * Kbohor; }
            else if (Kbohor < 0.15 && Kbohor > 0.065) { Kdohor = 0.18 + 0.82 * Kbohor; }
            else { Kdohor = 0.10 + 2.08 * Kbohor; }

            double fi_ = fi(slope_);//eq.(32)

            // fb_ = min(5.0,Kbo/Kbohor*ra_/(rahor_>0.0?rahor_:max(0.00001,ra_)));//eq.(34)
            fb_ = ra_ / (rahor_ > 0.0 ? rahor_ : 0.0001);//eq.(34)

            double fia_ = fia(Kbohor, Kdohor, slope_, fb_); //eq.(33)
            double dir_radiation = Kbo * ra_;
            double dif_radiation = fia_ * Kdo * rahor_;
            double ref_radiation = param.albedo * (1 - fi_) * (Kbo + Kdo) * rahor_;
            return dir_radiation + dif_radiation + ref_radiation; // predicted clear sky solar radiation for inclined surface [W/m2]
        }

        /** @brief computes extraterrestrial radiation*/
        double compute_ra(double step,double delta, double phi, double s, double gamma, double omega1, double omega2, double doy){

            // NEXT Calculations is done to define the integration omegalim properly, based on the step
            compute_sun_rise_set(delta, phi, s, gamma); // this function calculates daily values, for timesteps less than 24h we need adjustment of limits

            omega1_ = omega1;
            omega2_ = omega2;
            omega1b_ = omega1;
            omega2b_ = omega1b_;
    //        if (omega1>omega2){
    //            std::swap(omega1,omega2);
    //        }

            if (twoperiods_)
            {
                //TODO: change algorithm it should be simple, it should have less if-else blocks
                if (omega2 > omega1_24_) {
                    if ((omega1 <= omega1_24_) && (omega1 <= omega2)) {
                        omega1_ = omega1_24_;
                    }
                }
                if (omega1 < omega2_24_) {
                    if ((omega2 >= omega2_24_) && (omega1 >= omega1_24b_)) {
                        omega2_ = omega2_24_;
                    }
                }

                if ((omega1 < omega2_24b_) ) {
                    if ((omega2>= omega2_24b_) && (omega2>=omega1_24b_) && (omega2<omega2_24_) && (omega1 >= omega1_24_)){
                        devide_ = true;
                    }
                    if ((omega2 >= omega2_24b_) && (omega1 >= omega1_24_)) {
                        omega2_ = omega2_24b_;
                    }
                }
                if (omega2 > omega1_24b_) {
                    if ((omega1 <= omega1_24b_) && (omega2 <= omega2_24_)) {
                        omega1_ = omega1_24b_;
                    }
                }
            } else {
                // 1-h or 3-h step, if not the whole period has sun
                if (omega2 > omega1_24_) {
                    if ((omega1 <= omega1_24_) && (omega2 <= omega2_24_)) {
                        omega1_ = omega1_24_;
                    }
                }
                if (omega1 < omega2_24_) {
                    if ((omega2 >= omega2_24_) && (omega1 >= omega1_24_)) {
                        omega2_ = omega2_24_;
                    }
                }
            }

            //double costt = costt_step(omega1_,omega2_);//-a_*(omega2-omega1) + b_ * (sin(omega2)-sin(omega1))+c_*(cos(omega2)-cos(omega1));

            if (step>=23){// integrate over whole period for timestep 24h
                if (twoperiods_) {
                    return std::max(0.0, gsc * (costt_step(omega1_24_, omega2_24b_) + costt_step(omega1_24b_, omega2_24_)) * (1 + 0.0033 * cos(doy * 2 * pi / 365)) / pi / 2);
                } else
                    return std::max(0.0, gsc * (costt_step(omega1_24_, omega2_24_)) *(1 + 0.0033 * cos(doy * 2 * pi / 365)) / pi / 2);
            } else {
                if ( (!twoperiods_ && (omega1_ >= omega1_24_ && omega2_ <= omega2_24_)) || (twoperiods_ && ((omega1_ >= omega1_24_ && omega2_ <= omega2_24b_) || (omega1_ >= omega1_24b_ && omega2_ <= omega2_24_)))) {
                    return std::max(0.0, gsc * costt_step(omega1_,omega2_) * (1 + 0.0033 * cos(doy * 2 * pi / 365)) / pi /2); // eq.(1) with cos(theta)hor
                } else if (twoperiods_ && devide_){
                    return std::max(0.0, gsc * (costt_step(omega1_, omega2_24b_) + costt_step(omega1_24b_, omega2_)) * (1 + 0.0033 * cos(doy * 2 * pi / 365)) / pi / 2);
                } else
                    return 0.0;

            }
        }

        /** @brief computes instantaneous predicted short-wave clear-sky radiation (direct, diffuse, reflected) for inclined surfaces
        * ref.: Allen, R. G.; Trezza, R. & Tasumi, M. Analytical integrated functions for daily solar radiation on slopes Agricultural and Forest Meteorology, 2006, 139, 55-73
        * @param latitude, [deg]
        * @param utctime tstart,
        * @param ucttime tend,
        * @param slope, [deg]
        * @param aspect, [deg]
        * @param temperature, [degC]
        * @param rhumidity, [percent]
        * @param elevation
        * @return */
        double psw_radiation_step(double latitude, utctime t1, utctimespan dt, double slope=0.0, double aspect = 0.0,
                                double temperature = 0.0, double rhumidity = 40.0, double elevation = 0.0) {
            doy_ = utc.day_of_year(t1);
            double lt1 = utc.calendar_units(t1).hour + utc.calendar_units(t1).minute / 60.0;
            double lt2 = utc.calendar_units(t1+dt).hour + utc.calendar_units(t1+dt).minute / 60.0;
            if (lt1>=lt2){
                lt2=24.0;
            }
            const double dt_in_hours = to_seconds(dt)/to_seconds(calendar::HOUR);
            step_ = dt_in_hours;
            delta_ = compute_earth_declination(doy_);
            double omega1 = hour_angle(lt1); // earth hour angle
            double omega2 = hour_angle(lt2); // earth hour angle
            omega1_ = omega1;
            omega2_ = omega2;

            slope_= slope*pi/180.0;
            aspect_ = aspect*pi/180;
            phi_ = latitude * pi / 180;


            compute_abc(delta_,phi_,0.0,0.0);
            rahor_ = compute_ra(step_,delta_,phi_,0.0,0.0,omega1,omega2,doy_); // this function also updates omega1_, omega2_
            if (step_ >=23){
                compute_fs(omega1_24_, omega2_24_,0.0,0.0);
            }
            else
                compute_fs(omega1_,omega2_,0.0,0.0); //only 1 period for horizontal surface
            double sin_betahor = std::min(1.0,std::max(0.01,compute_beta_step())); // eq.(22),


            compute_abc(delta_,phi_,slope_,aspect_);
            ra_ = compute_ra(step_,delta_,phi_,slope_,aspect_,omega1,omega2,doy_);
            if (step_ >=23){
                if (twoperiods_){
                    compute_fs(omega1_24_, omega2_24_, omega1_24b_,omega2_24b_);
                }
                else compute_fs(omega1_24_, omega2_24_,0.0,0.0);
            }
            else {
                if (devide_ && twoperiods_) {
                    compute_fs(omega1_, omega2_, omega1_24b_, omega2_24b_);
                    devide_ = false;
                } else
                    compute_fs(omega1_, omega2_, 0.0, 0.0);
            }
            // weighted sin beta
    //        sin_beta_ = std::min(1.0,std::max(0.01,compute_beta_step())); // eq.(22),
            sin_beta_ = sin_betahor; // not weighted sin beta can be used only for timsteps less than 1h

            double eatm = atm_pressure(elevation); // [kPa] atmospheric pressure as a function of elevation
            double ea = actual_vp(temperature, rhumidity); //[kPa] actual vapor pressure
            double W = 0.14 * ea * eatm + 2.1; // eq.(18) //equivalent depth of precipitable water in the atmosphere[mm]

            double Kbo = std::min(1.0, std::max(0.001, 0.98 * exp(-0.00146 * eatm / param.turbidity / sin_beta_ -
                                                            0.075 * pow((W / sin_beta_), 0.4)))); // eq.(17)
            double Kbohor = std::min(1.0, std::max(0.001, 0.98 * exp(-0.00146 * eatm / param.turbidity / sin_betahor -
                                                                    0.075 * pow((W / sin_betahor), 0.4)))); // eq.(17)

            double Kdo; // transmissivity of diffuse radiation, eq.(19)a,b,c
            if (Kbo >= 0.15) { Kdo = 0.35 - 0.36 * Kbo; }
            else if (Kbo < 0.15 && Kbo > 0.065) { Kdo = 0.18 + 0.82 * Kbo; }
            else { Kdo = 0.10 + 2.08 * Kbo; }

            double Kdohor;
            if (Kbohor >= 0.15) { Kdohor = 0.35 - 0.36 * Kbohor; }
            else if (Kbohor < 0.15 && Kbohor > 0.065) { Kdohor = 0.18 + 0.82 * Kbohor; }
            else { Kdohor = 0.10 + 2.08 * Kbohor; }

            double fi_ = fi(slope_);//eq.(32)


            fb_ = ra_ / (rahor_ > 0.0 ? rahor_ : 0.0001);//eq.(34)
            if(step_>=23) {
    //                        fb_ = std::min(5.0, Kbo / Kbohor * ra_ / (rahor_ > 0.0 ? rahor_ : std::max(0.00001, ra_)));//eq.(34)
                fb_ = Kbo / Kbohor * ra_ / (rahor_ > 0.0 ? rahor_ : 0.0001);//eq.(34)
            }
            fb_ = Kbo / Kbohor * ra_ / (rahor_ > 0.0 ? rahor_ : 0.0001);//eq.(34)
            double fia_ = fia(Kbohor, Kdohor, slope_, fb_); //eq.(33)
            double dir_radiation = Kbo * ra_;
            double dif_radiation = fia_ * Kdo * rahor_;
            double ref_radiation = param.albedo * (1 - fi_) * (Kbo + Kdo) * rahor_;
            return dir_radiation + dif_radiation + ref_radiation; // predicted clear sky solar radiation for inclined surface [W/m2]
        }

        /** @brief translates measured solar radiation from horizontal surfaces to slopes
        * ref.: Allen, R. G.; Trezza, R. & Tasumi, M. Analytical integrated functions for daily solar radiation on slopes Agricultural and Forest Meteorology, 2006, 139, 55-73
        * @param latitude, [deg]
        * @param utctime,
        * @param slope, [deg]
        * @param aspect, [deg]
        * @param temperature, [degC]
        * @param rhumidity, [percent]
        * @param elevation, [m]
        * @param rsm,[W/m^2] -- measured solar radiation
        * @param precipitation [mm]
        * @return predicted/translated onto the slope net sw radiation*/
        double tsw_radiation(double latitude, utctime t, double slope = 0.0, double aspect = 0.0,
                            double temperature = 0.0, double rhumidity = 40.0, double elevation = 0.0,
                            double rsm = 0.0, double precipitation = 0.0) {
            // first calculate all predicted values
            double psw_rad = psw_radiation(latitude, t, slope, aspect, temperature, rhumidity, elevation); // clear-sky net sw radiation
            double tsw_rad = psw_rad;

            double tauswhor = rsm > 0.0 ? rsm / (rahor_ > 0.0 ? rahor_ : rsm)
                                        : 1.0; //? not sure if we use a theoretical rahor here
            double KBhor;
            if (tauswhor >= 0.42) { KBhor = 1.56 * tauswhor - 0.55; }
            else if (tauswhor > 0.175 && tauswhor < 0.42) {
                KBhor = 0.022 - 0.280 * tauswhor + 0.828 * tauswhor * tauswhor + 0.765 * pow(tauswhor, 3);
            }
            else { KBhor = 0.016 * tauswhor; }
            double KDhor = tauswhor - KBhor;
            double rs_rso = 1.0;
            if (asin(sin_beta_)>0.3){
                rs_rso = std::min(1.0,std::max(0.3,rsm / psw_rad));
                fcd_ = std::max(0.055, std::min(1.0, param.ac * rs_rso - param.bc));
            }
            // cloud-cover correction:
            double cloud_cover = std::min(4.0,precipitation > 1.0 ? 1+0.5*std::log(precipitation)+pow(std::log(precipitation),2) : 1.0);
            tsw_rad = (param.as - param.bs*cloud_cover)*tsw_rad; // cloud cover correction

            if (rsm>0.0)
                tsw_rad = rsm * (fb_ * KBhor / tauswhor + fia(KBhor, KDhor,slope_, fb_) * KDhor / tauswhor +
                                            param.albedo * (1 - fi(slope_)));

            return tsw_rad;
        }

        /** @brief translates measured solar radiation from horizontal surfaces to slopes
        * ref.: Allen, R. G.; Trezza, R. & Tasumi, M. Analytical integrated functions for daily solar radiation on slopes Agricultural and Forest Meteorology, 2006, 139, 55-73
        * @param latitude, [deg]
        * @param utctime tstart,
        * @param utctime tend,
        * @param slope, [deg]
        * @param aspect, [deg]
        * @param temperature, [degC]
        * @param rhumidity, [percent]
        * @param elevation, [m]
        * @param rsm,[W/m^2] -- measured solar radiation
        * @return */
        double tsw_radiation_step(double latitude, utctime tstart, utctimespan dt, double slope = 0.0, double aspect = 0.0,
                                double temperature = 0.0, double rhumidity = 40.0, double elevation = 0.0,
                                double rsm = 0.0, double precipitation = 0.0) {
            // first calculate all predicted values
            double psw_rad = psw_radiation_step(latitude, tstart, dt, slope, aspect, temperature, rhumidity, elevation);
            double tsw_rad = psw_rad;
            double tauswhor = rsm > 0.0 ? rsm / (rahor_ > 0.0 ? rahor_ : rsm)
                                        : 1.0; //? not sure if we use a theoretical rahor here
            double KBhor;
            if (tauswhor >= 0.42) { KBhor = 1.56 * tauswhor - 0.55; }
            else if (tauswhor > 0.175 && tauswhor < 0.42) {
                KBhor = 0.022 - 0.280 * tauswhor + 0.828 * tauswhor * tauswhor + 0.765 * pow(tauswhor, 3);
            }
            else { KBhor = 0.016 * tauswhor; }
            double KDhor = tauswhor - KBhor;

            double rs_rso = 1.0;

            if (asin(sin_beta_)>0.3){
                    rs_rso = std::min(1.0,std::max(0.3,rsm / psw_rad));
                    fcd_ = std::max(0.055, std::min(1.0, param.ac * rs_rso - param.bc));
            }

            if (rsm>0.0)
                tsw_rad = rsm * (fb_ * KBhor / tauswhor + fia(KBhor, KDhor, slope_, fb_) * KDhor / tauswhor +
                                    param.albedo * (1 - fi(slope_)));
            return tsw_rad;
        }
        /** @brief clear-sky longwave raditiation
        * ref.: Lawrence Dingman Physical Hydrology, Third Edition, 2015, p.261
        * @param temperature, [degC] -- air temperature
        * @param rhumidity, [persent] -- relative humidity
        * @return net lw_radiation W/m^2 */
        /// TODO https://www.hydrol-earth-syst-sci.net/17/1331/2013/hess-17-1331-2013-supplement.pdf
        // TODO discuss the option to have different formulations here.
        double lw_radiation(double temperature, double rhumidity) const noexcept {
            double Lin = 2.7*actual_vp(temperature,rhumidity)+0.245*(temperature+273.15)-45.14;
    //        double ss_temp = std::min(temperature+273.15-2.5,273.16);
            double ss_temp = std::min(1.16*(temperature+273.15)-2.09,273.16); //Hegdahl et al, 2016
            double epsilon_ss = 0.95;//water TODO: as parameter
            double Lout = epsilon_ss*sigma*pow(ss_temp,4)+(1-epsilon_ss)*Lin;
            return (Lin-Lout)/MJm2d2Wm2;
        }
        /** @brief clear-sky longwave raditiation
        * ref.: Lawrence Dingman Physical Hydrology, Third Edition, 2015, p.261
        * @param temperature, [degC] -- air temperature
        * @param rhumidity, [persent] -- relative humidity
        * @return net lw_radiation W/m^2 */
        /// TODO https://www.hydrol-earth-syst-sci.net/17/1331/2013/hess-17-1331-2013-supplement.pdf
        // TODO discuss the option to have different formulations here.
        double lw_radiation_step(double temperature, double rhumidity) const noexcept {
            double Lin = 2.7*actual_vp(temperature,rhumidity)+0.245*(temperature+273.15)-45.14;
    //        double ss_temp = std::min(temperature+273.15-2.5,273.16);
            double ss_temp = std::min(1.16*(temperature+273.15)-2.09,273.16); //Hegdahl et al, 2016
            double epsilon_ss = 0.95;//water TODO: as parameter
            double Lout = epsilon_ss*sigma*pow(ss_temp,4)+(1-epsilon_ss)*Lin;
            return (Lin-Lout)/MJm2d2Wm2;
        }
        /** @brief clear-sky longwave raditiation
        * ref.: ASCE-EWRI p.165
        * @param temperature, [degC] -- air temperature
        * @param rhumidity, [persent] -- relative humidity
        * @param fcd,  -- cloud fraction factor (calculated inside sw radiation)
        * @return net lw_radiation W/m^2 */
        double lw_radiation_asce_st(double temperature, double rhumidity,double fcd=0.3) const noexcept {
            double sigma_asce = 2.042*pow(10,-10);
            double netlw  = sigma_asce*fcd*(param.al-param.bl*sqrt(actual_vp(temperature,rhumidity)))*pow(temperature+273.15,4);
            return netlw/MJm2h2Wm2; // added empirical coefficient to match step method results, which were verified for 1h timestep based on ASCE-EWRI
        }

    };

}
