/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>
#include <fstream>

char const* version() {
   return "v5.x";
}
namespace expose {
    extern void api_geo_point();
    extern void api_geo_cell_data();
    extern void hydrology_vectors();
    extern void target_specification();
    extern void region_environment() ;
    extern void priestley_taylor();
    extern void actual_evapotranspiration();
    extern void gamma_snow();
    extern void universal_snow();
    extern void kirchner();
    extern void precipitation_correction();
    extern void hbv_snow();
    extern void hbv_physical_snow();
    extern void cell_environment();
    extern void interpolation();
    extern void skaugen_snow();
    extern void kalman();
	extern void hbv_soil();
	extern void hbv_tank();
	extern void hbv_actual_evapotranspiration();
	extern void glacier_melt();
	extern void routing();
    extern void api_cell_state_id();
    extern void radiation();
    extern void penman_monteith();

    void api() {
        api_geo_point();
        api_geo_cell_data();
        hydrology_vectors();
        target_specification();
        region_environment();
        precipitation_correction();
        priestley_taylor();
        actual_evapotranspiration();
        gamma_snow();
        skaugen_snow();
        hbv_snow();
        hbv_physical_snow();
        kirchner();
        cell_environment();
        interpolation();
        kalman();
		hbv_soil();
		hbv_tank();
		hbv_actual_evapotranspiration();
		glacier_melt();
		routing();
        api_cell_state_id();
        radiation();
        penman_monteith();
    }
}


BOOST_PYTHON_MODULE(_api) {
    namespace py = boost::python;
    py::scope().attr("__doc__") = "Shyft hydrology python api providing basic types";
    py::def("version", version);
    py::docstring_options doc_options(true, true, false);// all except c++ signatures
    expose::api();
}

