#include <shyft/py/energy_market/api_utils.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/water_route.h>
#include <shyft/energy_market/hydro_power/power_station.h>
#include <shyft/energy_market/hydro_power/catchment.h>

    namespace expose {
    namespace py=boost::python;
    using std::string;    
    void catchment_stuff() {
        using namespace boost::python;
        using namespace shyft::energy_market::hydro_power;
        class_<catchment, std::shared_ptr<catchment>, boost::noncopyable>(
            "Catchment",
            doc_intro("")
            )
            .def(init<int, const string&, const string&,hydro_power_system_ const&>(
                (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json"), py::arg("hps")), 
                doc_intro("tbd")
            )
            )
            .def_readwrite("id", &catchment::id)
            .def_readwrite("name", &catchment::name)
            .def_readwrite("json", &catchment::json)
            .def(self == self)
            .def(self != self)
            ;

        typedef std::vector<catchment_> CatchmentList;
        class_<CatchmentList>("CatchmentList")
            .def(vector_indexing_suite<CatchmentList, true>())
            ;
    }

    void water_route_stuff() {
        using namespace boost::python;
        using namespace shyft::energy_market::hydro_power;
        // -- method overloading pointers:
        waterway_ (*input_from_w)( waterway_ const&me,waterway_ const& )=&waterway::input_from;
        waterway_ (*input_from_p)( waterway_ const&me, unit_ const& ) = &waterway::input_from;
        waterway_ (*input_from_r_r)( waterway_ const&me, reservoir_ const& , connection_role) = &waterway::input_from;
        waterway_ (*input_from_r)( waterway_ const&me, reservoir_ const& ) = &waterway::input_from;
        waterway_ (*output_to_w)( waterway_ const&me, waterway_ const& ) = &waterway::output_to;
        waterway_ (*output_to_r)( waterway_ const&me, reservoir_ const& ) = &waterway::output_to;
        waterway_ (*output_to_p)( waterway_ const&me, unit_ const& ) = &waterway::output_to;

        class_<waterway, bases<hydro_component>, std::shared_ptr<waterway>, boost::noncopyable>("Waterway", 
            doc_intro("The waterway can be a river or a tunnel, and connects the reservoirs, units(turbine).")
             )
            .def(init<int,const string&,const string&,hydro_power_system_ const&>((py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json"),py::arg("hps")), doc_intro("tbd")))
            .def_readonly("downstream",&waterway::downstream,doc_intro("returns downstream object(if any)"))
            .def("add_gate",&waterway::add_gate,(py::arg("self"),py::arg("gate")),doc_intro("add a gate to the waterway"))
            .def("remove_gate",&waterway::remove_gate,(py::arg("self"),py::arg("gate")),doc_intro("remove a gate from the waterway"))
            .def_readonly("gates",&waterway::gates,doc_intro("the gates attached to the inlet of the waterway"))
            .def(self == self)
            .def(self != self)
        ;
        typedef std::vector<waterway_> WaterwayList;
        class_<WaterwayList>("WaterwayList")
            .def(vector_indexing_suite<WaterwayList, true>())
            ;

        class_<gate, bases<>, std::shared_ptr<gate>, boost::noncopyable>("Gate", 
            doc_intro(
            "A gate controls the amount of flow into the waterway by the gate-opening.\n"
            "In the case of tunnels, it's usually either closed or open.\n"
            "For reservoir flood-routes, the gate should be used to model the volume-flood characteristics.\n"
            "The resulting flow through a waterway is a function of many factors, most imporant:\n"
            " * gate opening and gate-characteristics\n"
            " * upstream water-level\n"
            " * downstrem water-level(in some-cases)\n"
            " * waterway properties(might be state dependent)\n"
            )
        )
        .def(init<int,const string&,const string&>((py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")),doc_intro("construct a new gate")))
        .def_readwrite("id",&gate::id,doc_intro("unique id for this gate"))
        .def_readwrite("name",&gate::name,doc_intro("name of the gate"))
        .def_readwrite("json",&gate::json,doc_intro("json for storing py-type data"))
        .def_readonly("water_route",&gate::wtr,doc_intro("deprecated:use waterway"))
        .def_readonly("waterway",&gate::wtr,doc_intro("ref. to the waterway where this gate controls the flow"))
        
        .def(self==self)
        .def(self!=self)
        ;
        typedef std::vector<gate_> GateList;
        class_<GateList>("GateList")
            .def(vector_indexing_suite<GateList, true>())
            ;

        def("wtr_input_from", input_from_w, args("wtr","waterway"), doc_intro(""));
        def("wtr_input_from", input_from_p, args("wtr", "unit"), doc_intro(""));
        def("wtr_input_from", input_from_r_r, args("wtr", "reservoir", "connection_role"), doc_intro(""));
        def("wtr_input_from", input_from_r, args("wtr", "reservoir"), doc_intro("connection_role is main"));
        def("wtr_output_to", output_to_w, args("wtr", "waterway"), doc_intro(""));
        def("wtr_output_to", output_to_r, args("wtr", "reservoir"), doc_intro(""));
        def("wtr_output_to", output_to_p, args("wtr", "unit"), doc_intro(""));

    }

    void reservoir_stuff() {
        using namespace boost::python;
        using namespace shyft::energy_market::hydro_power;

        class_<reservoir, bases<hydro_component>, std::shared_ptr<reservoir>, boost::noncopyable>("Reservoir", doc_intro(""),no_init)
            .def(init<int,const string&,const string&,hydro_power_system_ const&>((py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json"),py::arg("hps")), doc_intro("tbd")))
            .add_property("downstream_power_stations",&reservoir::downstream_powerstations,doc_intro("deprecated:use downstream_power_plants"))
            .add_property("upstream_power_stations",&reservoir::upstream_powerstations,doc_intro("deprecated:use upstream_power_plants"))
            .add_property("downstream_power_plants",&reservoir::downstream_powerstations,doc_intro("return down-stream power plants list"))
            .add_property("upstream_power_plants",&reservoir::upstream_powerstations,doc_intro("upstream power plants"))
            .def(self == self)
            .def(self != self)
            ;
                
        def("rsv_input_from",&reservoir::input_from,(py::arg("reservoir"),py::arg("waterway")),doc_intro("tbd"),py::return_arg<1>());
        def("rsv_output_to",&reservoir::output_to,(py::arg("reservoir"),py::arg("waterway"),py::arg("role")),doc_intro("tbd"),py::return_arg<1>());

        typedef std::vector<reservoir_> ReservoirList;
        class_<ReservoirList>("ReservoirList")
            .def(vector_indexing_suite<ReservoirList, true>())
            ;
    }
    void power_plant_stuff() {
        using namespace boost::python;
        using namespace shyft::energy_market::hydro_power;
        using shyft::energy_market::id_base;
        
        class_<unit, bases<hydro_component>, std::shared_ptr<unit>, boost::noncopyable>(
            "Unit", 
            doc_intro(
                "An Unit consist of a turbine and a connected generator.\n"
                "The turbine is hydrologically connected to upstream tunnel and downstream tunell/river.\n"
                "The generator part is connected to the electrical grid through a busbar.\n"
                ""
                "In the long term models, the entire power plant is represented by a virtual unit that represents\n"
                "the total capability of the power-plant.\n"
                "\n"
                "The short-term detailed models, usually describes every aggratate up to a granularity that is\n"
                "relevant for the short-term optimization/simulation horizont.\n"
                "\n"
                "A power plant is a collection of one or more units that are natural to group into one power plant."
            )
            )
            .def(init<int,const string&,const string&,hydro_power_system_ const&>((py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json"),py::arg("hps")), doc_intro("tbd")))
            .add_property("downstream", &unit::downstream, doc_intro("returns downstream waterway(river/tunnel) object(if any)"))
            .add_property("upstream",&unit::upstream,doc_intro("returns upstream tunnel(water-route) object(if any)"))
            .add_property("upstreams_reservoirs",&unit::upstream_reservoirs,doc_intro("a list of reservoir that feeds this power-plant, usually 1, but could be more"))
            .add_property("power_station",&unit::pwr_station_,doc_intro("deprecated: use power_plant"))
            .add_property("power_plant",&unit::pwr_station_,doc_intro("return the hydro power plant associated with this unit"))
            .def("input_from",&unit::input_from,args("water_route"),doc_intro("connect the output from the waterway to the input of the power plant"))
            .def("output_to",&unit::output_to,args("water_route"),doc_intro("connect the output of the plant to the input of the input of the waterway"))
            .def(self == self)
            .def(self != self)
            ;
        
        typedef std::vector<unit_> UnitList;
        class_<UnitList>("UnitList")
            .def(vector_indexing_suite<UnitList, true>())
        ;
        
        class_<power_plant, bases<>, std::shared_ptr<power_plant>, boost::noncopyable>(
        "PowerPlant", 
        doc_intro(
            "A hydro power plant is the site/building that contains a number of units.\n"
            "The attributes of the power plant, are typically sum-requirement and/or operations that applies\n"
            "all of the units."
            "\n"
        )
        )
        .def(init<int,const string&,const string&,hydro_power_system_ const&>((py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json"),py::arg("hps")), doc_intro("tbd")))
        .add_property("hps", &power_plant::hps_, doc_intro("returns the hydro-power-system this component is a part of"))
        .def_readwrite("id", &power_plant::id, doc_intro("a system assigned unique id"))
        .def_readwrite("name", &power_plant::name, doc_intro("the name of the component, subject to unique-constraints"))
        .def_readwrite("json",&power_plant::json, doc_intro("some info as json"))

        .def_readwrite("units",&power_plant::units,doc_intro("associated aggregates"))
        .def("add_unit",&power_plant::add_unit,(py::arg("self"),py::arg("aggregate")),doc_intro("add unit to plant"))
        .def("remove_unit",&power_plant::remove_unit,(py::arg("self"),py::arg("aggregate")),doc_intro("remove unit from plant"))
        // bw compat
        .def_readwrite("aggregates",&power_plant::units,doc_intro("deprecated: use units"))
        .def("add_aggregate",&power_plant::add_unit,(py::arg("self"),py::arg("aggregate")),doc_intro("deprecated:use add_unit"))
        .def("remove_aggregate",&power_plant::remove_unit,(py::arg("self"),py::arg("aggregate")),doc_intro("deprecated:use remove_unit"))
        .def(self == self)
        .def(self != self)
        ;
        typedef std::vector<power_plant_> PowerStationList;
        class_<PowerStationList>("PowerPlantList")
            .def(vector_indexing_suite<PowerStationList, true>())
        ;
        
    }

    void hydro_component_stuff() {
        using namespace boost::python;
        using namespace shyft::energy_market::hydro_power;
        enum_<connection_role>("ConnectionRole")
            .value("main", connection_role::main)
            .value("bypass", connection_role::bypass)
            .value("flood", connection_role::flood)
            .value("input", connection_role::input)
            .export_values()
        ;
        typedef hydro_connection HydroConnection;
        class_<HydroConnection,bases<>,HydroConnection,boost::noncopyable>("HydroConnection", doc_intro(""),no_init)
            //.def(init<connection_role,hydro_component_ const&>(args("a_role","a_target"),doc_intro("tbd")))
            .def_readwrite("role",&HydroConnection::role,doc_intro("role like main,bypass,flood,input"))
            .add_property("target",&HydroConnection::target_,doc_intro("target of the hydro-connection, Reservoir|Aggregate|Waterway"))
            .add_property("has_target",&HydroConnection::has_target,doc_intro("true if valid/available target"))
            ;
        typedef std::vector<HydroConnection> HydroConnectionList;
        class_<HydroConnectionList>("HydroConnectionList")
            .def(vector_indexing_suite<HydroConnectionList>())
        ;


        typedef hydro_component HydroComponent;
        class_<HydroComponent, bases<>, std::shared_ptr<HydroComponent>, boost::noncopyable>("HydroComponent",
            doc_intro("tbd")
            ,boost::python::no_init
            )
            .add_property("hps", &HydroComponent::hps_, doc_intro("returns the hydro-power-system this component is a part of"))
            .def_readwrite("id", &HydroComponent::id, doc_intro("a system assigned unique id"))
            .def_readwrite("name", &HydroComponent::name, doc_intro("the name of the component, subject to unique-constraints"))
            .def_readwrite("json",&HydroComponent::json, doc_intro("some info as json"))
            .def_readonly("upstreams", &HydroComponent::upstreams, doc_intro("list of hydro-components that are conceptually upstreams"))
            .def_readonly("downstreams", &HydroComponent::downstreams, doc_intro("list of hydro-components that are conceptually downstreams"))
            .add_property("obj", &py_object_ext<HydroComponent>::get_obj, &py_object_ext<HydroComponent>::set_obj, "a python object")
            .def("disconnect", &HydroComponent::disconnect, args("comp1", "comp2"), doc_intro("disconnect two components")).staticmethod("disconnect");

            // downstreams
        ;
    }

    struct hps_ext {
        static std::vector<char> to_blob(const std::shared_ptr<shyft::energy_market::hydro_power::hydro_power_system>& hps) {
            auto s=shyft::energy_market::hydro_power::hydro_power_system::to_blob(hps);
            return std::vector<char>(s.begin(),s.end());
        }
        static std::shared_ptr<shyft::energy_market::hydro_power::hydro_power_system> from_blob(std::vector<char>&blob) {
            string s(blob.begin(),blob.end());
            return shyft::energy_market::hydro_power::hydro_power_system::from_blob(s);
        }
    };

    void hydro_power_system_stuff() {
        using namespace boost::python;
        using namespace shyft::energy_market::hydro_power;
        typedef hydro_power_system HydroPowerSystem;
        class_<HydroPowerSystem,bases<>,std::shared_ptr<HydroPowerSystem>,boost::noncopyable>("HydroPowerSystem", doc_intro(""),no_init)
            .def(init<string>(args("name"), doc_intro("tbd")))
            .def(init<int,string>((py::arg("id"),py::arg("name")),doc_intro("a new hps")))
            .def_readwrite("name", &HydroPowerSystem::name, doc_intro("tbd"))
            .def_readwrite("id", &HydroPowerSystem::id, doc_intro("tbd"))
            .def_readwrite("created", &HydroPowerSystem::created, doc_intro("tbd"))
            .def_readonly("water_routes",&HydroPowerSystem::waterways,"deprecated:use waterways")
            .def_readonly("waterways",&HydroPowerSystem::waterways,"all the waterways(tunells,rivers) of the system")
            .def_readonly("aggregates",&HydroPowerSystem::units,"deprecated: use units")
            .def_readonly("units",&HydroPowerSystem::units,"all the hydropower units in this system")
            .def_readonly("reservoirs",&HydroPowerSystem::reservoirs,"all the reservoirs")
            .def_readonly("catchments",&HydroPowerSystem::catchments,"all the catchments for the system")
            .def_readonly("power_stations",&HydroPowerSystem::power_plants,"deprecated: use power_plant")
            .def_readonly("power_plants",&HydroPowerSystem::power_plants,"all power plants, with references to units")
//             .add_property("obj", &py_object_ext<HydroPowerSystem>::get_obj, &py_object_ext<HydroPowerSystem>::set_obj, "a python object")
            .def("find_aggregate_by_name",&HydroPowerSystem::find_unit_by_name,(py::arg("self"),py::arg("name")),"deprecated: use find_unit_by_name")
            .def("find_unit_by_name",&HydroPowerSystem::find_unit_by_name,(py::arg("self"),py::arg("name")),"returns object that exactly  matches name")
            .def("find_reservoir_by_name", &HydroPowerSystem::find_reservoir_by_name, (py::arg("self"), py::arg("name")), "returns object that exactly  matches name")
            .def("find_water_route_by_name", &HydroPowerSystem::find_waterway_by_name, (py::arg("self"), py::arg("name")), "deprecated:use find_waterway_by_name")
            .def("find_waterway_by_name", &HydroPowerSystem::find_waterway_by_name, (py::arg("self"), py::arg("name")), "returns object that exactly  matches name")
            .def("find_power_station_by_name", &HydroPowerSystem::find_power_plant_by_name, (py::arg("self"), py::arg("name")), "deprecated:use find_power_plant_by_name")
            .def("find_power_plant_by_name", &HydroPowerSystem::find_power_plant_by_name, (py::arg("self"), py::arg("name")), "returns object that exactly  matches name")
            .def("find_aggregate_by_id", &HydroPowerSystem::find_unit_by_id, (py::arg("self"), py::arg("id")), "deprecated:use find_unit_by_id")
            .def("find_unit_by_id", &HydroPowerSystem::find_unit_by_id, (py::arg("self"), py::arg("id")), "returns object with specified id")
            .def("find_reservoir_by_id", &HydroPowerSystem::find_reservoir_by_id, (py::arg("self"), py::arg("id")), "returns object with specified id")
            .def("find_water_route_by_id", &HydroPowerSystem::find_waterway_by_id, (py::arg("self"), py::arg("id")), "deprecated:use find_waterway_by_id")
            .def("find_waterway_by_id", &HydroPowerSystem::find_waterway_by_id, (py::arg("self"), py::arg("id")), "returns object with specified id")
            .def("find_power_station_by_id", &HydroPowerSystem::find_power_plant_by_id, (py::arg("self"), py::arg("id")), "deprecated:use find_power_plant_by_id")
            .def("find_power_plant_by_id", &HydroPowerSystem::find_power_plant_by_id, (py::arg("self"), py::arg("id")), "returns object with specified id")

            .def("equal_structure",&HydroPowerSystem::equal_structure,args("other_hps"),doc_intro("tbd"))
            .def("to_blob_ref", &hps_ext::to_blob,args("me"),
                doc_intro("serialize the model into an blob")
                doc_returns("blob", "string", "blob-serialized version of the model")
                doc_see_also("from_blob")
            ).staticmethod("to_blob_ref")
            .def("from_blob", &hps_ext::from_blob, args("blob_string"),
                doc_intro("constructs a model from a blob_string previously created by the to_blob method")
                doc_parameters()
                doc_parameter("blob_string", "string", "blob-formatted representation of the model, as create by the to_blob method")
            ).staticmethod("from_blob")
            ;
        
        class_<hydro_power_system_builder>("HydroPowerSystemBuilder", doc_intro("class to support building hydro-power-systems save and easy"), no_init)
            .def(init<hydro_power_system_&>(args("hydro_power_system")))
            .def("create_river", &hydro_power_system_builder::create_river, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")), doc_intro("create and add river to the system"))
            .def("create_tunnel", &hydro_power_system_builder::create_tunnel, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")), doc_intro("create and add river to the system"))
            .def("create_water_route", &hydro_power_system_builder::create_waterway, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")), doc_intro("deprecated:use create_waterway"))
            .def("create_waterway", &hydro_power_system_builder::create_waterway, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")), doc_intro("create and add river to the system"))
            .def("create_aggregate", &hydro_power_system_builder::create_unit, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")), doc_intro("deprecated:use create_unit"))
            .def("create_unit", &hydro_power_system_builder::create_unit, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")), doc_intro("creates a new unit with the specified parameters"))
            .def("create_reservoir", &hydro_power_system_builder::create_reservoir, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")), doc_intro("tbd"))
            .def("create_power_station", &hydro_power_system_builder::create_power_plant, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")), doc_intro("deprecated: use create_power_plant"))
            .def("create_power_plant", &hydro_power_system_builder::create_power_plant, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")), doc_intro("tbd"))
            .def("create_catchment", &hydro_power_system_builder::create_catchment, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")), doc_intro("create and add watermark series to the system"))
            ;
    }

    void all_of_hydro_power_system() {
        // call each expose f here
        catchment_stuff();
        hydro_component_stuff();
        water_route_stuff();
        reservoir_stuff();
        power_plant_stuff();
        hydro_power_system_stuff();
    }
}
