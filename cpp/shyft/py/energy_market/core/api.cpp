#include <shyft/py/energy_market/api_utils.h>
#include <boost/python/docstring_options.hpp>
#include <shyft/core/utctime_utilities.h>
#include <shyft/energy_market/market/model.h>
#include <shyft/energy_market/market/model_area.h>
#include <shyft/energy_market/market/power_module.h>
#include <shyft/energy_market/market/power_line.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/py/api/py_convertible.h>
#include <string>
#include <string_view>

namespace py=boost::python;

namespace expose {
    using std::shared_ptr;
    using std::string;

    template<class T>
    string py_str_of_typed_list(string type, const std::vector<T>& list, size_t indent);

    string py_str(const shyft::energy_market::hydro_power::point& v, size_t = 0) {
        return string{"Point("} + std::to_string(v.x) + ", " + std::to_string(v.y) + ")";
    }

    string py_str(const std::vector<shyft::energy_market::hydro_power::point>& v, size_t indent = 0) {
        return py_str_of_typed_list("PointList", v, indent);
    }

    string py_str(const shyft::energy_market::hydro_power::xy_point_curve& v, size_t indent = 0) {
        std::string r{ "XyPointCurve(\n" };
        r += std::string(indent,' ') + " points = " + py_str(v.points, indent+1);
        r += ")";
        return r;
    }

    string py_str(const std::vector<shyft::energy_market::hydro_power::xy_point_curve>& v, size_t indent = 0) {
        return py_str_of_typed_list("XyPointCurveList", v, indent);
    }

    string py_str(const shyft::energy_market::hydro_power::xy_point_curve_with_z& v, size_t indent=0) {
        string r{ "XyPointCurveWithZ(\n" };
        r += string(indent,' ') + " z = " + std::to_string(v.z) + ",\n";
        r += string(indent,' ') + " xy_point_curve = " + py_str(v.xy_curve, indent+1);
        r += ")";
        return r;
    }
    string py_str(const std::vector<shyft::energy_market::hydro_power::xy_point_curve_with_z>& v, size_t indent = 0) {
        return py_str_of_typed_list("XyPointCurveWithZList", v, indent);
    }

    string py_str(const shyft::energy_market::hydro_power::turbine_efficiency& v, size_t indent = 0) {
        string r{ "TurbineEfficiency(\n" };
        r += string(indent, ' ') + " production_min = " + std::to_string(v.production_min) + ",\n";
        r += string(indent, ' ') + " production_max = " + std::to_string(v.production_max) + ",\n";
        r += string(indent, ' ') + " efficiency_curves = " + py_str_of_typed_list("XyPointCurveWithZList",v.efficiency_curves, indent + 1);
        r += ")";
        return r;
    }

    string py_str(const std::vector<shyft::energy_market::hydro_power::turbine_efficiency>& v, size_t indent=0) {
        return py_str_of_typed_list("TurbineEfficiencyList", v, indent);
    }

    string py_str(const shyft::energy_market::hydro_power::turbine_description& v) {
        string r{ "TurbineDescription(\n" };
        r += " efficiencies = " + py_str(v.efficiencies, 1);
        r += ")";
        return r;
    }

    template<class T>
    string py_str_of_typed_list(string type, const std::vector<T>& list, size_t indent) {
        string r = type + string("([");
        if (!list.empty()) {
            auto it = list.cbegin();
            r += "\n " + string(indent, ' ') + py_str(*it, indent + 1);
            while (++it != list.cend())
                r += ",\n " + string(indent, ' ') + py_str(*it, indent + 1);
        }
        r += "])";
        return r;
    }

    void int_string_dict() {
        using namespace boost::python;
        using IntStringDict=std::map<int, string> ;
        class_<IntStringDict>("IntStringDict",
            doc_intro("A dict of int,string, the key-value is the int"))
            .def(map_indexing_suite<IntStringDict, true>()) 
            .def(init<const IntStringDict&>(args("clone_me")))
        ;
    }

    void xy_point_curves_etc() {
        using namespace boost::python;
        using Point = shyft::energy_market::hydro_power::point;
        using std::vector;

        class_<Point>("Point", doc_intro("Simply a point (x,y)"))
            .def(init<double,double>(args("x","y"), doc_intro("construct a point with x and y")))
            .def(init<const Point&>((py::arg("clone")),doc_intro("Create a clone.")))
            .def_readwrite("x",&Point::x)
            .def_readwrite("y", &Point::y)
            .def(self == self)
            .def(self != self)
            .def<string(*)(const Point&)>("__str__", [](const Point&p)->string{return py_str(p);})
            .def<string(*)(const Point&)>("__repr__", [](const Point&p)->string{return py_str(p);})
        ;

        typedef std::vector<Point> PointList;
        class_<PointList>("PointList", doc_intro("A strongly typed list of Point."))
            .def(vector_indexing_suite<PointList>())
            .def(init<const PointList&>((args("clone")),doc_intro("Create a clone.")))
            .def(self == self)
            .def(self != self)
            .def<string(*)(const PointList&)>("__str__", [](const PointList&p)->string{return py_str(p);})
            .def<string(*)(const PointList&)>("__repr__", [](const PointList&p)->string{return py_str(p);})
        ;
        py_api::iterable_converter().from_python<PointList>();

        using XyPointCurve = shyft::energy_market::hydro_power::xy_point_curve;
        class_<XyPointCurve,py::bases<>,shared_ptr<XyPointCurve>>("XyPointCurve", doc_intro(""))
            .def(init<const PointList&>(args("points")))
            .def(init<const std::vector<double>&,const std::vector<double>&>(args("x_vector","y_vector")))
            .def(init<const XyPointCurve&>((args("clone")), doc_intro("Create a clone.")))
            .def_readwrite("points",&XyPointCurve::points)
            .def("calculate_x",&XyPointCurve::calculate_x,(py::arg("self"),py::arg("y")),doc_intro("interpolating and extending"))
            .def("calculate_y", &XyPointCurve::calculate_x, (py::arg("self"),py::arg("x")), doc_intro("interpolating and extending"))
            .def("is_mono_increasing",&XyPointCurve::is_xy_mono_increasing,doc_intro("true if y=f(x) is monotone and increasing"))
            .def(self == self)
            .def(self != self)
            .def<string(*)(const XyPointCurve&)>("__str__", [](const XyPointCurve&p)->string{return py_str(p);})
            .def<string(*)(const XyPointCurve&)>("__repr__", [](const XyPointCurve&p)->string{return py_str(p);})
        ;

        using XyPointCurveList = std::vector<XyPointCurve>;
        class_<XyPointCurveList>(
            "XyPointCurveList", doc_intro("A strongly typed list of XyPointCurve."))
            .def(vector_indexing_suite<XyPointCurveList>())
            .def(init<const XyPointCurveList&>((args("clone")),doc_intro("Create a clone.")))
            .def(self == self)
            .def(self != self)
            .def<string(*)(const XyPointCurveList&)>("__str__", [](const XyPointCurveList&p)->string{return py_str(p);})
            .def<string(*)(const XyPointCurveList&)>("__repr__", [](const XyPointCurveList&p)->string{return py_str(p);})
        ;
        py_api::iterable_converter().from_python<XyPointCurveList>();

        using XyPointCurveWithZ = shyft::energy_market::hydro_power::xy_point_curve_with_z;
        class_<XyPointCurveWithZ, py::bases<>, shared_ptr<XyPointCurveWithZ>>(
            "XyPointCurveWithZ", doc_intro("A XyPointCurve with a reference value z."))
            .def(init<const XyPointCurve&,double>(args("xy_point_curve","z")))
            .def(init<const XyPointCurveWithZ&>((args("clone")), doc_intro("Create a clone.")))
            .def_readwrite("xy_point_curve", &XyPointCurveWithZ::xy_curve)
            .def_readwrite("z",&XyPointCurveWithZ::z)
            .def(self == self)
            .def(self != self)
            .def<string(*)(const XyPointCurveWithZ&)>("__str__",  [](const XyPointCurveWithZ&p)->string{return py_str(p);})
            .def<string(*)(const XyPointCurveWithZ&)>("__repr__", [](const XyPointCurveWithZ&p)->string{return py_str(p);})
        ;

        using XyPointCurveWithZList=vector<XyPointCurveWithZ>;
        class_<XyPointCurveWithZList>(
            "XyPointCurveWithZList", doc_intro("A strongly typed list of XyPointCurveWithZ."))
            .def(vector_indexing_suite<XyPointCurveWithZList>())
            .def(init<const XyPointCurveWithZList&>((args("clone")), doc_intro("Create a clone.")))
            .def(self == self)
            .def(self != self)
            .def<string(*)(const XyPointCurveWithZList&)>("__str__",  [](const XyPointCurveWithZList&p)->string{return py_str(p);})
            .def<string(*)(const XyPointCurveWithZList&)>("__repr__",  [](const XyPointCurveWithZList&p)->string{return py_str(p);})
        ;
        py_api::iterable_converter().from_python<XyPointCurveWithZList>();

        using TurbineEfficiency = shyft::energy_market::hydro_power::turbine_efficiency;
        class_<TurbineEfficiency, py::bases<>, shared_ptr<TurbineEfficiency>>(
            "TurbineEfficiency",
                doc_intro("A turbine efficiency.")
                doc_details("Defined by a set of efficiency curves, one for each net head, with an optional production min/max limit.\n"
                            "For Pelton turbines there is one turbine efficiency object for each needle combination.\n"
                            "For other turbines there is only one turbine effieciency object, describing the entire turbine."))
            .def(init<const XyPointCurveWithZList&>(args("efficiency_curves")))
            .def(init<const XyPointCurveWithZList&,double,double>(args("efficiency_curves","production_min","production_max")))
            .def(init<const TurbineEfficiency&>((args("clone")), doc_intro("Create a clone.")))
            .def_readwrite("efficiency_curves", &TurbineEfficiency::efficiency_curves,
                doc_intro("A list of XyPointCurveWithZ efficiency curves for the net head range of the turbine or needle combination."))
            .def_readwrite("production_min", &TurbineEfficiency::production_min,
                doc_intro("The minimum production for which the efficiency curves are valid.")
                doc_notes()
                doc_note("Only relevant when representing a Pelton needle combination."))
            .def_readwrite("production_max", &TurbineEfficiency::production_max,
                doc_intro("The maximum production for which the efficiency curves are valid.")
                doc_notes()
                doc_note("Only relevant when representing a Pelton needle combination."))
            .def(self == self)
            .def(self != self)
            .def<string(*)(const TurbineEfficiency&)>("__str__",  [](const TurbineEfficiency&p)->string{return py_str(p);})
            .def<string(*)(const TurbineEfficiency&)>("__repr__", [](const TurbineEfficiency&p)->string{return py_str(p);})
        ;

        using TurbineEfficiencyList=vector<TurbineEfficiency>;
        class_<TurbineEfficiencyList>(
            "TurbineEfficiencyList", doc_intro("A strongly typed list of TurbineEfficiency."))
            .def(vector_indexing_suite<TurbineEfficiencyList>())
            .def(init<const TurbineEfficiencyList&>((args("clone")), doc_intro("Create a clone.")))
            .def(self == self)
            .def(self != self)
            .def<string(*)(const TurbineEfficiencyList&)>("__str__",[](const TurbineEfficiencyList&p)->string{return py_str(p);})
            .def<string(*)(const TurbineEfficiencyList&)>("__repr__", [](const TurbineEfficiencyList&p)->string{return py_str(p);})
        ;
        py_api::iterable_converter().from_python<TurbineEfficiencyList>();

        using TurbineDescription = shyft::energy_market::hydro_power::turbine_description;
        class_<TurbineDescription, py::bases<>, shared_ptr<TurbineDescription>>(
            "TurbineDescription",
                doc_intro("Describes a turbine with all turbine efficiencies.")
                doc_details("For Pelton turbines there are multiple sets of efficiency curves; one set for each needle combination,\n"
                            "where each of them may contain multiple efficiency curves; one for each net head.\n"
                            "For other turbines there is only one set of efficiency curves, describing the entire turbine;\n"
                            "one for each net head."))
            .def(init<const TurbineEfficiencyList&>(args("efficiencies")))
            .def(init<const TurbineDescription&>((args("clone")), doc_intro("Create a clone.")))
            .def_readwrite("efficiencies", &TurbineDescription::efficiencies,
                doc_intro("A list of TurbineEfficiency.")
                doc_details("Containing a single entry describing the entire turbine, or one entry for each Pelton needle combinations."))
            .def(self == self)
            .def(self != self)
            .def<string(*)(const TurbineDescription&)>("__str__", [](const TurbineDescription&p)->string{return py_str(p);})
            .def<string(*)(const TurbineDescription&)>("__repr__", [](const TurbineDescription&p)->string{return py_str(p);})
        ;

    }

    using std::vector;
    vector<shyft::energy_market::hydro_power::point> create_from_vectors(const vector<double>&x,const vector<double>&y) {
        if(x.size()!=y.size())
            throw std::runtime_error("diff size");
        vector<shyft::energy_market::hydro_power::point> r;
        for(size_t i=0; i<x.size();++i) {
            r.push_back(shyft::energy_market::hydro_power::point(x[i],y[i]));
        }
        return r;
    }
    void point_vector_from_x_y() {
        py::def("points_from_x_y",create_from_vectors,(py::arg("x"),py::arg("y")));
    }

    struct mod_ext {
        static std::vector<char> to_blob(const std::shared_ptr<shyft::energy_market::market::model>& m) {
            auto s=m->to_blob();
            return std::vector<char>(s.begin(),s.end());
        }
        static std::shared_ptr<shyft::energy_market::market::model> from_blob(std::vector<char>&blob) {
            string s(blob.begin(),blob.end());
            return shyft::energy_market::market::model::from_blob(s);
        }
    };

    void model() {
        using namespace boost::python;
        using Model = shyft::energy_market::market::model;
        using ModelArea = shyft::energy_market::market::model_area;
        using ModelBuilder = shyft::energy_market::market::model_builder;
            
        class_<ModelBuilder>("ModelBuilder", 
            doc_intro("This class helps building an EMPS model, step by step"),
            no_init
            )
            .def(init<std::shared_ptr<Model>&>( (py::arg("model")),
                doc_intro("Make a model-builder for the model")
                doc_intro("The model can be modified/built using the methods")
                doc_intro("available in this class")
                doc_parameters()
                doc_parameter("model","Model","the model to be built/modified")
                )
            )
            .def("create_power_line", &ModelBuilder::create_power_line, (py::arg("self"),py::arg("a"),py::arg("b"),py::arg("id"),py::arg("name"),py::arg("json")=""),
                doc_intro("create and add a power line with capacity_MW between area a and b to the model")
                doc_parameters()
                doc_parameter("a", "ModelArea", "from existing model-area, that is part of the current model")
                doc_parameter("b", "ModelArea", "to existing model-area, that is part of the current model")
                doc_parameter("id", "int", "unique ID of the power-line")
                doc_parameter("name", "string", "unique name of the power-line")
                doc_parameter("json","string","json for the power-line")
                doc_returns("pl", "PowerLine", "the newly created power-line, that is now a part of the model")
            )
            .def("create_model_area", &ModelBuilder::create_model_area, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")=""),
                doc_intro("create and add an area to the model.")
                doc_intro("ensures that area_name, and that area_id is unique.")
                doc_parameters()
                doc_parameter("id", "int", "unique identifier for the area, must be unique within model")
                doc_parameter("name", "string", "any valid area-name, must be unique within model")
                doc_parameter("json","string","json for the area")
                doc_returns("area", "ModelArea", "a reference to the newly added area")
                doc_see_also("add_area")
            )
            .def("create_power_module", &ModelBuilder::create_power_module,
                (py::arg("self"),py::arg("model_area"),py::arg("id"),py::arg("name"),py::arg("json")),
                doc_intro("create and add power-module to the area, doing validity checks")
                doc_parameters()
                doc_parameter("model_area","ModelArea","the model-area for which we create a power-module")
                doc_parameter("id", "string", "encoded power_type/load/wind module id")
                doc_parameter("module_name", "string", "unique module-name for each area")
                doc_parameter("json","string","json for the pm")
                doc_returns("pm", "PowerModule", "a reference to the created and added power-module")
            )
            ;

        class_<Model, bases<>, std::shared_ptr<Model>, boost::noncopyable>(
            "Model",
            doc_intro("The Model class describes the  LTM (persisted) model")
            doc_intro("A model consists of model_areas and power-lines interconnecting them.")
            doc_intro("To buid a model use the .add_area() and .add_power_line() methods")
            doc_see_also("ModelArea,PowerLine,PowerModule")
            )
            .def(init<int,const string&,py::optional<const string&>>( (py::arg("id"),py::arg("name"),py::arg("json")),
                doc_intro("constructs a Model object with the specified parameters")
                doc_parameters()
                doc_parameter("id","int","a global unique identifier of the mode")
                doc_parameter("name","string","the name of the model")
                doc_parameter("json","string","extra info as json for the model")
                )
            )
            .def_readwrite("name",&Model::name,doc_intro("The name of the model"))
            .def_readwrite("id",&Model::id,doc_intro("The unique id of the model"))
            .def_readwrite("json",&Model::json,doc_intro("The json data-carrier of the model"))
            .def_readwrite("created",&Model::created,doc_intro("The timestamp when the model was created, utc seconds 1970"))
            .def_readonly("area",&Model::area, doc_intro("a dict(area-name,area) for the model-areas")) //return_internal_reference<>()
            .def_readonly("power_lines",&Model::power_lines,doc_intro("a list of power-lines,each with connection to the areas they interconnect"))
            .def("equal_structure",&Model::equal_structure,args("other_model"),
                doc_intro("Compare this model with other_model for equality.")
                doc_parameters()
                doc_parameter("other_model","Model","The model to compare with")
                doc_returns("equal","bool","true if other_model is equal to self, disregarding the name,id, and created properties")
            )
            .add_property("obj", &py_object_ext<Model>::get_obj, &py_object_ext<Model>::set_obj, "a python object")
            .def( self == self)
            .def( self != self)
            .def("to_blob",&mod_ext::to_blob,
                doc_intro("serialize the model into a blob")
                doc_returns("blob","ByteVector","serialized version of the model")
                doc_see_also("from_blob")
            )
            .def("from_blob",&mod_ext::from_blob,args("blob"),
                doc_intro("constructs a model from a blob previously created by the to_blob method")
                doc_parameters()
                doc_parameter("blob","ByteVector","blob representation of the model, as create by the to_blob method")
            ).staticmethod("from_blob")
            ;
            using ModelList=vector<shared_ptr<Model>>;
            py::class_<ModelList>("ModelList", "A strongly typed list, vector, of models")
                .def(py::vector_indexing_suite<ModelList, true>())
            ;
    }
    void model_area() {
        using namespace boost::python;
        using ModelArea = shyft::energy_market::market::model_area;
        class_<ModelArea, bases<>, std::shared_ptr<ModelArea>, boost::noncopyable>(
            "ModelArea",
            doc_intro("The ModelArea class describes the EMPS LTM (persisted) model-area")
            doc_intro("A model-area consists of power modules and hydro-power-system.")
            doc_intro("To buid a model-are use the .add_power_module() and the hydro-power-system builder")
            doc_see_also("Model,PowerLine,PowerModule,HydroPowerSystem")
            )
            .def(init<std::shared_ptr<shyft::energy_market::market::model>const&,int,const string&,py::optional<const string&>>((py::arg("model"), py::arg("id"),py::arg("name"),py::arg("json")),
                doc_intro("constructs a ModelArea object with the specified parameters")
                doc_parameters()
                doc_parameter("model","Model","the model owning the created model-area")
                doc_parameter("id", "int", "a global unique identifier of the model-area")
                doc_parameter("name", "string", "the name of the model-area")
                doc_parameter("json", "string", "extra info as json")
                )
            )
            .def_readwrite("name", &ModelArea::name, doc_intro("The name of the model-area"))
            .def_readwrite("id", &ModelArea::id, doc_intro("The unique id of the model-area"))
            .def_readwrite("json", &ModelArea::id, doc_intro("the json for the model-area"))
            .def_readonly("power_modules",&ModelArea::power_modules,
                doc_intro("the power-modules in this area, a dictionary using power-module unique id"))
            .add_property("detailed_hydro",&ModelArea::get_detailed_hydro,&ModelArea::set_detailed_hydro,
                doc_intro("a detailed hydro description.")
                doc_see_also("HydroPowerSystem")
            )
            .add_property("model",&ModelArea::get_model,"the emps model for the area")
            .add_property("obj", &py_object_ext<ModelArea>::get_obj, &py_object_ext<ModelArea>::set_obj, "a python object")
            .def(self == self)
            .def(self != self)
        ;
        typedef std::map<int, std::shared_ptr<ModelArea>> ModelAreaDict;
        class_<ModelAreaDict>("ModelAreaDict",
            doc_intro("A dict of ModelArea, the key-value is the area-name"))
            .def(map_indexing_suite<ModelAreaDict,true>()) // true since the shared_ptr is already a proxy
            .def(init<const ModelAreaDict&>(args("clone_me")))
            ;

    }

    void power_line() {
        using namespace boost::python;
        using PowerLine = shyft::energy_market::market::power_line;
        class_<PowerLine, bases<>, std::shared_ptr<PowerLine>, boost::noncopyable>(
            "PowerLine",
            doc_intro("The PowerLine class describes the LTM (persisted) power-line")
            doc_intro("A power-line represents the transmission capacity between two model-areas.")
            doc_intro("Use the ModelArea.create_power_line(a1,a2,id) to build a power line")
            doc_see_also("Model,ModelArea,PowerModule,HydroPowerSystem"),
            py::no_init
            )
            .def(init<std::shared_ptr<shyft::energy_market::market::model>const&,std::shared_ptr<shyft::energy_market::market::model_area>&, std::shared_ptr<shyft::energy_market::market::model_area>&, int,const string&,py::optional<const string&>>(
                (py::arg("model"),py::arg("area_1"),py::arg("area_2"),py::arg("id"),py::arg("name"),py::arg("json")),
                doc_intro("constructs a PowerLine object between area 1 and 2 with the specified id")
                doc_parameters()
                doc_parameter("model","Model","the model for the power-line")
                doc_parameter("area_1", "ModelArea", "a reference to an existing area in the model")
                doc_parameter("area_2", "ModelArea", "a reference to an existing area in the model")
                doc_parameter("id", "int", "a global unique identifier for the power-line")
                doc_parameter("name", "string", "a global unique name for the power-line")
                doc_parameter("json", "string", "extra json for the power-line")
                )
            )
            .def_readwrite("id", &PowerLine::id, doc_intro("The unique id of the power-line"))
            .add_property("name", &PowerLine::name, doc_intro("The name of the power-line"))
            .def_readwrite("json",&PowerLine::json,doc_intro("json for the power-line"))
            .add_property("model",&PowerLine::get_model,doc_intro("the model for this power-line"))
            .add_property("area_1",&PowerLine::get_area_1,&PowerLine::set_area_1,doc_intro("reference to area-from"))
            .add_property("area_2", &PowerLine::get_area_2, &PowerLine::set_area_2, doc_intro("reference to area-to"))
            .add_property("obj", &py_object_ext<PowerLine>::get_obj, &py_object_ext<PowerLine>::set_obj, "a python object")
            .def(self == self)
            .def(self != self)
            ;
        typedef std::vector<std::shared_ptr<PowerLine>> PowerLineList;
        class_<PowerLineList>("PowerLineList",
            doc_intro("A dict of ModelArea, the key-value is the area-name"))
            .def(vector_indexing_suite<PowerLineList, true>()) // true since the shared_ptr is already a proxy
            .def(init<const PowerLineList&>(args("clone_me")))
            ;

    }

    
    void power_module() {
        using namespace boost::python;
        using PowerModule=shyft::energy_market::market::power_module;
        
        class_<PowerModule, bases<>, std::shared_ptr<PowerModule>, boost::noncopyable>(
            "PowerModule",
            doc_intro("The PowerModule class describes the LTM (persisted) power-module")
            doc_intro("A power-module represents an actor that consume/produces power for given price/volume")
            doc_intro("characteristics. The user can influence this characteristics giving")
            doc_intro("specific semantic load_type/power_type and extra data and/or relations to")
            doc_intro("other power-modules within the same area.")
            doc_see_also("Model,ModelArea,PowerLine,HydroPowerSystem")
            )
            .def(init<std::shared_ptr<shyft::energy_market::market::model_area>const&,int,string,py::optional<string>>(
                (py::arg("area"),py::arg("id"),py::arg("name"),py::arg("json")),
                doc_intro("constructs a PowerModule with specified mandatory name and module-id")
                doc_parameters()
                doc_parameter("area","ModelArea","the area for this power-module")
                doc_parameter("id","int","unique pm-id for area")
                doc_parameter("name", "string", "the name of the power-module")
                doc_parameter("json","string","optional json ")
                )
            )
            .add_property("area",&PowerModule::get_area,"the model-area for this power-module")
            .def_readwrite("id", &PowerModule::id, doc_intro("The unique id of the module"))
            .def_readwrite("name", &PowerModule::name, doc_intro("The name of the module"))
            .def_readwrite("json", &PowerModule::json)
            .def(self == self)
            .def(self != self)
            .add_property("obj", &py_object_ext<PowerModule>::get_obj, &py_object_ext<PowerModule>::set_obj, "a python object")
            ;

        typedef std::map<int,std::shared_ptr<PowerModule>> PowerModuleDict;
        class_<PowerModuleDict>("PowerModuleDict",
            doc_intro("A dict of PowerModule, the key-value is the power module id"))
            .def(map_indexing_suite<PowerModuleDict, true>()) // true since the shared_ptr is already a proxy
            .def(init<const PowerModuleDict&>(args("clone_me")))
            ;

    }
    
    void py_destroy(void *o){
        auto pyo = static_cast<py::object*>(o);
        if(pyo){
            delete pyo;   
        }
    };
    
    void register_py_destroy(){shyft::energy_market::em_handle::destroy = py_destroy;}

    extern void vectors();
    extern void all_of_hydro_power_system();
    extern void all_time_series_support();
    extern void calendar_and_time();
    extern void ex_client_server();
}

BOOST_PYTHON_MODULE(_core) {
    boost::python::scope().attr("__doc__") = "Shyft Open Source Energy Market model core";
    boost::python::docstring_options doc_options(true, true, false);// all except c++ signatures
    expose::model_area();
    expose::model();
    expose::int_string_dict();
    expose::power_line();
    expose::power_module();
    expose::xy_point_curves_etc();
    expose::all_of_hydro_power_system();
    expose::all_time_series_support();
    expose::point_vector_from_x_y();
    expose::ex_client_server();
    expose::register_py_destroy();
}
