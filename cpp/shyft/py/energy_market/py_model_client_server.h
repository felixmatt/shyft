#pragma once

#include <mutex>
#include <string>
#include <memory>
#include <vector>
#include <shyft/py/energy_market/api_utils.h>
#include <shyft/energy_market/srv/model_info.h>
#include <shyft/energy_market/srv/client.h>
#include <shyft/energy_market//srv/server.h>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>


namespace shyft::py::energy_market {

    using std::mutex;
    using std::unique_lock;
    using std::string;
    using std::vector;
    using std::shared_ptr;
    using shyft::energy_market::srv::model_info;
    using shyft::energy_market::srv::client;
    using shyft::energy_market::srv::server;
    namespace py=boost::python;


    /** This class do a scoped gil-release when declared 
    * 
    * - useful for typical c++  io ops that takes time.
    */
    struct scoped_gil_release {
        scoped_gil_release() noexcept {
            py_thread_state = PyEval_SaveThread();
        }
        ~scoped_gil_release() noexcept {
            PyEval_RestoreThread(py_thread_state);
        }
        scoped_gil_release(const scoped_gil_release&) = delete;
        scoped_gil_release(scoped_gil_release&&) = delete;
        scoped_gil_release& operator=(const scoped_gil_release&) = delete;
    private:
        PyThreadState * py_thread_state;
    };

    /** This class do a scoped gil aquire, 
    *
    * - useful when doing callbacks from c++ threads to python
    */
    struct scoped_gil_aquire {
        scoped_gil_aquire() noexcept {
            py_state = PyGILState_Ensure();
        }
        ~scoped_gil_aquire() noexcept {
            PyGILState_Release(py_state);
        }
        scoped_gil_aquire(const scoped_gil_aquire&) = delete;
        scoped_gil_aquire(scoped_gil_aquire&&) = delete;
        scoped_gil_aquire& operator=(const scoped_gil_aquire&) = delete;
    private:
        PyGILState_STATE   py_state;
    };

    /** @brief A  client for model type M suitable for python exposure
     *
     * This class takes care of  python gil and mutex, ensuring that any attempt using
     * multiple python threads will be serialized.
     * gil is released while the call is in progress.
     * 
     * Using this template saves us the repeating work for similar model-repositories
     * 
     * @tparam M a model, same requirements as for shyft::energy_market::srv::client<M> 
     * 
     */
    template<class M>
    struct py_client {
        using client_t=client<M>;
        mutex mx; ///< to enforce just one thread active on this client object at a time
        client_t impl;
        py_client(const std::string& host_port,int timeout_ms):impl{host_port,timeout_ms} {}
        ~py_client() { }

        py_client()=delete;
        py_client(py_client const&) = delete;
        py_client(py_client &&) = delete;
        py_client& operator=(py_client const&o) = delete;

        void close(int timeout_ms=1000) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            impl.close(timeout_ms);
        }
        
        void reopen(int timeout_ms=1000) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            impl.reopen(timeout_ms);
        }
        
        vector<model_info> get_model_infos(vector<int64_t>const & mids) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.get_model_infos(mids);
        }
        
        int64_t store_model( shared_ptr<M> const& m, model_info const &mi) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.store_model(m,mi);
        }
        
        shared_ptr<M> read_model(int64_t mid)  {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.read_model(mid);
        }
        
        vector<shared_ptr<M>> read_models(vector<int64_t> mids) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.read_models(mids);
        }
        
        int64_t remove_model(int64_t mid) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.remove_model(mid);
        }

        bool update_model_info(int64_t mid, model_info const &mi) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.update_model_info(mid,mi);
        }
        void close_conn() {//weird, close is not a name we can use here..
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            impl.close();
        }
    };

    /** @brief The server side component for a model repository
     *
     * 
     * This class wraps/provides the server-side for the model-repository of type DB<M>
     * suitable for exposure to python.
     * @tparam DB the model, same requirements as for the shyft::energy_market::srv::server class,M=DB::model_t
     * 
     */
    template<class DB>
    struct py_server  {
         server<DB> impl;
        py_server(const string& root_dir):impl(root_dir) {
            if (!PyEval_ThreadsInitialized()) {
                PyEval_InitThreads();// ensure threads-is enabled
            }
        }
        
        void set_listening_port(int port) {impl.set_listening_port(port);}
        int  get_listening_port() { return impl.get_listening_port();}
        
        void set_max_connections(int  n) {impl.set_max_connections(size_t(n));}
        int  get_max_connections() { return int(impl.get_max_connections());}
        
        void set_listening_ip(string const& ip) {impl.set_listening_ip(ip);}
        string  get_listening_ip() {return impl.get_listening_ip();}
        
        int start_server() {return impl.start_server(); }
        void stop_server(int timeout_ms) { 
            impl.set_graceful_close_timeout(timeout_ms);
            impl.clear(); 
        }
        
        bool is_running() { return impl.is_running(); }
        
        ~py_server() { }
        //-- get rid of stuff that would not work
         py_server()=delete;
         py_server(py_server const&)=delete;
         py_server(py_server&&)=delete;
         py_server& operator=(py_server const &)=delete;
         py_server& operator=(py_server&&)=delete;
    };

    /** @brief Expose to python the client side api for a model-repository of type M
     * 
     * @tparam M the model type, same requirements as for shyft::energy_market::srv::client
     */
    template <class M>
    void expose_client(const char* name,const char *doc_str) {
        using cm=py_client<M>;
        
        py::class_<cm,boost::noncopyable>(name,doc_str,py::no_init)
           .def(
                py::init<string const&,int>(
                    (py::arg("self"),py::arg("host_port"),py::arg("timeout_ms")),
                    "TODO"
                )
            )

            .def("get_model_infos",&cm::get_model_infos,
                 (py::arg("self"),py::arg("mids")),
                doc_intro("returns all or selected model-info objects based on model-identifiers(mids)")
                doc_parameters()
                doc_parameter("mids","IntVector","empty = all, or a list of known exisiting model-identifiers")
                doc_returns("model_infos","ModelInfoVector","Strongly typed list of ModelInfo")
            )
            
            .def("store_model",&cm::store_model,
                 (py::arg("self"),py::arg("m"),py::arg("mi")),
                doc_intro("Store the model to backend, if m.id ==0 then a new unique model-info is created and used")
                doc_parameters()
                doc_parameter("m","Model","The model to store")
                doc_parameter("mi","ModelInfo","The model-info to store for the model")
                doc_returns("mid","int","model-identifier for the stored  model and model-info ")
            )

            .def("read_model",&cm::read_model,
                 (py::arg("self"),py::arg("mid")),
                doc_intro("Read and return the model for specified model-identifier (mid)")
                doc_parameters()
                doc_parameter("mid","int","the model-identifer for the wanted model")
                doc_returns("m","Model","The resulting model from the server")
            )
            .def("read_models",&cm::read_models,
                 (py::arg("self"),py::arg("mids")),
                doc_intro("Read and return the model for specified model-identifier (mid)")
                doc_parameters()
                doc_parameter("mids","Int64Vector","A strongly typed list of ints, the model-identifers for the wanted models")
                doc_returns("m","Model","The resulting model from the server")
            )
          
            .def("remove_model",&cm::remove_model,
                 (py::arg("self"),py::arg("mid")),
                doc_intro("Remove the specified model bymodel-identifier (mid)")
                doc_parameters()
                doc_parameter("mid","int","the model-identifer for the wanted model")
                doc_returns("ec","int","0 or error-code?")
            )
            .def("update_model_info",&cm::update_model_info,
                 (py::arg("self"),py::arg("mid"),py::arg("mi")),
                doc_intro("Update the model-info for specified model-identifier(mid)")
                doc_parameters()
                doc_parameter("mid","int","model-identifer")
                doc_parameter("mi","ModelInfo","The new updated model-info")
                doc_returns("ok","bool","true if success")
            )
            .def("close",&cm::close_conn,
                (py::arg("self")),
                doc_intro("Close the connection, it will auto-open if ever needed")
            )
        ;
        
    }
    
    /** @brief a function to expose a server using a DB for model DB::model_t
     * 
     * 
     * This class simply expose the methods of the above py_server that takes
     * a backend storage service 'DB', for  model type M=DB::model_t
     * @tparam DB template for backend-store, same requirements as for py_server<M>
     * 
     */
    template<class DB>
    void expose_server(const char *name, const char *doc_str) {
        using srv=shyft::py::energy_market::py_server<DB>;
            py::class_<srv, boost::noncopyable >(name,doc_str,
                py::init<const string&>((py::arg("self"),py::arg("root_dir")),
                        doc_intro("Creates a server object that serves models from root_dir.")
                        doc_intro("The root_dir will be create if it does not exsists.")
                        doc_parameters()
                        doc_parameter("root_dir","str","Path to the root-directory that keeps/will keep the model-files")
                )
            )
            .def("set_listening_port", &srv::set_listening_port, (py::arg("self"),py::arg("port_no")),
                doc_intro("set the listening port for the service")
                doc_parameters()
                doc_parameter("port_no","int","a valid and available tcp-ip port number to listen on.")
                doc_paramcont("typically it could be 20000 (avoid using official reserved numbers)")
                doc_returns("nothing","None","")
            )
            .def("set_listening_ip", &srv::set_listening_ip, (py::arg("self"),py::arg("ip")),
                doc_intro("set the listening port for the service")
                doc_parameters()
                doc_parameter("ip","str","ip or host-name to start listening on")
                doc_returns("nothing","None","")
            )
            .def("start_server",&srv::start_server,(py::arg("self")),
                doc_intro("start server listening in background, and processing messages")
                doc_see_also("set_listening_port(port_no),is_running")
                doc_returns("port_no","in","the port used for listening operations, either the value as by set_listening_port, or if it was unspecified, a new available port")
            )
            .def("set_max_connections",&srv::set_max_connections,(py::arg("self"),py::arg("max_connect")),
                doc_intro("limits simultaneous connections to the server (it's multithreaded, and uses on thread pr. connect)")
                doc_parameters()
                doc_parameter("max_connect","int","maximum number of connections before denying more connections")
                doc_see_also("get_max_connections()")
            )
            .def("get_max_connections",&srv::get_max_connections, (py::arg("self")),
                doc_intro("returns the maximum number of connections to be served concurrently"))
            .def("stop_server",&srv::stop_server, (py::arg("self")),
                doc_intro("stop serving connections, gracefully.")
                doc_see_also("start_server()")
            )
            .def("is_running",&srv::is_running, (py::arg("self")),
                doc_intro("true if server is listening and running")
                doc_see_also("start_server()")
            )
            .def("get_listening_port",&srv::get_listening_port, (py::arg("self")),
                "returns the port number it's listening at for serving incoming request"
            )
            ;
    }
  
    
}
