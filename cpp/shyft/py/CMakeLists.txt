

#
# define the shyft core api that exposes all common classes and methods
#
    set(core_api  "api")
    set(core_api_sources 
        hydrology/api_actual_evapotranspiration.cpp     hydrology/api_kirchner.cpp
        hydrology/api_cell_environment.cpp              hydrology/api_precipitation_correction.cpp
        hydrology/api_priestley_taylor.cpp              hydrology/api_gamma_snow.cpp                        
        hydrology/api_pt_gs_k.cpp                       hydrology/api_geo_cell_data.cpp                         
        hydrology/api_region_environment.cpp            hydrology/api_r_pm_gs_k.cpp
        hydrology/api_geo_point.cpp                     hydrology/api_routing.cpp
        hydrology/api_glacier_melt.cpp                  hydrology/api.cpp
        hydrology/api_skaugen.cpp                       hydrology/api_hbv_actual_evapotranspiration.cpp  
        hydrology/api_state.cpp                         hydrology/api_kalman.cpp
        hydrology/api_hbv_physical_snow.cpp             hydrology/api_target_specification.cpp
        hydrology/api_hbv_snow.cpp                      hydrology/api_hbv_soil.cpp                                 
        hydrology/api_hbv_tank.cpp                      hydrology/api_penman_monteith.cpp      
        hydrology/api_interpolation.cpp                 hydrology/api_hydrology_vectors.cpp
        hydrology/api_radiation.cpp                           
     )

    add_library(${core_api} SHARED ${core_api_sources} )
    target_include_directories(${core_api} PRIVATE  ${include_python} ${include_numpy})
    set_target_properties(${core_api} PROPERTIES 
        OUTPUT_NAME ${core_api}
        PREFIX "_" 
        INSTALL_RPATH "$ORIGIN/../lib"
    ) 
    if(MSVC)
        set_target_properties(${core_api} PROPERTIES SUFFIX ".pyd") # Python extension use .pyd instead of .dll on Windows
    endif()
    set_property(TARGET ${core_api} APPEND PROPERTY COMPILE_DEFINITIONS SHYFT_EXTENSION)
    target_link_libraries(${core_api} shyft_core shyft_api dlib::dlib ${boost_py_link_libraries} ${arma_libs})
    install(TARGETS ${core_api} DESTINATION ${CMAKE_SOURCE_DIR}/shyft/api)

    set(ts_sources
        time_series/api_ts.cpp
        time_series/api_time_series.cpp
        time_series/api_dtss.cpp  
        time_series/api_time_axis.cpp
        time_series/api_utctime.cpp
        time_series/api_vectors.cpp
    )

    add_library(time_series SHARED ${ts_sources} )
    target_include_directories(time_series PRIVATE  ${include_python} ${include_numpy})
    set_target_properties(time_series PROPERTIES 
        OUTPUT_NAME time_series
        PREFIX "_" 
        INSTALL_RPATH "$ORIGIN/../lib"
    )
    if(MSVC)
        set_target_properties(time_series PROPERTIES SUFFIX ".pyd") # Python extension use .pyd instead of .dll on Windows
    endif()
    set_property(TARGET time_series APPEND PROPERTY COMPILE_DEFINITIONS SHYFT_EXTENSION)
    target_link_libraries(time_series shyft_core shyft_api dlib::dlib ${boost_py_link_libraries} ${arma_libs})
    install(TARGETS time_series DESTINATION  ${CMAKE_SOURCE_DIR}/shyft/time_series)

#
# define each shyft method-stack that exposes complete stacks
#
    foreach(shyft_stack  "r_pm_gs_k" "pt_gs_k" "pt_ss_k" "pt_hs_k" "hbv_stack" "pt_hps_k")
        add_library(${shyft_stack} SHARED  hydrology/${shyft_stack}.cpp)
        target_include_directories(${shyft_stack} PRIVATE  ${include_python} ${include_numpy} )
        set_target_properties(${shyft_stack} PROPERTIES 
            OUTPUT_NAME ${shyft_stack}
            PREFIX "_" 
            INSTALL_RPATH "$ORIGIN/../../lib"
            )
        if(MSVC)
            set_target_properties(${shyft_stack} PROPERTIES SUFFIX ".pyd") # Python extension use .pyd instead of .dll on Windows
        endif()
        set_property(TARGET ${shyft_stack} APPEND PROPERTY COMPILE_DEFINITIONS SHYFT_EXTENSION)
        target_link_libraries(${shyft_stack} shyft_core shyft_api  dlib::dlib ${boost_py_link_libraries} ${arma_libs} )
        install(TARGETS ${shyft_stack} DESTINATION ${CMAKE_SOURCE_DIR}/shyft/api/${shyft_stack})
    endforeach(shyft_stack)

#
# install 3rd party .so files ( but we still require blas/lapack + gcc 7.x runtime)
# we place them in the lib directory parallell to the shyft directory, and notice the INSTALL_RPATH
# for the targets do refer to this location. If you change any of these, just keep in sync 
#  (if not the you will get reminded when runtime fail to load referenced shared libraries)
#
    if (MSVC)
        # boost: some logic to get to the name of the .dll from the variable pointing to .lib
        get_filename_component(b_system_relname ${Boost_SYSTEM_LIBRARY_RELEASE} NAME_WE)
        get_filename_component(b_filesystem_relname ${Boost_FILESYSTEM_LIBRARY_RELEASE} NAME_WE)
        get_filename_component(b_serialization_relname ${Boost_SERIALIZATION_LIBRARY_RELEASE} NAME_WE)
        get_filename_component(b_python3_relname ${Boost_PYTHON${BOOST_PYTHON_VERSION}_LIBRARY_RELEASE} NAME_WE)
        install(FILES
            ${Boost_LIBRARY_DIR_RELEASE}/${b_system_relname}${CMAKE_SHARED_LIBRARY_SUFFIX}
            ${Boost_LIBRARY_DIR_RELEASE}/${b_filesystem_relname}${CMAKE_SHARED_LIBRARY_SUFFIX}
            ${Boost_LIBRARY_DIR_RELEASE}/${b_python3_relname}${CMAKE_SHARED_LIBRARY_SUFFIX}
            ${Boost_LIBRARY_DIR_RELEASE}/${b_serialization_relname}${CMAKE_SHARED_LIBRARY_SUFFIX}
            DESTINATION
                ${CMAKE_SOURCE_DIR}/shyft/lib
            CONFIGURATIONS Release MinSizeRel RelWithDebInfo
        )
        get_filename_component(b_system_dbgname ${Boost_SYSTEM_LIBRARY_DEBUG} NAME_WE)
        get_filename_component(b_filesystem_dbgname ${Boost_FILESYSTEM_LIBRARY_DEBUG} NAME_WE)
        get_filename_component(b_serialization_dbgname ${Boost_SERIALIZATION_LIBRARY_DEBUG} NAME_WE)
        get_filename_component(b_python3_dbgname ${Boost_PYTHON${BOOST_PYTHON_VERSION}_LIBRARY_DEBUG} NAME_WE)
        install(FILES
            ${Boost_LIBRARY_DIR_DEBUG}/${b_system_dbgname}${CMAKE_SHARED_LIBRARY_SUFFIX}
            ${Boost_LIBRARY_DIR_DEBUG}/${b_filesystem_dbgname}${CMAKE_SHARED_LIBRARY_SUFFIX}
            ${Boost_LIBRARY_DIR_DEBUG}/${b_python3_dbgname}${CMAKE_SHARED_LIBRARY_SUFFIX}
            ${Boost_LIBRARY_DIR_DEBUG}/${b_serialization_dbgname}${CMAKE_SHARED_LIBRARY_SUFFIX}
            DESTINATION
                ${CMAKE_SOURCE_DIR}/shyft/lib
            CONFIGURATIONS Debug
        )
        # openssl
        file(GLOB ssl LIST_DIRECTORIES false "${OPENSSL_INCLUDE_DIR}/../*.dll")
        install(FILES ${ssl}
            DESTINATION
                ${CMAKE_SOURCE_DIR}/shyft/lib
        )
        # skipping dlib and armadillo, assumed to be statically linked, but blas/lapack needs a copy
        string(REPLACE ".lib" ".dll" math_dlls "${math_libs}")
        install(FILES ${math_dlls}
            DESTINATION
                ${CMAKE_SOURCE_DIR}/shyft/lib
        )
            
    else()
        # step 1: pull out abs-paths from imported targets (release builds)
        get_property(dlib_lib TARGET dlib::dlib PROPERTY IMPORTED_LOCATION_RELEASE)
        # not yet supported on cmake 3.9.x:
        #get_target_property(arma_lib armadillo IMPORTED_LOCATION_NOCONFIG)
        # step 2: get the real-name (we want libboost_system.so.1.66.0, not the symlink libboost_system.so)
        get_filename_component(a_lib ${ARMADILLO_LIBRARIES} REALPATH CACHE)
        get_filename_component(d_lib ${dlib_lib} REALPATH CACHE)
        get_filename_component(s_lib ${OPENSSL_SSL_LIBRARY} REALPATH CACHE)
        get_filename_component(c_lib ${OPENSSL_CRYPTO_LIBRARY} REALPATH CACHE)
        string(REGEX REPLACE ".[1-9]00.[1-9]" "" a_lib_major ${a_lib}) # fixup for armadillo that references major version
        # libssl needs to be bundled, and.. realpath gives .10 on RHEL, not .10 that is referenced by .so
        string(REGEX REPLACE ".[1-9].[0-9].[0-9][a-z]" ".10" ss_lib ${s_lib})
        string(REGEX REPLACE ".[1-9].[0-9].[0-9][a-z]" ".10" cc_lib ${c_lib})
        get_filename_component(b_filesystem ${Boost_FILESYSTEM_LIBRARY_RELEASE} REALPATH CACHE)
        get_filename_component(b_system ${Boost_SYSTEM_LIBRARY_RELEASE} REALPATH CACHE)
        get_filename_component(b_serialization ${Boost_SERIALIZATION_LIBRARY_RELEASE} REALPATH CACHE)
        get_filename_component(b_python3 ${Boost_PYTHON${BOOST_PYTHON_VERSION}_LIBRARY_RELEASE} REALPATH CACHE)
        # step 3: install to the shyft/lib (that is referenced by the RPATH relative to the extensions libraries on linux)
        install(FILES 
            ${a_lib_major}
            ${a_lib}
            ${d_lib}
            ${b_system}
            ${b_filesystem}
            ${b_python3}
            ${b_serialization}
            ${ss_lib}
            ${cc_lib}
            ${s_lib}
            ${c_lib}
            DESTINATION ${CMAKE_SOURCE_DIR}/shyft/lib
            PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
        )
    endif()
