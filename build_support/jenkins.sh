#!/bin/bash

# This script will attempt to:
#  build shyft,
#  run tests,
#  generate coverage reports
#  make a conda package
#  upload the package using anaconda
#
# The script may be invoked by build robots such as Jenkins.

# Set any host-specific configuration
host=$(hostname)
case $host in
    'D40060-ubuntu')
	export WORKSPACE=/home/jenkins/workspace;
	export PATH=/home/u40420/projects/cmake/bin:$PATH
	;;
    'oslxpsht002p.energycorp.com')
	export WORKSPACE=/var/lib/jenkins/workspace;
	export PATH=/usr/local/bin:$PATH
	source scl_source enable devtoolset-8
	;;
    *) echo "Error, no configuration defined for this host (${host})";
       exit 1
       ;;
esac;


# Make sure the script exits if any command fails
set -e

# Set paths and activate conda environment
source $WORKSPACE/miniconda/etc/profile.d/conda.sh
conda activate shyft_env
export SHYFT_DEPENDENCIES_DIR=$WORKSPACE/shyft_dependencies
export SHYFT_DATA=$WORKSPACE/shyft-data
export PYTHONPATH=$WORKSPACE/shyft
export LD_LIBRARY_PATH=$WORKSPACE/shyft_dependencies/lib
# Build shyft
cd $WORKSPACE/shyft
mkdir -p build
cd build
cmake ..
make -j 12 CMAKE_VERBOSE_MAKEFILE=0

# Run C++ tests
make install
make test
# Run Python tests
cd ..
export SHYFT_SKIP_OPENDAP_TEST=1
pytest --cov-report html:py_cover --cov=shyft test_suites

# Convert coverage report to xml to be parsed by Cobertura plugin
coverage xml

# Build conda package
numpy_version=$(python -c "import numpy; print(numpy.version.short_version)")
shyft_minor=$(git rev-list --count HEAD)
export SHYFT_VERSION="4.6.${shyft_minor}"
conda build --numpy $numpy_version conda_recipe/all
filename=$(conda build --numpy $numpy_version --output conda_recipe/all)
anaconda upload --no-progress --force --user energycorp ${filename}

conda build --numpy $numpy_version conda_recipe/time_series
filename=$(conda build --numpy $numpy_version --output conda_recipe/time_series)
anaconda upload --no-progress --force --user energycorp ${filename}
