# Compute the installation prefix relative to this file.(is this really needed ?)
get_filename_component(_IMPORT_PREFIX "${CMAKE_CURRENT_LIST_FILE}" PATH)
get_filename_component(_IMPORT_PREFIX "${_IMPORT_PREFIX}" PATH)
get_filename_component(_IMPORT_PREFIX "${_IMPORT_PREFIX}" PATH)
get_filename_component(_IMPORT_PREFIX "${_IMPORT_PREFIX}" PATH)
if(_IMPORT_PREFIX STREQUAL "/")
    set(_IMPORT_PREFIX "")
endif()

set(shyft_core_INCLUDE_DIRS ${_IMPORT_PREFIX}/include)
if(MSVC)
    set(shyft_core_LIBRARIES
        debug ${_IMPORT_PREFIX}/lib/shyft_core_debug.lib optimized ${_IMPORT_PREFIX}/lib/shyft_core.lib
        debug ${_IMPORT_PREFIX}/lib/em_model_core_debug.lib optimized ${_IMPORT_PREFIX}/lib/em_model_core.lib
        debug ${_IMPORT_PREFIX}/lib/stm_core_debug.lib optimized ${_IMPORT_PREFIX}/lib/stm_core.lib
     )
else()
    set(shyft_core_LIBRARIES
        ${_IMPORT_PREFIX}/lib/libshyft_core.a
        ${_IMPORT_PREFIX}/lib/libem_model_core.a
        ${_IMPORT_PREFIX}/lib/libstm_core.a
    )
endif()

message(STATUS "Shyft core libraries:")
message(STATUS "  ${shyft_core_LIBRARIES}")
