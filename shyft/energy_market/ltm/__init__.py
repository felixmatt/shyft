from os import environ, name as os_name
from pathlib import Path

if os_name == 'nt':
    lib_path = str((Path(__file__).parent.parent/'lib').absolute())
    if lib_path not in environ['PATH']:
        environ['PATH'] = lib_path + ';' + environ['PATH']

from ..core import _core
from ._ltm import *
