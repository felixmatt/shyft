#
# step 1: include lib to path to ensure we have path to boost .dlls we are using
# (in dev-mode, this does not matter since we have path to those elsewhere, but in deployment, we try to be self-contained)
from shyft.time_series import DoubleVector,time

from os import path


# The absolute import here is PyCharm specific. Please consider removing this when PyCharm improves!
from ._core import *
from .model_repository import ModelRepository


def wtr_self_input_from(wtr, o, cr=None):
    if cr is None:
        wtr_input_from(wtr, o)
    else:
        wtr_input_from(wtr, o, cr)
    return wtr


def wtr_self_output_to(wtr, o, cr=None):
    if cr is None:
        wtr_output_to(wtr, o)
    else:
        wtr_output_to(wtr, o, cr)
    return wtr

#backward compatibility
Aggregate=Unit
AggregateList=UnitList
PowerStation=PowerPlant
PowerStationList=PowerPlantList
WaterRoute=Waterway
WaterRouteList=WaterwayList

Reservoir.input_from = lambda self, wtr: rsv_input_from(self, wtr)
Reservoir.output_to = lambda self, wtr, role: rsv_output_to(self, wtr, role)

Waterway.input_from = wtr_self_input_from
Waterway.output_to = wtr_self_output_to

HydroPowerSystem.create_river = lambda self, uid, name, json="": HydroPowerSystemBuilder(self).create_river(uid, name, json)
HydroPowerSystem.create_tunnel = lambda self, uid, name, json="": HydroPowerSystemBuilder(self).create_tunnel(uid, name, json)
HydroPowerSystem.create_aggregate = lambda self, uid, name, json="": HydroPowerSystemBuilder(self).create_unit(uid, name, json)
HydroPowerSystem.create_unit = lambda self, uid, name, json="": HydroPowerSystemBuilder(self).create_unit(uid, name, json)

HydroPowerSystem.create_power_station = lambda self, uid, name, json="": HydroPowerSystemBuilder(self).create_power_plant(uid, name, json)
HydroPowerSystem.create_power_plant = lambda self, uid, name, json="": HydroPowerSystemBuilder(self).create_power_plant(uid, name, json)
HydroPowerSystem.create_reservoir = lambda self, uid, name, json="": HydroPowerSystemBuilder(self).create_reservoir(uid, name, json)
HydroPowerSystem.create_catchment = lambda self, uid, name, json="": HydroPowerSystemBuilder(self).create_catchment(uid, name, json)
HydroPowerSystem.to_blob = lambda self: HydroPowerSystem.to_blob_ref(self)

# fixup building Model, ModelArea
Model.create_model_area = lambda self, uid, name, json="": ModelBuilder(self).create_model_area(uid, name, json)
Model.create_power_module = lambda self, area, uid, name, json="": ModelBuilder(self).create_power_module( uid, name, json, area)
Model.create_power_line = lambda self, a, b, uid, name, json="": ModelBuilder(self).create_power_line(uid, name, json, a, b)
ModelArea.create_power_module = lambda self, uid, name, json="": ModelBuilder(self.model).create_power_module(uid, name, json,self)


def create_model_service(model_directory, storage_type='blob'):
    """ Create and return the client for the Ltm model service
    Parameters
    ----------
    model_directory : string
        specifies the network host name, ip, name
    storage_type : string
        specifies type of api-service, ('blob')
        default = 'blob'

    """
    if storage_type == 'blob':
        if not path.exists(model_directory):
            raise RuntimeError("Model directory does not exists:'{0}'".format(model_directory))

        if not path.isdir(model_directory):
            raise RuntimeError("Specified model directory is not a directory:'{0}'".format(model_directory))

        return ModelRepository(model_directory)

    raise RuntimeError("unknown service storage type specified, please support 'db' or 'blob'")
