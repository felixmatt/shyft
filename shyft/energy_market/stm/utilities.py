from typing import List

from shyft.time_series import time
from shyft.energy_market.core import XyPointCurve, XyPointCurveWithZ, TurbineEfficiency, TurbineDescription
from shyft.energy_market.stm import t_double, t_xy, t_turbine_description


def create_t_double(t0: time, v: float) -> t_double:
    """ to ease construction """
    r = t_double()
    r[t0] = v
    return r


def create_t_xy(t0: time, point_curve: XyPointCurve) -> t_xy:
    """ to ease construction """
    r = t_xy()
    r[t0] = point_curve
    return r


def create_t_turbine_description(t0: time, efficiency_curves: List[XyPointCurveWithZ]) -> t_turbine_description:
    """ to ease construction """
    r = t_turbine_description()
    te = TurbineEfficiency(efficiency_curves)
    td = TurbineDescription([te])
    r[t0] = td
    return r


def create_t_turbine_description_pelton(t0: time,
                                        turbine_efficiencies: List[TurbineEfficiency]) -> t_turbine_description:
    """ to ease construction """
    r = t_turbine_description()
    td = TurbineDescription(turbine_efficiencies)
    r[t0] = td
    return r
