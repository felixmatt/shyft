import pytest
from os import path
from os import environ

from shyft.api import shyftdata_dir
from shyft.time_series import (Calendar, utctime_now, UtcPeriod, deltahours)
from shyft.repository.netcdf.gfs_data_repository import GFSDataRepository
from shapely.geometry import box


@property
def start_date():
    utc = Calendar()
    today = utc.trim(utctime_now(), Calendar.DAY)
    return today - Calendar.DAY*2  # yesterday


@property
def epsg_bbox():
    """ this should cut a slice out of test-data located in shyft-data repository/arome  """
    EPSG = 32632
    x0 = 436100.0  # Lower left
    y0 = 6823000.0  # Lower right
    nx = 74
    ny = 124
    dx = 1000.0
    dy = 1000.0
    return EPSG, ([x0, x0 + nx*dx, x0 + nx*dx, x0], [y0, y0, y0 + ny*dy, y0 + ny*dy]), box(x0, y0, x0 + dx*nx, y0 + dy*ny)


@pytest.mark.skipif("SHYFT_OPENDAP_TEST" not in environ, reason="gfs repository is not available from everywhere")
def test_open_dap_app():
    _get_timeseries()
    _get_forecast()
    _get_ensemble()


def _get_timeseries():
    """
    Simple regression test of OpenDAP data repository.
    """
    epsg, bbox, bpoly = epsg_bbox
    dem_file = path.join(shyftdata_dir, "netcdf", "etopo180.nc")
    n_hours = 30
    t0 = start_date + deltahours(7)
    period = UtcPeriod(t0, t0 + deltahours(n_hours))

    repos = GFSDataRepository(epsg=epsg, dem_file=dem_file, padding=5000.0, utc=t0)  # //epsg, dem_file, padding=5000., utc=None
    data_names = ("temperature", "wind_speed", "precipitation", "relative_humidity", "radiation")
    sources = repos.get_timeseries(data_names, period, geo_location_criteria=bpoly)
    assert set(data_names) == set(sources.keys())
    assert len(sources["temperature"]) == 2  # TODO: this was 6 before common changes.
    data1 = sources["temperature"][0]
    data2 = sources["temperature"][1]
    assert data1.mid_point().x != data2.mid_point().x
    assert data1.mid_point().y != data2.mid_point().y
    assert data1.mid_point().z != data2.mid_point().z
    assert data1.ts.time(0) <= period.start, 'expect returned fc ts to cover requested period'
    assert data1.ts.total_period().end >= period.end, 'expect returned fc ts to cover requested period'


def _get_forecast():
    """
    Simple forecast regression test of OpenDAP data repository.
    """
    epsg, bbox, bpoly = epsg_bbox

    dem_file = path.join(shyftdata_dir, "netcdf", "etopo180.nc")
    n_hours = 30
    t0 = start_date + deltahours(9)
    period = UtcPeriod(t0, t0 + deltahours(n_hours))
    t_c = start_date + deltahours(7)  # the beginning of the forecast criteria

    repos = GFSDataRepository(epsg, dem_file)
    data_names = ("temperature",)  # the full set: "wind_speed", "precipitation", "relative_humidity", "radiation")
    sources = repos.get_forecast(data_names, period, t_c, geo_location_criteria=bpoly)
    assert set(data_names) == set(sources.keys())
    assert len(sources["temperature"]) == 2
    data1 = sources["temperature"][0]
    data2 = sources["temperature"][1]
    assert data1.mid_point().x != data2.mid_point().x
    assert data1.mid_point().y != data2.mid_point().y
    assert data1.mid_point().z != data2.mid_point().z
    assert data1.ts.time(0) <= period.start, 'expect returned fc ts to cover requested period'
    assert data1.ts.total_period().end >= period.end, 'expect returned fc ts to cover requested period'


def _get_ensemble():
    """
    Simple ensemble regression test of OpenDAP data repository.
    """
    epsg, bbox, bpoly = epsg_bbox
    dem_file = path.join(shyftdata_dir, "netcdf", "etopo180.nc")
    n_hours = 30
    t0 = start_date + deltahours(9)  # api.YMDhms(year, month, day, hour)
    period = UtcPeriod(t0, t0 + deltahours(n_hours))
    t_c = t0

    repos = GFSDataRepository(epsg, dem_file)
    data_names = ("temperature",)  # this is the full set: "wind_speed", "precipitation", "relative_humidity", "radiation")
    ensembles = repos.get_forecast_ensemble(data_names, period, t_c, geo_location_criteria=bpoly)
    for sources in ensembles:
        assert set(data_names) == set(sources.keys())
        assert len(sources["temperature"]) == 2
        data1 = sources["temperature"][0]
        data2 = sources["temperature"][1]
        assert data1.mid_point().x != data2.mid_point().x
        assert data1.mid_point().y != data2.mid_point().y
        assert data1.mid_point().z != data2.mid_point().z
        assert data1.ts.time(0) <= period.start, 'expect returned fc ts to cover requested period'
        assert data1.ts.total_period().end >= period.end, 'expect returned fc ts to cover requested period'
