from shyft.time_series import deltahours
from shyft.api import River
from shyft.api import RoutingInfo
from shyft.api import UHGParameter
from shyft.api import RiverNetwork

"""Verify and illustrate the building of Routing classes
"""


def test_routing_info():
    ri = RoutingInfo(2, 1000.0)
    assert ri is not None
    assert ri.id == 2
    assert round(abs(ri.distance - 1000.0), 7) == 0
    ri.distance = 2000.0
    assert round(abs(ri.distance - 2000.0), 7) == 0


def test_unit_hydrograph_parameter():
    p = UHGParameter()
    assert p is not None
    assert round(abs(p.alpha - 7.0), 7) == 0  # default values
    assert round(abs(p.beta - 0.0), 7) == 0
    assert round(abs(p.velocity - 1.0), 7) == 0
    p.alpha = 2.7
    p.beta = 0.07
    p.velocity = 1/3600.0
    assert round(abs(p.alpha - 2.7), 7) == 0
    assert round(abs(p.beta - 0.07), 7) == 0
    assert round(abs(p.velocity - 1.0/3600.0), 7) == 0


def test_river():
    r1 = River(1)
    assert r1 is not None
    assert r1.id == 1
    r2 = River(2, RoutingInfo(3, 1000.0), UHGParameter(1/3600.0, 7.0, 0.0))
    assert r2.id == 2
    r3 = River(3, RoutingInfo(id=1, distance=36000.00))
    assert r3.id == 3
    assert r3.downstream.id == 1
    assert r3.downstream.distance == 36000.0

    r3_uhg = r3.uhg(deltahours(1))
    assert len(r3_uhg) == 10
    r2.parameter.alpha = 2.0
    r2.parameter.beta = 0.00
    r2.parameter.velocity = 1/3600.0
    assert round(abs(r2.parameter.alpha - 2.0), 7) == 0
    assert round(abs(r2.parameter.beta - 0.00), 7) == 0
    assert round(abs(r2.parameter.velocity - 1/3600.0), 7) == 0
    r2.downstream = RoutingInfo(2, 2000.0)
    assert r2.downstream.id == 2
    assert round(abs(r2.downstream.distance - 2000.0), 7) == 0
    # not possible, read only: r2.id = 3


def test_river_network():
    rn = RiverNetwork()
    assert rn is not None
    rn.add(River(1))
    rn.add(River(2))  # important detail, #2 must be added before referred
    rn.add(River(3, RoutingInfo(2, 1000.0), UHGParameter(1/3600.0, 7.0, 0.0)))
    rn.set_downstream_by_id(1, 2)
    # already done as pr. constuction above: rn.set_downstream_by_id(3,2)
    rn.add(River(4))
    rn.set_downstream_by_id(2, 4)
    assert len(rn.upstreams_by_id(4)) == 1
    assert len(rn.upstreams_by_id(2)) == 2
    assert len(rn.upstreams_by_id(1)) == 0
    assert rn.downstream_by_id(1) == 2
    assert rn.downstream_by_id(3) == 2
    up2 = rn.upstreams_by_id(2)
    assert 1 in up2
    assert 3 in up2
    rn.add(River(6))
    rn.set_downstream_by_id(6, 1)
    rn.remove_by_id(1)
    assert rn.downstream_by_id(6) == 0  # auto fix references ok
    rn_clone = RiverNetwork(rn)
    assert rn_clone is not None
    rn_clone.add(River(1, RoutingInfo(2, 1000.0)))
    assert len(rn_clone.upstreams_by_id(2)) == 2
    assert len(rn.upstreams_by_id(2)) == 1  # still just one in original netw.
    r3 = rn.river_by_id(3)
    r3.downstream.distance = 1234.0  # got a reference We can modify
    r3b = rn.river_by_id(3)  # pull out the reference once again
    assert round(abs(r3b.downstream.distance - 1234.0), 7) == 0  # verify its modified
