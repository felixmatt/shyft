import numpy as np
from shyft.time_series import (TsVector, TimeSeries, TimeAxis, point_interpretation_policy as ts_point_fx, deltahours)
from shyft.api import GeoPointVector
from shyft.api import GeoPoint
from shyft.api import TemperatureSourceVector, TemperatureSource
from shyft.api import PrecipitationSourceVector
from shyft.api import RelHumSourceVector
from shyft.api import RadiationSourceVector
from shyft.api import create_temperature_source_vector_from_np_array
from shyft.api import create_precipitation_source_vector_from_np_array
from shyft.api import create_rel_hum_source_vector_from_np_array
from shyft.api import create_radiation_source_vector_from_np_array


def test_create_xx_source_vector():
    # arrange the setup
    a = np.array([[1.1, 1.2, 1.3], [2.1, 2.2, 2.3]], dtype=np.float64)
    ta = TimeAxis(deltahours(0), deltahours(1), 3)
    gpv = GeoPointVector()
    gpv[:] = [GeoPoint(1, 2, 3), GeoPoint(4, 5, 6)]
    cfs = [(create_precipitation_source_vector_from_np_array, PrecipitationSourceVector),
           (create_temperature_source_vector_from_np_array, TemperatureSourceVector),
           (create_radiation_source_vector_from_np_array, RadiationSourceVector),
           (create_rel_hum_source_vector_from_np_array, RelHumSourceVector),
           (create_radiation_source_vector_from_np_array, RadiationSourceVector)]
    # test all creation types:
    for cf in cfs:
        r = cf[0](ta, gpv, a, ts_point_fx.POINT_AVERAGE_VALUE)  # act here
        assert isinstance(r, cf[1])  # then the asserts
        assert len(r) == len(gpv)
        for i in range(len(gpv)):
            assert r[i].mid_point() == gpv[i]
            assert np.allclose(r[i].ts.values.to_numpy(), a[i])
            assert r[i].ts.point_interpretation() == ts_point_fx.POINT_AVERAGE_VALUE


def test_create_xx_vector_from_list():
    """ verify we can construct from list of xxSource"""
    ts = TemperatureSource(
        GeoPoint(1.0, 2.0, 3.0),
        TimeSeries(TimeAxis(0, 3600, 10), fill_value=1.0, point_fx=ts_point_fx.POINT_AVERAGE_VALUE)
    )
    tsv = TemperatureSourceVector([ts])
    assert len(tsv) == 1
    assert len(TemperatureSourceVector([])) == 0
    assert len(TemperatureSourceVector([ts, ts])) == 2


def test_create_tsvector_from_ts_list():
    ts_list = [TimeSeries(TimeAxis(0, 3600, 10), fill_value=float(i), point_fx=ts_point_fx.POINT_AVERAGE_VALUE) for i in range(3)]
    tsv = TsVector(ts_list)
    assert tsv
    assert len(tsv) == 3
    assert tsv[0].value(0) == 0.0
    assert tsv[1].value(0) == 1.0
    assert tsv[2].value(0) == 2.0
