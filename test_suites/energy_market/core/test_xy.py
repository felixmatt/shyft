from shyft.energy_market.core import Point, PointList, XyPointCurve, XyPointCurveList, XyPointCurveWithZ, XyPointCurveWithZList, TurbineEfficiency, TurbineEfficiencyList, TurbineDescription


def verify_repr(a):
    """verify that recreating object from __repr__ works, assuming comparison operator can be trusted"""
    b = eval(repr(a))
    assert a == b


def test_repr():
    verify_repr(Point(20.0, 96.0))
    verify_repr([Point(20.0, 96.0), Point(40.0, 98.0), Point(60.0, 99.0), Point(80.0, 98.0)])
    verify_repr(PointList([Point(20.0, 96.0), Point(40.0, 98.0), Point(60.0, 99.0), Point(80.0, 98.0)]))
    verify_repr(XyPointCurve(PointList([Point(20.0, 96.0), Point(40.0, 98.0), Point(60.0, 99.0), Point(80.0, 98.0)])))
    verify_repr([XyPointCurve(PointList([Point(20.0, 96.0), Point(40.0, 98.0), Point(60.0, 99.0), Point(80.0, 98.0)])),XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0)]))])
    verify_repr(XyPointCurveList([XyPointCurve(PointList([Point(20.0, 96.0), Point(40.0, 98.0), Point(60.0, 99.0), Point(80.0, 98.0)])),XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0)]))]))
    verify_repr(XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0))
    verify_repr([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0),XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),72.0)])
    verify_repr(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0),XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),72.0)]))
    verify_repr(TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0),XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),72.0)])))
    verify_repr([TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0),XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),72.0)])),TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0),XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),72.0)]))])
    verify_repr(TurbineEfficiencyList([TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0),XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),72.0)])),TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0),XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),72.0)]))]))
    verify_repr(TurbineDescription(TurbineEfficiencyList([TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0),XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),72.0)])),TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0),XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),72.0)]))])))


def test_point():
    p1 = Point()
    assert p1.x == 0.0 and p1.y == 0.0
    p2 = Point(1.0, 2.0)
    assert p2.x == 1.0 and p2.y == 2.0
    assert p1 != p2
    p3 = Point(p2)
    assert p2 == p3
    p3.x = p3.x + 0.001
    assert p2 != p3


def test_point_list():
    p = PointList()
    assert len(p) == 0
    p = PointList([Point(0.0, 0.0), Point(1.0, 1.0)])
    assert len(p) == 2
    assert p[0] == Point()
    assert p[1] == Point(1.0, 1.0)
    p.append(Point(2.0, 2.0))
    assert len(p) == 3
    q = PointList(p)
    assert len(q) == 3
    assert q == p
    q.append(Point(4.0, 4.0))
    assert q != p


def test_xy_point_curve():
    a = XyPointCurve(PointList([Point(0.0, 0.0), Point(10.0, 10.0)]))
    assert len(a.points) == 2
    assert a.calculate_y(5.0) == 5.0
    assert a.calculate_x(1.0) == 1.0
    assert a.is_mono_increasing()


def test_xy_point_curve_with_z():
    a = XyPointCurveWithZ(XyPointCurve(PointList([Point(0.0, 0.0), Point(10.0, 10.0)])), z=1.0)
    assert a.z == 1.0
    assert len(a.xy_point_curve.points) == 2
    b = XyPointCurveWithZ(a)
    assert a == b
    a.z = 2.0
    assert a != b


def test_turbine_efficiency():
    """turbine efficiency, for a set of net heads"""
    a = TurbineEfficiency()
    assert len(a.efficiency_curves) == 0
    a.efficiency_curves.append(XyPointCurveWithZ(XyPointCurve(PointList([Point(10.0, 0.6), Point(15.0, 0.8), Point(20.0, 0.7)])), 100))
    assert len(a.efficiency_curves) == 1
    assert abs(a.efficiency_curves[0].z - 100.0) < 1e-8
    # append one more net head (z)
    a.efficiency_curves.append(XyPointCurveWithZ(XyPointCurve(PointList([Point(10.0, 0.6), Point(11.0, 0.7), Point(12.0, 0.65)])), 110))
    assert len(a.efficiency_curves) == 2
    assert abs(a.efficiency_curves[0].z - 100.0) < 1e-8
    assert abs(a.efficiency_curves[1].z - 110.0) < 1e-8


def test_turbine_description():
    """turbine description, with a list of turbine efficiency curves, one for each needle combination"""
    d = TurbineDescription()
    assert d
    assert len(d.efficiencies) == 0
    for z in range(0,6,1):
        a = TurbineEfficiency()
        a.efficiency_curves.append(XyPointCurveWithZ(XyPointCurve(PointList([Point(10.0, 0.6), Point(15.0, 0.8), Point(20.0, 0.7)])), 100))
        a.efficiency_curves.append(XyPointCurveWithZ(XyPointCurve(PointList([Point(10.0, 0.6), Point(11.0, 0.7), Point(12.0, 0.65)])), 110))
        a.efficiency_curves.append(XyPointCurveWithZ(XyPointCurve(PointList([Point(11.0, 0.6), Point(13.0, 0.8), Point(14.0, 0.65)])), 120))
        d.efficiencies.append(a)
    assert len(d.efficiencies) == 6