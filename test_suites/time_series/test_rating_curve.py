from shyft import time_series as sa


def test_rating_curve_segment():
    lower = 0.0
    a = 1.0
    b = 2.0
    c = 3.0
    rcs = sa.RatingCurveSegment(lower=lower, a=a, b=b, c=c)
    assert rcs.lower == lower
    assert round(abs(rcs.a - a), 7) == 0
    assert round(abs(rcs.b - b), 7) == 0
    assert round(abs(rcs.c - c), 7) == 0
    for level in range(10):
        assert round(abs(rcs.flow(level) - a*pow(level - b, c)), 7) == 0
    flows = rcs.flow([i for i in range(10)])
    for i in range(10):
        assert round(abs(flows[i] - a*pow(float(i) - b, c)), 7) == 0


def test_rating_curve_function():
    rcf = sa.RatingCurveFunction()
    assert rcf.size() == 0
    lower = 0.0
    a = 1.0
    b = 2.0
    c = 3.0
    rcs = sa.RatingCurveSegment(lower=lower, a=a, b=b, c=c)
    rcf.add_segment(rcs)
    rcf.add_segment(lower + 10.0, a, b, c)
    assert rcf.size() == 2
    assert round(abs(rcf.flow(4.0) - 8.0), 7) == 0
    assert round(abs(rcf.flow([4.0])[0] - 8.0), 7) == 0
    s = str(rcf)  # just to check that str works
    assert len(s) > 10
    sum_levels = 0.0
    for rcs in rcf:  # demo iterable
        sum_levels += rcs.lower
    assert round(abs(sum_levels - (lower + 10.0)), 7) == 0
