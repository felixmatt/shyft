﻿from shyft.time_series import (Calendar, time, utctime_now, UtcPeriod, deltahours, YMDhms, trim_policy, intersection)
import datetime as dt
import math

"""Verify and illustrate the Calendar & time from the api core, using
pyunit. Note that the Calendar not yet support local/dst semantics (but
plan to do so) Nevertheless, keeping it here allow users of api-Core to
use start practicing time/calendar perimeter.
"""

utc = Calendar()  # A utc calendar
std = Calendar(3600)  # UTC+01


def test_time_zone_region_id_list():
    assert len(Calendar.region_id_list()) > 1


def test_create_calendar_from_region_id():
    osl = Calendar("Europe/Oslo")
    assert osl is not None
    assert osl.tz_info.name() == "Europe/Oslo"
    assert osl.tz_info.base_offset() == 3600
    t = osl.time(2015, 6, 1)
    assert osl.tz_info.is_dst(t)
    assert osl.tz_info.utc_offset(t), 7200


def test_calendar_add_and_diff_units():
    osl = Calendar("Europe/Oslo")
    t0 = osl.time(2016, 6, 1, 12, 0, 0)
    t1 = osl.add(t0, Calendar.DAY, 7)
    t2 = osl.add(t1, Calendar.WEEK, -1)
    assert t0 == t2
    assert 7 == osl.diff_units(t0, t1, Calendar.DAY)
    assert 1 == osl.diff_units(t0, t1, Calendar.WEEK)
    assert 0 == osl.diff_units(t0, t1, Calendar.MONTH)
    assert 7*24 == osl.diff_units(t0, t1, deltahours(1))


def test_calendar_add_during_dst():
    osl = Calendar("Europe/Oslo")
    t0 = osl.time(2016, 3, 27)  # dst change during spring
    t1 = osl.add(t0, Calendar.DAY, 1)
    t2 = osl.add(t1, Calendar.DAY, -1)
    assert t0 == t2
    assert "2016-03-28T00:00:00+02" == osl.to_string(t1)
    assert 1 == osl.diff_units(t0, t1, Calendar.DAY)
    assert 23 == osl.diff_units(t0, t1, Calendar.HOUR)
    t0 = osl.time(2016, 10, 30)
    t1 = osl.add(t0, Calendar.WEEK, 1)
    t2 = osl.add(t1, Calendar.WEEK, -1)
    assert t0 == t2
    assert "2016-11-06T00:00:00+01" == osl.to_string(t1)
    assert 168 + 1 == osl.diff_units(t0, t1, Calendar.HOUR)


def test_calendar_add_3h_during_dst():
    osl = Calendar("Europe/Oslo")
    t0 = osl.time(2016, 3, 27)  # dst change during spring
    t1 = osl.add(t0, Calendar.DAY, 1)
    dt3h = deltahours(3)
    d3h = osl.diff_units(t0, t1, dt3h)
    assert 8 == d3h


def test_trim_day():
    t = utctime_now()
    td = std.trim(t, Calendar.DAY)
    c = std.calendar_units(td)
    a = std.calendar_units(t)
    assert c.second == 0, 'incorrect seconds should be 0'
    assert c.minute == 0, 'trim day should set minutes to 0'
    assert c.hour == 0, 'trim day should set hours to 0'
    assert a.year == c.year, 'trim day Should leave same year'
    assert a.month == c.month, 'trim day  Should leave month'
    assert a.day == c.day, 'should leave same day'


def test_quarter():
    t = std.time(2017, 2, 28, 1, 2, 3)
    tt = std.trim(t, Calendar.QUARTER)
    assert tt == std.time(2017, 1, 1)
    assert 1 == std.quarter(t)


def test_conversion_roundtrip():
    c1 = YMDhms(1960, 1, 2, 3, 4, 5)
    t1 = std.time(c1)
    c2 = std.calendar_units(t1)
    cw = std.calendar_week_units(t1)
    tw2 = std.time_from_week(1959, 53, 6, 3, 4, 5)
    tw1 = std.time(cw)
    assert tw2 == t1
    assert tw1 == t1
    assert cw.iso_year == 1959
    assert cw.iso_week == 53
    assert cw.week_day == 6
    assert cw.hour == 3
    assert cw.minute == 4
    assert cw.second == 5
    assert c1.year == c2.year, 'roundtrip should keep year'
    assert c1.month == c2.month
    assert c1.day == c2.day
    assert c1.hour == c2.hour
    assert c1.second == c2.second


def test_utctime_now():
    a = utctime_now()
    x = dt.datetime.utcnow()
    b = utc.time(x.year, x.month, x.day,
                 x.hour, x.minute, x.second)
    assert abs(a - b) < 2, 'Should be less than 2 seconds'


def test_utc_time_to_string():
    t = std.time(2000, 1, 2, 3, 4, 5, 6)
    s = std.to_string(t)
    assert s == "2000-01-02T03:04:05.000006+01"
    t = std.time(1960, 1, 2, 3, 4, 5, 6)
    s = std.to_string(t)
    assert s == "1960-01-02T03:04:05.000006+01"


def test_utcperiod_to_string():
    c1 = YMDhms(2000, 1, 2, 3, 4, 5)
    t = utc.time(c1)
    p = UtcPeriod(t, t + deltahours(1))
    s = p.to_string()
    assert s == "[2000-01-02T03:04:05Z,2000-01-02T04:04:05Z>"
    s2 = std.to_string(p)
    assert s2 == "[2000-01-02T04:04:05+01,2000-01-02T05:04:05+01>"


def test_utcperiod_str():
    c1 = YMDhms(2000, 1, 2, 3, 4, 5)
    t = utc.time(c1)
    p = UtcPeriod(t, t + deltahours(1))
    s = str(p)
    assert s == "[2000-01-02T03:04:05Z,2000-01-02T04:04:05Z>"
    s = repr(p)
    assert s == "[2000-01-02T03:04:05Z,2000-01-02T04:04:05Z>"


def test_utcperiod_methods():
    p0 = UtcPeriod()
    px = UtcPeriod(utc.time(2010), utc.time(2000))
    py = UtcPeriod(utc.time(2010), utc.time(2010))
    p1 = UtcPeriod(utc.time(2015), utc.time(2016))
    p2 = UtcPeriod(utc.time(2016), utc.time(2017))
    p3 = UtcPeriod(p1.start, p2.end)
    assert not p0.valid()
    assert not px.valid()
    assert py.valid()
    assert p1.contains(p1.start)
    assert not p1.contains(p1.end)
    assert not p1.overlaps(p2)
    assert not p2.overlaps(p1)
    assert p3.overlaps(p1)
    assert p3.overlaps(p2)


def test_utcperiod_trim():
    utc = Calendar()
    t0 = utc.time(2018, 1, 1, 0, 0, 0)
    t1 = utc.time(2018, 2, 1, 0, 0, 0)
    t2 = utc.time(2018, 3, 1, 0, 0, 0)
    t3 = utc.time(2018, 4, 1, 0, 0, 0)
    p_0_1 = UtcPeriod(t0, t1)
    p_0_3 = UtcPeriod(t0, t3)
    assert UtcPeriod(t0, t1).trim(utc, utc.MONTH, trim_policy.TRIM_IN) == p_0_1
    assert UtcPeriod(t0, t1).trim(utc, 3600*24*30, trim_policy.TRIM_IN) == p_0_1
    assert UtcPeriod(t0, t1 + 1).trim(utc, utc.MONTH) == p_0_1
    assert UtcPeriod(t1 - 1, t2 + 1).trim(utc, utc.MONTH, trim_policy.TRIM_OUT) == p_0_3

    # diff_units
    assert UtcPeriod(t0, t3).diff_units(utc, utc.MONTH) == 3
    # trim null period, should trow
    p_null = UtcPeriod()
    try:
        p_null.trim(utc, utc.DAY)
        did_raise = False
    except RuntimeError:
        did_raise = True
    assert did_raise

    assert p_null.diff_units(utc, utc.DAY) == 0
    assert UtcPeriod(t3, t0).diff_units(utc, utc.MONTH) == -3


def test_utcperiod_intersection():
    utc = Calendar()
    t0 = utc.time(2018, 1, 1, 0, 0, 0)
    t1 = utc.time(2018, 1, 2, 0, 0, 0)
    t2 = utc.time(2018, 1, 3, 0, 0, 0)
    t3 = utc.time(2018, 1, 4, 0, 0, 0)

    p0 = UtcPeriod(t0, t2)
    p1 = UtcPeriod(t1, t3)
    p2 = UtcPeriod(t0, t1)
    p3 = UtcPeriod(t2, t3)

    assert UtcPeriod.intersection(p0, p1) == UtcPeriod(t1, t2)
    assert not UtcPeriod.intersection(p0, p3).valid()
    assert not UtcPeriod.intersection(p2, p3).valid()
    assert intersection(p0, p1) == UtcPeriod(t1, t2)


def test_swig_python_time():
    """
    This particular test is here to point out a platform specific bug detected
    on windows.

    """
    c1 = YMDhms(1969, 12, 31, 23, 0, 0)
    t = utc.time(c1)  # at this point, the value returned from c++ is correct, but on its
    # way through swig layer to python it goes via int32 and then to int64, proper signhandling
    #
    t_str = utc.to_string(t)  # this one just to show it's still working as it should internally
    assert t_str == "1969-12-31T23:00:00Z"
    assert t == deltahours(-1)


def test_time_construct():
    assert (time().seconds - 0.0) == 0, 'default value should be 0.0'
    assert abs(time(3123456).seconds - 3123456) == 0, 'should be constructible from integer type'
    assert abs(time(3.123456).seconds - 3.123456) == 0, 'should be constructible from float type'
    assert abs(time('1970-01-01T00:00:23Z').seconds - 23.0) == 0, 'should be constructible from iso 8601 string'


def test_time_compare():
    assert time(123) == time(123)
    assert time(123) == 123
    assert time(123) != time(123.2)
    assert time(123) != 123.4
    assert time(123) <= time(123.2)
    assert time(123) <= time(123)
    assert time(1234) >= 134
    assert time(1234) >= 1234
    assert time(123) < time(123.2)
    assert time(1234) > 134


def test_time_math():
    t = time
    assert abs(t(2) + t(2) - 4) == 0
    assert abs(t(2) + 2 - 4) == 0
    assert abs(t(2) + 2.2 - 4.2) == 0

    assert abs(t(2) - t(2) - 0.0) == 0
    assert abs(t(2) - 2 - 0) == 0
    assert abs(t(2) - 2.2 - -0.2) == 0

    assert abs(t(2)*t(3) - 6.0) == 0
    assert abs(t(2)*0.1 - 0.2) == 0

    assert abs(t(2)/t(3) - 0.666667) == 0
    assert abs(t(2)/0.1 - 20.0) == 0

    assert abs(t(2)//t(3) - 0) == 0
    assert abs(t(10)//3 - 3) == 0

    assert abs(abs(t(-3)) - 3) == 0
    assert abs(abs(t(3.2)) - 3.2) == 0

    assert abs(t(10)%3 - 1.0) == 0


def test_time_floor():
    assert abs(math.floor(time(3.2)) - 3.0) == 0
    assert abs(math.floor(time(-3.2)) - -4.0) == 0


def test_time_round():
    assert abs(round(time(3.2)) - 3.0) == 0
    assert abs(round(time(-3.7)) - -4.0) == 0


def test_time_cast():
    assert abs(int(time(10.23)) - 10) == 0
    assert abs(float(time(10.23)) - 10.23) == 0
    assert abs(time(1.23).seconds - 1.23) == 0


def test_time_large_number():
    a = 1000.0*time(1534832966.984426)
    assert a is not None
    b = time(a)/1000.0
    assert b is not None
    sb = str(b)
    assert len(sb) > 0
    pass


def test_time_hash():
    t0 = time('2018-01-01T01:02:03Z')
    t1 = time('2018-01-01T01:02:03Z')
    h0 = hash(t0)
    h1 = hash(t1)
    assert h0 == h1
    d = {t0: 'A'}
    assert t0 in d
    assert t1 in d
