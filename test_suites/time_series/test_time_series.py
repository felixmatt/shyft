﻿from shyft.time_series import (DoubleVector, IntVector, Calendar, time, TimeSeries, TsVector,
                               POINT_INSTANT_VALUE, POINT_AVERAGE_VALUE,
                               TimeAxis, TimeAxisFixedDeltaT, TimeAxisByPoints,
                               TsPoint, TsFixed, TsFactory, ts_vector_values_at_time,
                               UtcTimeVector, Int64Vector, AverageAccessorTs,
                               deltahours, deltaminutes, utctime_now,
                               log, point_interpretation_policy,
                               statistics_property,
                               time_shift, integral,
                               create_periodic_pattern_ts,
                               kling_gupta, nash_sutcliffe,
                               ts_stringify, extend_split_policy,
                               RatingCurveSegment, RatingCurveFunction, RatingCurveParameters,
                               IcePackingRecessionParameters, IcePackingParameters, ice_packing_temperature_policy,
                               QacParameter, extend_fill_policy, min, max)


import numpy as np
import math
from numpy.testing import assert_array_almost_equal
import pytest


class TimeSeriesUtilCase:
    """Verify and illustrate TimeSeries

     a) point time-series:
        defined by a set of points,
        projection from point to f(t) (does the point represent state in time, or average of a period?)
        projection of f(t) to average/integral ts, like
        ts_avg_1=average_accessor(ts1,time_axis)

     """

    def __init__(self):
        self.c = Calendar()
        self.d = deltahours(1)
        self.n = 24
        self.t = self.c.trim(utctime_now(), self.d)
        self.ta = TimeAxisFixedDeltaT(self.t, self.d, self.n)


def test_operations_on_ts_fixed():
    u = TimeSeriesUtilCase()
    dv = np.arange(u.ta.size())
    v = DoubleVector.from_numpy(dv)
    # test create
    tsa = TsFixed(u.ta, v, POINT_INSTANT_VALUE)
    # assert its contains time and values as expected.
    assert u.ta.total_period() == tsa.total_period()
    for i in range(u.ta.size()):
        assert round(abs(tsa.value(i) - v[i]), 7) == 0

    for i in range(u.ta.size()):
        assert tsa.time(i) == u.ta(i).start
    for i in range(u.ta.size()):
        assert round(abs(tsa.get(i).v - v[i]), 7) == 0
    # set one value
    v[0] = 122
    tsa.set(0, v[0])
    assert round(abs(v[0] - tsa.value(0)), 7) == 0
    # test fill with values
    for i in range(len(v)): v[i] = 123
    tsa.fill(v[0])
    for i in range(u.ta.size()):
        assert round(abs(tsa.get(i).v - v[i]), 7) == 0


def test_vector_of_timeseries():
    u = TimeSeriesUtilCase()
    dv = np.arange(u.ta.size())
    v = DoubleVector.from_numpy(dv)
    tsf = TsFactory()
    tsa = tsf.create_point_ts(u.n, u.t, u.d, v)
    tsvector = TsVector()
    assert len(tsvector) == 0
    tsvector.push_back(tsa)
    assert len(tsvector) == 1
    tsvector.push_back(tsa)
    vv = tsvector.values_at_time(u.ta.time(3))  # verify it's easy to get out vectorized results at time t
    assert len(vv) == len(tsvector)
    assert round(abs(vv[0] - 3.0), 7) == 0
    assert round(abs(vv[1] - 3.0), 7) == 0
    ts_list = [tsa, tsa]
    vv = ts_vector_values_at_time(ts_list, u.ta.time(4))  # also check it work with list(TimeSeries)
    assert len(vv) == len(tsvector)
    assert round(abs(vv[0] - 4.0), 7) == 0
    assert round(abs(vv[1] - 4.0), 7) == 0


def test_ts_fixed():
    u = TimeSeriesUtilCase()
    u = TimeSeriesUtilCase()
    dv = np.arange(u.ta.size())
    v = DoubleVector.from_numpy(dv)
    xv = v.to_numpy()

    tsfixed = TsFixed(u.ta, v, POINT_AVERAGE_VALUE)
    assert tsfixed.size() == u.ta.size()
    assert round(abs(tsfixed.get(0).v - v[0]), 7) == 0
    vv = tsfixed.values.to_numpy()  # introduced .values for compatibility
    assert_array_almost_equal(dv, vv)
    tsfixed.values[0] = 10.0
    dv[0] = 10.0
    assert_array_almost_equal(dv, tsfixed.v.to_numpy())
    ts_ta = tsfixed.time_axis  # a TsFixed do have .time_axis and .values
    assert len(ts_ta) == len(u.ta)  # should have same length etc.

    # verify some simple core-ts to TimeSeries interoperability
    full_ts = tsfixed.TimeSeries  # returns a new TimeSeries as clone from tsfixed
    assert full_ts.size() == tsfixed.size()
    for i in range(tsfixed.size()):
        assert full_ts.time(i) == tsfixed.time(i)
        assert round(abs(full_ts.value(i) - tsfixed.value(i)), 5) == 0
    ns = tsfixed.nash_sutcliffe(full_ts)
    assert round(abs(ns - 1.0), 4) == 0
    kg = tsfixed.kling_gupta(full_ts, 1.0, 1.0, 1.0)
    assert round(abs(kg - 1.0), 4) == 0

    # u.assertAlmostEqual(v,vv)
    # some reference testing:
    ref_v = tsfixed.v
    del tsfixed
    assert_array_almost_equal(dv, ref_v.to_numpy())


def test_ts_point():
    u = TimeSeriesUtilCase()
    dv = np.arange(u.ta.size())
    v = DoubleVector.from_numpy(dv)
    t = UtcTimeVector()
    for i in range(u.ta.size()):
        t.push_back(u.ta(i).start)
    t.push_back(u.ta(u.ta.size() - 1).end)
    ta = TimeAxisByPoints(t)
    tspoint = TsPoint(ta, v, POINT_AVERAGE_VALUE)
    ts_ta = tspoint.time_axis  # a TsPoint do have .time_axis and .values
    assert len(ts_ta) == len(u.ta)  # should have same length etc.

    assert tspoint.size() == ta.size()
    assert round(abs(tspoint.get(0).v - v[0]), 7) == 0
    assert round(abs(tspoint.values[0] - v[0]), 7) == 0  # just to verfy compat .values works
    assert tspoint.get(0).t == ta(0).start
    # verify some simple core-ts to TimeSeries interoperability
    full_ts = tspoint.TimeSeries  # returns a new TimeSeries as clone from tsfixed
    assert full_ts.size() == tspoint.size()
    for i in range(tspoint.size()):
        assert full_ts.time(i) == tspoint.time(i)
        assert round(abs(full_ts.value(i) - tspoint.value(i)), 5) == 0
    ns = tspoint.nash_sutcliffe(full_ts)
    assert round(abs(ns - 1.0), 4) == 0
    kg = tspoint.kling_gupta(full_ts, 1.0, 1.0, 1.0)
    assert round(abs(kg - 1.0), 4) == 0


def test_ts_factory():
    u = TimeSeriesUtilCase()
    dv = np.arange(u.ta.size())
    v = DoubleVector.from_numpy(dv)
    t = UtcTimeVector()
    ti = Int64Vector()
    for i in range(u.ta.size()):
        t.push_back(u.ta(i).start)
        ti.append(u.ta(i).start.seconds)
    t.push_back(u.ta(u.ta.size() - 1).end)
    tsf = TsFactory()
    ts1 = tsf.create_point_ts(u.ta.size(), u.t, u.d, v)
    ts2 = tsf.create_time_point_ts(u.ta.total_period(), t, v)
    ts1i = tsf.create_point_ts(u.ta.size(), int(u.t.seconds), int(u.d.seconds), v, interpretation=POINT_AVERAGE_VALUE)
    ts2i = tsf.create_time_point_ts(u.ta.total_period(), times=UtcTimeVector(ti), values=v)  # TODO: remove requirement for UtcTimeVector, accept int's
    tslist = TsVector()
    tslist.push_back(ts1)
    tslist.push_back(ts2)
    assert tslist.size() == 2
    assert ts2 == ts2i
    assert not (ts2 != ts2i)


def test_average_accessor():
    u = TimeSeriesUtilCase()
    dv = np.arange(u.ta.size())
    v = DoubleVector.from_numpy(dv)
    t = UtcTimeVector()
    for i in range(u.ta.size()):
        t.push_back(u.ta(i).start)
    t.push_back(
        u.ta(u.ta.size() - 1).end)  # important! needs n+1 points to determine n periods in the timeaxis
    tsf = TsFactory()
    ts1 = tsf.create_point_ts(u.ta.size(), u.t, u.d, v)
    ts2 = tsf.create_time_point_ts(u.ta.total_period(), t, v)
    tax = TimeAxisFixedDeltaT(u.ta.total_period().start + deltaminutes(30), deltahours(1),
                              u.ta.size())
    avg1 = AverageAccessorTs(ts1, tax)
    assert avg1.size() == tax.size()
    assert ts2 is not None




def test_basic_timeseries_math_operations():
    u = TimeSeriesUtilCase()
    """
    Test that timeseries functionality is exposed, and briefly verify correctness
    of operators (the  shyft core do the rest of the test job, not repeated here).
    """
    t0 = utctime_now()
    dt = deltahours(1)
    n = 240
    ta = TimeAxis(t0, dt, n)

    a = TimeSeries(ta=ta, fill_value=3.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    assert a  # should evaluate to true
    b = TimeSeries(ta=ta, fill_value=1.0, point_fx=point_interpretation_policy.POINT_INSTANT_VALUE)
    b.fill(2.0)  # demo how to fill a point ts
    assert round(abs((1.0 - b).values.to_numpy().max() - -1.0), 7) == 0
    assert round(abs((b - 1.0).values.to_numpy().max() - 1.0), 7) == 0
    c = a + b*3.0 - a/2.0  # operator + * - /
    d = -a  # unary minus
    e = a.average(ta)  # average
    f = max(c, 300.0)
    g = min(c, -300.0)
    # h = a.max(c, 300) # class static method not supported
    h = c.max(300.0)
    k = c.min(-300)

    log_func = log(c)
    log_ts = c.log()

    assert a.size() == n
    assert b.size() == n
    assert c.size() == n
    assert log_func.size() == n
    assert log_ts.size() == n
    assert round(abs(c.value(0) - (3.0 + 2.0*3.0 - 3.0/2.0)), 7) == 0  # 7.5
    for i in range(n):
        assert abs(c.value(i) - (a.value(i) + b.value(i)*3.0 - a.value(i)/2.0)) < 0.0001
        assert abs(d.value(i) - (- a.value(i))) < 0.0001
        assert abs(e.value(i) - a.value(i)) < 0.00001
        assert abs(f.value(i) - 300.0) < 0.00001
        assert abs(h.value(i) - 300.0) < 0.00001
        assert abs(g.value(i) - (-300.0)) < 0.00001
        assert abs(k.value(i) - (-300.0)) < 0.00001
        assert abs(log_func.value(i) - np.log(c.value(i))) < 1e-5
        assert abs(log_ts.value(i) - np.log(c.value(i))) < 1e-5
    # now some more detailed tests for setting values
    b.set(0, 3.0)
    assert round(abs(b.value(0) - 3.0), 7) == 0
    #  3.0 + 3 * 3 - 3.0/2.0
    assert abs(c.value(1) - 7.5) < 0.0001  # 3 + 3*3  - 1.5 = 10.5
    assert abs(c.value(0) - 10.5) < 0.0001  # 3 + 3*3  - 1.5 = 10.5


def test_timeseries_vector():
    u = TimeSeriesUtilCase()
    c = Calendar()
    t0 = utctime_now()
    dt = deltahours(1)
    n = 240
    ta = TimeAxisFixedDeltaT(t0, dt, n)

    a = TimeSeries(ta=ta, fill_value=3.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    b = TimeSeries(ta=ta, fill_value=2.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)

    v = TsVector()
    v.append(a)
    v.append(b)

    assert len(v) == 2
    assert round(abs(v[0].value(0) - 3.0), 7) == 0, "expect first ts to be 3.0"
    aa = TimeSeries(ta=a.time_axis, values=a.values,
                    point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)  # copy construct (really copy the values!)
    a.fill(1.0)
    assert round(abs(v[0].value(0) - 1.0), 7) == 0, "expect first ts to be 1.0, because the vector keeps a reference "
    assert round(abs(aa.value(0) - 3.0), 7) == 0


def test_timeseries_vector_logarithm():
    u = TimeSeriesUtilCase()
    t0 = utctime_now()
    dt = deltahours(1)
    n = 240
    ta = TimeAxisFixedDeltaT(t0, dt, n)

    a = TimeSeries(ta=ta, fill_value=3.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    b = TimeSeries(ta=ta, fill_value=2.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)

    tsv = TsVector([a, b])
    tsv_log = tsv.log()

    assert len(tsv) == len(tsv_log)
    assert len(tsv[0]) == len(tsv_log[0])
    assert len(tsv[1]) == len(tsv_log[1])
    for i in range(n):
        assert abs(tsv_log[0].value(i) - np.log(3.0)) < 1e-5
        assert abs(tsv_log[1].value(i) - np.log(2.0)) < 1e-5


def test_percentiles():
    u = TimeSeriesUtilCase()
    c = Calendar()
    t0 = c.time(2016, 1, 1)
    dt = deltahours(1)
    n = 240
    ta = TimeAxisFixedDeltaT(t0, dt, n)
    timeseries = TsVector()

    for i in range(10):
        timeseries.append(
            TimeSeries(ta=ta, fill_value=i, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE))

    wanted_percentiles = IntVector([statistics_property.MIN_EXTREME,
                                    0, 10, 50,
                                    statistics_property.AVERAGE,
                                    70, 100,
                                    statistics_property.MAX_EXTREME])
    ta_day = TimeAxisFixedDeltaT(t0, dt*24, n//24)
    ta_day2 = TimeAxis(t0, dt*24, n//24)
    percentiles = timeseries.percentiles(ta_day, wanted_percentiles)
    percentiles2 = timeseries.percentiles(ta_day2, wanted_percentiles)  # just to verify it works with alt. syntax

    assert len(percentiles2) == len(percentiles)

    for i in range(len(ta_day)):
        assert round(abs(0.0 - percentiles[0].value(i)), 3) == 0, "min-extreme "
        assert round(abs(0.0 - percentiles[1].value(i)), 3) == 0, "  0-percentile"
        assert round(abs(0.9 - percentiles[2].value(i)), 3) == 0, " 10-percentile"
        assert round(abs(4.5 - percentiles[3].value(i)), 3) == 0, " 50-percentile"
        assert round(abs(4.5 - percentiles[4].value(i)), 3) == 0, "   -average"
        assert round(abs(6.3 - percentiles[5].value(i)), 3) == 0, " 70-percentile"
        assert round(abs(9.0 - percentiles[6].value(i)), 3) == 0, "100-percentile"
        assert round(abs(9.0 - percentiles[7].value(i)), 3) == 0, "max-extreme"


def test_percentiles_with_min_max_extremes():
    u = TimeSeriesUtilCase()
    """ the percentiles function now also supports picking out the min-max peak value
        within each interval.
        Setup test-data so that we have a well known percentile result,
        but also have peak-values within the interval that we can
        verify.
        We let hour ts 0..9 have values 0..9 constant 24*10 days
           then modify ts[1], every day first  value to a peak min value equal to - day_no*1
                              every day second value to a peak max value equal to + day_no*1
                              every day 3rd    value to a nan value
        ts[1] should then have same average value for each day (so same percentile)
                                        but min-max extreme should be equal to +- day_no*1
    """
    c = Calendar()
    t0 = c.time(2016, 1, 1)
    dt = deltahours(1)
    n = 240
    ta = TimeAxis(t0, dt, n)
    timeseries = TsVector()
    p_fx = point_interpretation_policy.POINT_AVERAGE_VALUE
    for i in range(10):
        timeseries.append(TimeSeries(ta=ta, fill_value=i, point_fx=p_fx))

    ts = timeseries[1]  # pick this one to insert min/max extremes
    for i in range(0, 240, 24):
        ts.set(i + 0, 1.0 - 100*i/24.0)
        ts.set(i + 1, 1.0 + 100*i/24.0)  # notice that when i==0, this gives 1.0
        ts.set(i + 2, float('nan'))  # also put in a nan, just to verify it is ignored during average processing

    wanted_percentiles = IntVector([statistics_property.MIN_EXTREME,
                                    0, 10, 50,
                                    statistics_property.AVERAGE,
                                    70, 100,
                                    statistics_property.MAX_EXTREME])
    ta_day = TimeAxis(t0, dt*24, n//24)
    percentiles = timeseries.percentiles(ta_day, wanted_percentiles)
    for i in range(len(ta_day)):
        if i == 0:  # first timestep, the min/max extremes are picked from 0'th and 9'th ts.
            assert round(abs(0.0 - percentiles[0].value(i)), 3) == 0, "min-extreme "
            assert round(abs(9.0 - percentiles[7].value(i)), 3) == 0, "min-extreme "
        else:
            assert round(abs(1.0 - 100.0*i*24.0/24.0 - percentiles[0].value(i)), 3) == 0, "min-extreme "
            assert round(abs(1.0 + 100.0*i*24.0/24.0 - percentiles[7].value(i)), 3) == 0, "max-extreme"
        assert round(abs(0.0 - percentiles[1].value(i)), 3) == 0, "  0-percentile"
        assert round(abs(0.9 - percentiles[2].value(i)), 3) == 0, " 10-percentile"
        assert round(abs(4.5 - percentiles[3].value(i)), 3) == 0, " 50-percentile"
        assert round(abs(4.5 - percentiles[4].value(i)), 3) == 0, "   -average"
        assert round(abs(6.3 - percentiles[5].value(i)), 3) == 0, " 70-percentile"
        assert round(abs(9.0 - percentiles[6].value(i)), 3) == 0, "100-percentile"


def test_time_shift():
    u = TimeSeriesUtilCase()
    c = Calendar()
    t0 = c.time(2016, 1, 1)
    t1 = c.time(2017, 1, 1)
    dt = deltahours(1)
    n = 240
    ta = TimeAxisFixedDeltaT(t0, dt, n)
    ts0 = TimeSeries(ta=ta, fill_value=3.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    tsa = TimeSeries('a')

    ts1 = time_shift(tsa, t1 - t0)
    assert ts1.needs_bind()
    ts1_blob = ts1.serialize()
    ts1 = TimeSeries.deserialize(ts1_blob)
    tsb = ts1.find_ts_bind_info()
    assert len(tsb) == 1
    tsb[0].ts.bind(ts0)
    ts1.bind_done()
    assert not ts1.needs_bind()

    ts2 = 2.0*ts1.time_shift(t0 - t1)  # just to verify it still can take part in an expression

    for i in range(ts0.size()):
        assert round(abs(ts0.value(i) - ts1.value(i)), 3) == 0, "expect values to be equal"
        assert round(abs(ts0.value(i)*2.0 - ts2.value(i)), 3) == 0, "expect values to be double value"
        assert ts0.time(i) + (t1 - t0) == ts1.time(i), "expect time to be offset delta_t different"
        assert ts0.time(i) == ts2.time(i), "expect time to be equal"


def test_accumulate():
    u = TimeSeriesUtilCase()
    c = Calendar()
    t0 = c.time(2016, 1, 1)
    dt = deltahours(1)
    n = 240
    ta = TimeAxis(t0, dt, n)
    ts0 = TimeSeries(ta=ta, fill_value=1.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    tsa = 1.0*TimeSeries('a') + 0.0  # an expression, that need bind

    ts1 = tsa.accumulate(ta)  # ok, maybe we should make method that does time-axis implicit ?
    assert ts1.needs_bind()
    ts1_blob = ts1.serialize()
    ts1 = TimeSeries.deserialize(ts1_blob)
    tsb = ts1.find_ts_bind_info()
    assert len(tsb) == 1
    tsb[0].ts.bind(ts0)
    ts1.bind_done()
    assert not ts1.needs_bind()

    ts1_values = ts1.values
    for i in range(n):
        expected_value = float(i*dt*1.0)
        assert round(abs(expected_value - ts1.value(i)), 3) == 0, "expect integral f(t)*dt"
        assert round(abs(expected_value - ts1_values[i]), 3) == 0, "expect value vector equal as well"


def test_integral():
    u = TimeSeriesUtilCase()
    c = Calendar()
    t0 = c.time(2016, 1, 1)
    dt = deltahours(1)
    n = 240
    ta = TimeAxis(t0, dt, n)
    fill_value = 1.0
    ts = TimeSeries(ta=ta, fill_value=fill_value, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    tsa = TimeSeries('a')*1.0 + 0.0  # expression, needing bind
    tsb = TimeSeries('b')*1.0 + 0.0  # another expression, needing bind for different ts
    ts_i1 = tsa.integral(ta)
    ts_i2 = integral(tsb, ta)
    # circulate through serialization
    ts_i1_blob = ts_i1.serialize()
    ts_i2_blob = ts_i2.serialize()
    ts_i1 = TimeSeries.deserialize(ts_i1_blob)
    ts_i2 = TimeSeries.deserialize(ts_i2_blob)

    for ts_i in [ts_i1, ts_i2]:
        assert ts_i.needs_bind()
        tsb = ts_i.find_ts_bind_info()
        assert len(tsb) == 1
        tsb[0].ts.bind(ts)
        ts_i.bind_done()
        assert not ts_i.needs_bind()

    ts_i1_values = ts_i1.values
    for i in range(n):
        expected_value = float(dt*fill_value)
        assert round(abs(expected_value - ts_i1.value(i)), 4) == 0, "expect integral of each interval"
        assert round(abs(expected_value - ts_i2.value(i)), 4) == 0, "expect integral of each interval"
        assert round(abs(expected_value - ts_i1_values[i]), 4) == 0, "expect integral of each interval"


def test_kling_gupta_and_nash_sutcliffe():
    u = TimeSeriesUtilCase()
    """
    Test/verify exposure of the kling_gupta and nash_sutcliffe correlation functions

    """

    def np_nash_sutcliffe(o, p):
        return 1 - (np.sum((o - p)**2))/(np.sum((o - np.mean(o))**2))

    c = Calendar()
    t0 = c.time(2016, 1, 1)
    dt = deltahours(1)
    n = 240
    ta = TimeAxis(t0, dt, n)
    from math import sin, pi
    rad_max = 10*2*pi
    obs_values = DoubleVector.from_numpy(np.array([sin(i*rad_max/n) for i in range(n)]))
    mod_values = DoubleVector.from_numpy(np.array([0.1 + sin(pi/10.0 + i*rad_max/n) for i in range(n)]))
    obs_ts = TimeSeries(ta=ta, values=obs_values, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    mod_ts = TimeSeries(ta=ta, values=mod_values, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)

    assert round(abs(kling_gupta(obs_ts, obs_ts, ta, 1.0, 1.0, 1.0) - 1.0), None) == 0, "1.0 for perfect match"
    assert round(abs(nash_sutcliffe(obs_ts, obs_ts, ta) - 1.0), None) == 0, "1.0 for perfect match"
    # verify some non trivial cases, and compare to numpy version of ns
    mod_inv = obs_ts*-1.0
    kge_inv = obs_ts.kling_gupta(mod_inv)  # also show how to use time-series.method itself to ease use
    ns_inv = obs_ts.nash_sutcliffe(mod_inv)  # similar for nash_sutcliffe, you can reach it directly from a ts
    ns_inv2 = np_nash_sutcliffe(obs_ts.values.to_numpy(), mod_inv.values.to_numpy())
    assert kge_inv <= 1.0, "should be less than 1"
    assert ns_inv <= 1.0, "should be less than 1"
    assert round(abs(ns_inv - ns_inv2), 4) == 0, "should equal numpy calculated value"
    kge_obs_mod = kling_gupta(obs_ts, mod_ts, ta, 1.0, 1.0, 1.0)
    assert kge_obs_mod <= 1.0
    assert round(abs(obs_ts.nash_sutcliffe(mod_ts) - np_nash_sutcliffe(obs_ts.values.to_numpy(), mod_ts.values.to_numpy())), 7) == 0


def test_periodic_pattern_ts():
    u = TimeSeriesUtilCase()
    c = Calendar()
    t0 = c.time(2016, 1, 1)
    dt = deltahours(1)
    n = 240
    ta = TimeAxis(t0, dt, n)
    pattern_values = DoubleVector.from_numpy(np.arange(8))
    pattern_dt = deltahours(3)
    pattern_t0 = c.time(2015, 6, 1)
    pattern_ts = create_periodic_pattern_ts(pattern_values, pattern_dt, pattern_t0,
                                            ta)  # this is how to create a periodic pattern ts (used in gridpp/kalman bias handling)
    assert round(abs(pattern_ts.value(0) - 0.0), 7) == 0
    assert round(abs(pattern_ts.value(1) - 0.0), 7) == 0
    assert round(abs(pattern_ts.value(2) - 0.0), 7) == 0
    assert round(abs(pattern_ts.value(3) - 1.0), 7) == 0  # next step in pattern starts here
    assert round(abs(pattern_ts.value(24) - 0.0), 7) == 0  # next day repeats the pattern


def test_partition_by():
    u = TimeSeriesUtilCase()
    """
    verify/demo exposure of the .partition_by function that can
    be used to produce yearly percentiles statistics for long historical
    time-series

    """
    c = Calendar()
    t0 = c.time(1930, 9, 1)
    dt = deltahours(1)
    n = c.diff_units(t0, c.time(2016, 9, 1), dt)

    ta = TimeAxis(t0, dt, n)
    pattern_values = DoubleVector.from_numpy(np.arange(len(ta)))  # increasing values

    src_ts = TimeSeries(ta=ta, values=pattern_values,
                        point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)

    partition_t0 = c.time(2016, 9, 1)
    n_partitions = 80
    partition_interval = Calendar.YEAR
    # get back TsVector,
    # where all TsVector[i].index_of(partition_t0)
    # is equal to the index ix for which the TsVector[i].value(ix) correspond to start value of that particular partition.
    ts_partitions = src_ts.partition_by(c, t0, partition_interval, n_partitions, partition_t0)
    assert len(ts_partitions) == n_partitions
    ty = t0
    for ts in ts_partitions:
        ix = ts.index_of(partition_t0)
        vix = ts.value(ix)
        expected_value = c.diff_units(t0, ty, dt)
        assert vix == expected_value
        ty = c.add(ty, partition_interval, 1)

    # Now finally, try percentiles on the partitions
    wanted_percentiles = [0, 10, 25, -1, 50, 75, 90, 100]
    ta_percentiles = TimeAxis(partition_t0, deltahours(24), 365)
    percentiles = ts_partitions.percentiles(ta_percentiles, wanted_percentiles)
    assert len(percentiles) == len(wanted_percentiles)


def test_empty_ts():
    u = TimeSeriesUtilCase()
    a = TimeSeries()
    assert a.size() == 0
    assert a.values.size() == 0
    assert len(a.values.to_numpy()) == 0
    assert not a.total_period().valid()
    assert not a  # evaluate to false
    try:
        a.time_axis
        u.assertFail("Expected exception")
    except RuntimeError as re:
        pass


def test_unbound_ts():
    u = TimeSeriesUtilCase()
    a = TimeSeries('a')
    assert a.needs_bind() == True
    assert a.ts_id() == 'a'
    with pytest.raises(RuntimeError):
        a.size()
    with pytest.raises(RuntimeError):
        a.time_axis
    with pytest.raises(RuntimeError):
        a.values
    with pytest.raises(RuntimeError):
        a.point_interpretation()

    s = ts_stringify(a*3.0 + 1.0)
    assert len(s) > 0
    pass


def test_abs():
    u = TimeSeriesUtilCase()
    c = Calendar()
    t0 = c.time(2016, 1, 1)
    dt = deltahours(1)
    n = 4
    v = DoubleVector([1.0, -1.5, float("nan"), 3.0])
    ta = TimeAxisFixedDeltaT(t0, dt, n)
    ts0 = TimeSeries(ta=ta, values=v, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    tsa = TimeSeries('a')
    ts1 = tsa.abs()
    ts1_blob = ts1.serialize()
    ts1 = TimeSeries.deserialize(ts1_blob)
    assert ts1.needs_bind()
    bts = ts1.find_ts_bind_info()
    assert len(bts) == 1
    bts[0].ts.bind(ts0)
    ts1.bind_done()
    assert not ts1.needs_bind()
    assert round(abs(ts0.value(0) - ts1.value(0)), 6) == 0
    assert round(abs(abs(ts0.value(1)) - ts1.value(1)), 6) == 0
    assert math.isnan(ts1.value(2))
    assert round(abs(ts0.value(3) - ts1.value(3)), 6) == 0
    tsv0 = TsVector()
    tsv0.append(ts0)
    tsv1 = tsv0.abs()
    assert round(abs(tsv0[0].value(0) - tsv1[0].value(0)), 6) == 0
    assert round(abs(abs(tsv0[0].value(1)) - tsv1[0].value(1)), 6) == 0
    assert math.isnan(tsv1[0].value(2))
    assert round(abs(tsv0[0].value(3) - tsv1[0].value(3)), 6) == 0


def test_ts_reference_and_bind():
    u = TimeSeriesUtilCase()
    c = Calendar()
    t0 = c.time(2016, 9, 1)
    dt = deltahours(1)
    n = c.diff_units(t0, c.time(2017, 9, 1), dt)

    ta = TimeAxis(t0, dt, n)
    pattern_values = DoubleVector.from_numpy(np.arange(len(ta)))  # increasing values

    a = TimeSeries(ta=ta, values=pattern_values, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    b_id = "netcdf://path_to_file/path_to_ts"
    b = TimeSeries(b_id)
    c = (a + b)*4.0  # make an expression, with a ts-reference, not yet bound
    c_blob = c.serialize()  # converts the entire stuff into a blob
    bind_info = c.find_ts_bind_info()

    assert len(bind_info) == 1, "should find just one ts to bind"
    assert bind_info[0].id == b_id, "the id to bind should be equal to b_id"
    try:
        c.value(0)  # verify touching a unbound ts raises exception
        assert not True, "should not reach here!"
    except RuntimeError:
        pass
    assert c.needs_bind() == True  # verify this expression needs binding
    # verify we can bind a ts
    bind_info[0].ts.bind(a)  # it's ok to bind same series multiple times, it takes a copy of a values
    c.bind_done()
    assert c.needs_bind() == False  # verify this expression do not need binding anymore
    # and now we can use c expression as pr. usual, evaluate etc.
    assert round(abs(c.value(10) - a.value(10)*2*4.0), 3) == 0

    c_resurrected = TimeSeries.deserialize(c_blob)

    bi = c_resurrected.find_ts_bind_info()
    bi[0].ts.bind(a)
    c_resurrected.bind_done()
    assert round(abs(c_resurrected.value(10) - a.value(10)*2*4.0), 3) == 0
    # verify we can create a ref.ts with something that resolves to a point ts.
    bind_expr_ts = TimeSeries("some_sym", 3.0*a)  # notice that we can bind with something that is an expression
    assert bind_expr_ts is not None
    assert round(abs(bind_expr_ts.value(0) - 3.0*a.value(0)), 7) == 0  # just to check, its for real


def test_ts_stringify():
    u = TimeSeriesUtilCase()
    a = TimeSeries('a')
    b = TimeSeries('b')
    c = a + b
    s_a = ts_stringify(a)
    s_a = ts_stringify(c)
    assert s_a is not None


def test_a_time_series_vector():
    u = TimeSeriesUtilCase()
    c = Calendar()
    t0 = c.time(2018, 7, 1)
    dt = deltahours(1)
    n = 240
    ta = TimeAxisFixedDeltaT(t0, dt, n)

    a = TimeSeries(ta=ta, fill_value=3.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    b = TimeSeries(ta=ta, fill_value=2.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    c = TimeSeries(ta=ta, fill_value=10.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    v = TsVector()
    v.append(a)
    v.append(b)

    assert len(v) == 2
    assert round(abs(v[0].value(0) - 3.0), 7) == 0, "expect first ts to be 3.0"
    aa = TimeSeries(ta=a.time_axis, values=a.values,
                    point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)  # copy construct (really copy the values!)
    a.fill(1.0)
    assert round(abs(v[0].value(0) - 1.0), 7) == 0, "expect first ts to be 1.0, because the vector keeps a reference "
    assert round(abs(aa.value(0) - 3.0), 7) == 0

    vt = v.values_at(t0).to_numpy()
    assert len(vt) == len(v)
    v1 = v[0:1]
    assert len(v1) == 1
    assert round(abs(v1[0].value(0) - 1.0), 7) == 0
    v_clone = TsVector(v)
    assert len(v_clone) == len(v)
    del v_clone[-1]
    assert len(v_clone) == 1
    assert len(v) == 2
    v_slice_all = v.slice(IntVector())
    v_slice_1 = v.slice(IntVector([1]))
    v_slice_12 = v.slice(IntVector([0, 1]))
    assert len(v_slice_all) == 2
    assert len(v_slice_1) == 1
    assert round(abs(v_slice_1[0].value(0) - 2.0), 7) == 0
    assert len(v_slice_12) == 2
    assert round(abs(v_slice_12[0].value(0) - 1.0), 7) == 0

    # multiplication by scalar
    v_x_2a = v*2.0
    v_x_2b = 2.0*v
    for i in range(len(v)):
        assert round(abs(v_x_2a[i].value(0) - 2*v[i].value(0)), 7) == 0
        assert round(abs(v_x_2b[i].value(0) - 2*v[i].value(0)), 7) == 0

    # division by scalar
    v_d_a = v/3.0
    v_d_b = 3.0/v
    for i in range(len(v)):
        assert round(abs(v_d_a[i].value(0) - v[i].value(0)/3.0), 7) == 0
        assert round(abs(v_d_b[i].value(0) - 3.0/v[i].value(0)), 7) == 0

    # addition by scalar
    v_a_a = v + 3.0
    v_a_b = 3.0 + v
    for i in range(len(v)):
        assert round(abs(v_a_a[i].value(0) - (v[i].value(0) + 3.0)), 7) == 0
        assert round(abs(v_a_b[i].value(0) - (3.0 + v[i].value(0))), 7) == 0

    # sub by scalar
    v_s_a = v - 3.0
    v_s_b = 3.0 - v
    for i in range(len(v)):
        assert round(abs(v_s_a[i].value(0) - (v[i].value(0) - 3.0)), 7) == 0
        assert round(abs(v_s_b[i].value(0) - (3.0 - v[i].value(0))), 7) == 0

    # multiplication vector by ts
    v_x_ts = v*c
    ts_x_v = c*v
    for i in range(len(v)):
        assert round(abs(v_x_ts[i].value(0) - v[i].value(0)*c.value(0)), 7) == 0
        assert round(abs(ts_x_v[i].value(0) - c.value(0)*v[i].value(0)), 7) == 0

    # division vector by ts
    v_d_ts = v/c
    ts_d_v = c/v
    for i in range(len(v)):
        assert round(abs(v_d_ts[i].value(0) - v[i].value(0)/c.value(0)), 7) == 0
        assert round(abs(ts_d_v[i].value(0) - c.value(0)/v[i].value(0)), 7) == 0

    # add vector by ts
    v_a_ts = v + c
    ts_a_v = c + v
    for i in range(len(v)):
        assert round(abs(v_a_ts[i].value(0) - (v[i].value(0) + c.value(0))), 7) == 0
        assert round(abs(ts_a_v[i].value(0) - (c.value(0) + v[i].value(0))), 7) == 0

    # sub vector by ts
    v_s_ts = v - c
    ts_s_v = c - v
    for i in range(len(v)):
        assert round(abs(v_s_ts[i].value(0) - (v[i].value(0) - c.value(0))), 7) == 0
        assert round(abs(ts_s_v[i].value(0) - (c.value(0) - v[i].value(0))), 7) == 0

    # vector mult vector
    va = v
    vb = 2.0*v

    v_m_v = va*vb
    assert len(v_m_v) == len(va)
    for i in range(len(va)):
        assert round(abs(v_m_v[i].value(0) - va[i].value(0)*vb[i].value(0)), 7) == 0

    # vector div vector
    v_d_v = va/vb
    assert len(v_d_v) == len(va)
    for i in range(len(va)):
        assert round(abs(v_d_v[i].value(0) - va[i].value(0)/vb[i].value(0)), 7) == 0

    # vector add vector
    v_a_v = va + vb
    assert len(v_a_v) == len(va)
    for i in range(len(va)):
        assert round(abs(v_a_v[i].value(0) - (va[i].value(0) + vb[i].value(0))), 7) == 0

    # vector sub vector
    v_s_v = va - vb
    assert len(v_s_v) == len(va)
    for i in range(len(va)):
        assert round(abs(v_s_v[i].value(0) - (va[i].value(0) - vb[i].value(0))), 7) == 0

    # vector unary minus
    v_u = - va
    assert len(v_u) == len(va)
    for i in range(len(va)):
        assert round(abs(v_u[i].value(0) - (-va[i].value(0))), 7) == 0

    # integral functions, just to verify exposure works, and one value is according to spec.
    ta2 = TimeAxis(t0, dt*24, n//24)
    v_avg = v.average(ta2)
    v_int = v.integral(ta2)
    v_acc = v.accumulate(ta2)
    v_sft = v.time_shift(dt*24)
    assert v_avg is not None
    assert v_int is not None
    assert v_acc is not None
    assert v_sft is not None
    assert round(abs(v_avg[0].value(0) - 1.0), 7) == 0
    assert round(abs(v_int[0].value(0) - 86400.0), 7) == 0
    assert round(abs(v_acc[0].value(0) - 0.0), 7) == 0
    assert v_sft[0].time(0) == (t0 + dt*24)

    # min/max functions
    min_v_double = va.min(-1000.0)
    max_v_double = va.max(1000.0)
    assert round(abs(min_v_double[0].value(0) - (-1000.0)), 7) == 0
    assert round(abs(max_v_double[0].value(0) - (+1000.0)), 7) == 0
    min_v_double = min(va, -1000.0)
    max_v_double = max(va, +1000.0)
    assert round(abs(min_v_double[0].value(0) - (-1000.0)), 7) == 0
    assert round(abs(max_v_double[0].value(0) - (+1000.0)), 7) == 0
    # c = 10.0
    c1000 = 100.0*c
    min_v_double = va.min(-c1000)
    max_v_double = va.max(c1000)
    assert round(abs(min_v_double[0].value(0) - (-c1000.value(0))), 7) == 0
    assert round(abs(max_v_double[0].value(0) - (c1000.value(0))), 7) == 0
    min_v_double = min(va, -c1000)
    max_v_double = max(va, c1000)
    assert round(abs(min_v_double[0].value(0) - (-c1000.value(0))), 7) == 0
    assert round(abs(max_v_double[0].value(0) - (c1000.value(0))), 7) == 0

    v1000 = va*1000.0
    min_v_double = va.min(-v1000)
    max_v_double = va.max(v1000)
    assert round(abs(min_v_double[0].value(0) - (-v1000[0].value(0))), 7) == 0
    assert round(abs(max_v_double[0].value(0) - (v1000[0].value(0))), 7) == 0
    min_v_double = min(va, -v1000)
    max_v_double = max(va, v1000)
    assert round(abs(min_v_double[0].value(0) - (-v1000[0].value(0))), 7) == 0
    assert round(abs(max_v_double[0].value(0) - (v1000[0].value(0))), 7) == 0

    # finally, test that exception is raised if we try to multiply two unequal sized vectors

    try:
        x = v_clone*va
        assert False, 'We expected exception for unequal sized ts-vector op'
    except RuntimeError as re:
        pass

    # also test that empty vector + vector -> vector etc.
    va_2 = va + TsVector()
    va_3 = TsVector() + va
    va_4 = va - TsVector()
    va_5 = TsVector() - va
    va_x = TsVector() + TsVector()
    assert len(va_2) == len(va)
    assert len(va_3) == len(va)
    assert len(va_4) == len(va)
    assert len(va_5) == len(va)
    assert (not va_x) == True
    assert (not va_2) == False
    va_2_ok = False
    va_x_ok = True
    if va_2:
        va_2_ok = True
    if va_x:
        va_x_ok = False
    assert va_2_ok
    assert va_x_ok


def test_ts_extend():
    u = TimeSeriesUtilCase()
    t0 = utctime_now()
    dt = deltahours(1)
    n = 512
    ta_a = TimeAxisFixedDeltaT(t0, dt, 2*n)
    ta_b = TimeAxisFixedDeltaT(t0 + n*dt, dt, 2*n)
    ta_c = TimeAxisFixedDeltaT(t0 + 2*n*dt, dt, 2*n)
    ta_d = TimeAxisFixedDeltaT(t0 + 3*n*dt, dt, 2*n)

    a = TimeSeries(ta=ta_a, fill_value=1.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    b = TimeSeries(ta=ta_b, fill_value=2.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    c = TimeSeries(ta=ta_c, fill_value=4.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    d = TimeSeries(ta=ta_d, fill_value=8.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)

    # default behavior: extend from end of a
    ac = a.extend(c)

    for i in range(2*n):  # valus from first ts
        assert ac(t0 + i*dt) == 1.0
    for i in range(2*n):  # values from extension ts
        assert ac(t0 + (i + 2*n)*dt) == 4.0

    # default behavior: extend from end of a, fill gap with nan
    ad = a.extend(d)

    for i in range(2*n):  # values from first
        assert ad(t0 + i*dt) == 1.0
    for i in range(n):  # gap
        assert math.isnan(ad(t0 + (i + 2*n)*dt))
    for i in range(2*n):  # extension
        assert ad(t0 + (i + 3*n)*dt) == 8.0

    # split at the first value of d instead of last of c
    cd = c.extend(d, split_policy=extend_split_policy.RHS_FIRST)

    for i in range(n):  # first, only until the extension start
        assert cd(t0 + (2*n + i)*dt) == 4.0
    for i in range(2*n):  # extension
        assert cd(t0 + (3*n + i)*dt) == 8.0

    # split at a given time step, and extend the last value through the gap
    ac = a.extend(c, split_policy=extend_split_policy.AT_VALUE, split_at=(t0 + dt*n//2),
                  fill_policy=extend_fill_policy.USE_LAST)

    for i in range(n//2):  # first, only until the given split value
        assert ac(t0 + i*dt) == 1.0
    for i in range(3*n//2):  # gap, uses last value before gap
        assert ac(t0 + (n//2 + i)*dt) == 1.0
    for i in range(2*n):  # extension
        assert ac(t0 + (2*n + i)*dt) == 4.0

    # split at the beginning of the ts to extend when the extension start before it
    cb = c.extend(b, split_policy=extend_split_policy.AT_VALUE, split_at=(t0 + 2*n*dt))

    for i in range(n):  # don't extend before
        assert math.isnan(cb(t0 + (n + i)*dt))
    for i in range(n):  # we split at the beginning => only values from extension
        assert cb(t0 + (2*n + i)*dt) == 2.0
    for i in range(n):  # no values after extension
        assert math.isnan(cb(t0 + (3*n + i)*dt))

    # extend with ts starting after the end, fill the gap with a given value
    ad = a.extend(d, fill_policy=extend_fill_policy.FILL_VALUE, fill_value=5.5)

    for i in range(2*n):  # first
        assert ad(t0 + i*dt) == 1.0
    for i in range(n):  # gap, filled with 5.5
        assert ad(t0 + (2*n + i)*dt) == 5.5
    for i in range(2*n):  # extension
        assert ad(t0 + (3*n + i)*dt) == 8.0

    # check extend with more exotic combination of time-axis(we had an issue with this..)
    a = TimeSeries(TimeAxis(0, 1, 10), fill_value=1.0, point_fx=POINT_AVERAGE_VALUE)
    b = TimeSeries(TimeAxis(Calendar(), 0, 1, 20), fill_value=2.0, point_fx=POINT_AVERAGE_VALUE)
    ab = a.extend(b)
    ba = b.extend(a, split_policy=extend_split_policy.AT_VALUE, split_at=a.time_axis.time(5))
    assert round(abs(ab.value(0) - 1.0), 7) == 0
    assert round(abs(ab.value(11) - 2.0), 7) == 0
    assert round(abs(ba.value(0) - 2.0), 7) == 0
    assert round(abs(ab.value(7) - 1.0), 7) == 0


def test_ts_extend_with_points_fail():
    u = TimeSeriesUtilCase()
    # testcase from issue https://github.com/statkraft/shyft/issues/414
    # reported by Tobias:
    # Extend fails when extender and extendee time_axis do not overlap:
    tsp_1_fail = TimeSeries(TimeAxis([0, 1, 1.5]), [1, 2], POINT_INSTANT_VALUE)
    tsp_2_fail = TimeSeries(TimeAxis([2, 3, 4]), [3, 4], POINT_INSTANT_VALUE)
    extend_fail = tsp_1_fail.extend(tsp_2_fail)
    assert len(extend_fail.time_axis) == 5
    tsp_1 = TimeSeries(TimeAxis([0, 1, 2]), [1, 2], POINT_INSTANT_VALUE)
    tsp_2 = TimeSeries(TimeAxis([2, 3, 4]), [3, 4], POINT_INSTANT_VALUE)

    extend = tsp_1.extend(tsp_2)
    assert len(extend.time_axis) == 4


def test_ts_extend_exact_at_end():
    u = TimeSeriesUtilCase()
    a = TimeSeries(TimeAxis([0, 1, 1.5]), [1, 2], POINT_INSTANT_VALUE)
    b = TimeSeries(TimeAxis([1.5, 3, 4]), [3, 4], POINT_INSTANT_VALUE)
    r = a.extend(b)
    assert r is not None
    assert r.values is not None
    assert len(r.time_axis) == 4


def test_ts_slice():
    u = TimeSeriesUtilCase()

    # Test data
    values = np.linspace(1, 4, 4)
    dt = 3600
    times = [0.0] + [dt*v for v in values]

    # Make three TimeSeries with with the same data in three different TimeAxes
    tsv = TsVector()
    tsv.append(TimeSeries(ta=TimeAxis(times[0], dt, len(values)),
                          values=values, point_fx=POINT_AVERAGE_VALUE))
    tsv.append(TimeSeries(ta=TimeAxis(Calendar(), times[0], dt, len(values)),
                          values=values, point_fx=POINT_AVERAGE_VALUE))
    tsv.append(TimeSeries(ta=TimeAxis(times),
                          values=values, point_fx=POINT_AVERAGE_VALUE))

    for ts in tsv:

        # Make all possible valid slices of all timeseries
        for start in range(len(values)):
            for n in range(1, len(values) - start + 1):
                sliced = ts.slice(start, n)

                # We should have a sliced TimeSeriew with n elements and identical point interpretation
                assert len(sliced) == n
                assert len(sliced.time_axis) == n
                assert sliced.point_interpretation() == ts.point_interpretation()

                # Verify n identical time_points and values
                for i in range(n):
                    assert sliced.value(i) == ts.value(start + i)
                    assert sliced.time_axis.time(i) == ts.time_axis.time(start + i)

                # Verify last time point
                last_ix = start + n
                end = ts.time_axis.total_period().end
                if last_ix < len(ts):
                    end = ts.time_axis.time(last_ix)
                assert sliced.time_axis.total_period().end == end

        # Then verify that invalid slices fail
        with pytest.raises(RuntimeError):
            ts.slice(-10, 1)  # Start before beginning
        with pytest.raises(RuntimeError):
            ts.slice(10, 1)  # Start after end
        with pytest.raises(RuntimeError):
            ts.slice(0, 10)  # Request too many items
        with pytest.raises(RuntimeError):
            ts.slice(0, -10)  # Request too few items
        with pytest.raises(RuntimeError):
            ts.slice(0, 0)  # Request no items


def test_extend_vector_of_timeseries():
    u = TimeSeriesUtilCase()
    t0 = utctime_now()
    dt = deltahours(1)
    n = 512

    tsvector = TsVector()

    ta = TimeAxisFixedDeltaT(t0 + 3*n*dt, dt, 2*n)

    tsvector.push_back(TimeSeries(
        ta=TimeAxisFixedDeltaT(t0, dt, 2*n),
        fill_value=1.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE))
    tsvector.push_back(TimeSeries(
        ta=TimeAxisFixedDeltaT(t0 + 2*n*dt, dt, 2*n),
        fill_value=2.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE))

    extension = TimeSeries(ta=ta, fill_value=8.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)

    # extend after all time-series in the vector
    extended_tsvector = tsvector.extend_ts(extension)

    # assert first element
    for i in range(2*n):
        assert extended_tsvector[0](t0 + i*dt) == 1.0
    for i in range(n):
        assert math.isnan(extended_tsvector[0](t0 + (2*n + i)*dt))
    for i in range(2*n):
        assert extended_tsvector[0](t0 + (3*n + i)*dt) == 8.0

    # assert second element
    for i in range(2*n):
        assert extended_tsvector[1](t0 + (2*n + i)*dt) == 2.0
    for i in range(n):
        assert extended_tsvector[1](t0 + (4*n + i)*dt) == 8.0

    tsvector_2 = TsVector()
    tsvector_2.push_back(TimeSeries(
        ta=TimeAxisFixedDeltaT(t0 + 2*n*dt, dt, 4*n),
        fill_value=10.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE))
    tsvector_2.push_back(TimeSeries(
        ta=TimeAxisFixedDeltaT(t0 + 4*n*dt, dt, 4*n),
        fill_value=20.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE))

    # extend each element in tsvector by the corresponding element in tsvector_2
    extended_tsvector = tsvector.extend_ts(tsvector_2)

    # assert first element
    for i in range(2*n):
        assert extended_tsvector[0](t0 + i*dt) == 1.0
    for i in range(4*n):
        assert extended_tsvector[0](t0 + (2*n + i)*dt) == 10.0

    # assert second element
    for i in range(2*n):
        assert extended_tsvector[1](t0 + (2*n + i)*dt) == 2.0
    for i in range(4*n):
        assert extended_tsvector[1](t0 + (4*n + i)*dt) == 20.0


def test_rating_curve_ts():
    u = TimeSeriesUtilCase()
    t0 = Calendar().time(2018, 7, 1)
    ta = TimeAxis(t0, deltaminutes(30), 48*2)
    data = np.linspace(0, 10, ta.size())
    ts = TimeSeries(ta, data, POINT_INSTANT_VALUE)

    rcf1 = RatingCurveFunction()
    rcf1.add_segment(0, 1, 0, 1)
    rcf1.add_segment(RatingCurveSegment(5, 2, 0, 1))

    rcf2 = RatingCurveFunction()
    rcf2.add_segment(0, 3, 0, 1)
    rcf2.add_segment(RatingCurveSegment(8, 4, 0, 1))

    rcp = RatingCurveParameters()
    rcp.add_curve(t0, rcf1)
    rcp.add_curve(t0 + deltahours(24), rcf2)

    sts = TimeSeries("a")
    rcsts = sts.rating_curve(rcp)

    rcsts_blob = rcsts.serialize()
    rcsts_2 = TimeSeries.deserialize(rcsts_blob)

    assert rcsts_2.needs_bind()
    fbi = rcsts_2.find_ts_bind_info()
    assert len(fbi) == 1
    fbi[0].ts.bind(ts)
    rcsts_2.bind_done()
    assert not rcsts_2.needs_bind()

    assert len(rcsts_2) == len(ts)
    for i in range(rcsts_2.size()):
        expected = (1*ts.get(i).v if ts.get(i).v < 5 else 2*ts.get(i).v) if ts.get(i).t < t0 + deltahours(24) else (
            3*ts.get(i).v if ts.get(i).v < 8 else 4*ts.get(i).v)
        assert rcsts_2.get(i).t == ts.get(i).t
        assert rcsts_2.get(i).v == expected


def test_ice_packing_ts_parameters():
    u = TimeSeriesUtilCase()
    p = IcePackingParameters(3600*24*7, -15.0)
    assert p.threshold_window == 3600*24*7
    assert round(abs(p.threshold_temperature - -15.0), 7) == 0
    q = IcePackingParameters(threshold_temperature=-10.2, threshold_window=3600*24*10)
    assert q.threshold_window == 3600*24*10
    assert round(abs(q.threshold_temperature - -10.2), 7) == 0
    assert not (q == p)
    assert p == p
    assert p != q
    q.threshold_temperature = p.threshold_temperature
    q.threshold_window = p.threshold_window
    assert p == q
    assert not (p != q)


def test_ice_packing_recession_parameters():
    u = TimeSeriesUtilCase()
    iprp = IcePackingRecessionParameters(alpha=0.0001, recession_minimum=0.25)
    assert round(abs(iprp.alpha - 0.0001), 7) == 0
    assert round(abs(iprp.recession_minimum - 0.25), 7) == 0
    iprp.alpha *= 2
    iprp.recession_minimum *= 4
    assert round(abs(iprp.alpha - 0.0001*2), 7) == 0
    assert round(abs(iprp.recession_minimum - 0.25*4), 7) == 0
    iprp2 = IcePackingRecessionParameters(alpha=0.0001, recession_minimum=0.25)
    assert iprp != iprp2
    assert not (iprp == iprp2)
    assert iprp == iprp
    assert not (iprp != iprp)
    assert iprp != iprp2


def test_ice_packing_ts():
    u = TimeSeriesUtilCase()
    t0 = Calendar().time(2018, 1, 1)

    ts_dt = deltaminutes(30)
    window = deltahours(1)

    ta = TimeAxis(t0, ts_dt, 5*2)
    data = np.linspace(-5, 5, len(ta))
    data[len(ta)//2] = float('nan')  # introduce missing value
    ts = TimeSeries(ta, data, POINT_INSTANT_VALUE)  # notice that this implicate linear between point
    ipp = IcePackingParameters(threshold_window=window, threshold_temperature=0.0)
    source = TimeSeries("a")
    ip_source = source.ice_packing(ipp, ice_packing_temperature_policy.ALLOW_INITIAL_MISSING)

    ip_source_blob = ip_source.serialize()
    ip_source_2 = TimeSeries.deserialize(ip_source_blob)

    assert ip_source_2.needs_bind()
    fbi = ip_source_2.find_ts_bind_info()
    assert len(fbi) == 1
    fbi[0].ts.bind(ts)
    ip_source_2.bind_done()
    assert not ip_source_2.needs_bind()

    assert len(ip_source_2) == len(ts)
    assert ip_source_2.time_axis == ts.time_axis  # time-axis support equality
    expected = np.array([0, 1, 1, 1, 1, float('nan'), float('nan'), float('nan'), 0, 0], dtype=np.float64)
    assert np.allclose(expected, ip_source_2.values.to_numpy(), equal_nan=True)


def test_ice_packing_recession_ts():
    u = TimeSeriesUtilCase()
    t0 = utctime_now()

    ts_dt = deltaminutes(30)
    window = deltahours(2)

    ta = TimeAxis(t0, ts_dt, 48*2 + 1)
    # flow data
    x0 = int(ta.total_period().start)
    x1 = int(ta.total_period().end)
    x = np.linspace(x0, x1, len(ta))
    flow_data = -0.0000000015*(x - x0)*(x - x1) + 1
    flow_ts = TimeSeries(ta, flow_data, POINT_AVERAGE_VALUE)
    # temperature data
    temperature_data = np.linspace(-5, 5, ta.size())
    temperature_data[48] = float('nan')  # introduce missing value
    temp_ts = TimeSeries(ta, temperature_data, POINT_INSTANT_VALUE)

    ipp = IcePackingParameters(threshold_window=window, threshold_temperature=0.0)
    iprp = IcePackingRecessionParameters(alpha=0.0001, recession_minimum=0.25)
    assert round(abs(iprp.alpha - 0.0001), 7) == 0
    assert round(abs(iprp.recession_minimum - 0.25), 7) == 0

    # unbound data sources
    source_t = TimeSeries("temperature")
    source_f = TimeSeries("flow")

    # unbound computed series
    ip_ts_ub = source_t.ice_packing(ipp, ice_packing_temperature_policy.ALLOW_ANY_MISSING)
    ipr_ts_ub = source_f.ice_packing_recession(ip_ts_ub, iprp)

    # serialize and deserialize
    ip_blob = ip_ts_ub.serialize()
    ip_ts = TimeSeries.deserialize(ip_blob)
    # -----
    ipr_blob = ipr_ts_ub.serialize()
    ipr_ts = TimeSeries.deserialize(ipr_blob)

    # bind the deserialized series
    assert ip_ts.needs_bind()
    assert ipr_ts.needs_bind()
    # -----
    fbi_ip = ip_ts.find_ts_bind_info()
    assert len(fbi_ip) == 1
    assert fbi_ip[0].id == 'temperature'
    fbi_ip[0].ts.bind(temp_ts)
    # -----
    ip_ts.bind_done()
    assert not ip_ts.needs_bind()
    # -----
    fbi_ipr = ipr_ts.find_ts_bind_info()
    assert len(fbi_ipr) == 2
    for i in range(len(fbi_ipr)):
        if fbi_ipr[i].id == 'flow':
            fbi_ipr[i].ts.bind(flow_ts)
        elif fbi_ipr[i].id == 'temperature':
            fbi_ipr[i].ts.bind(temp_ts)
    # -----
    ipr_ts.bind_done()
    assert not ipr_ts.needs_bind()

    prev = None
    assert len(ipr_ts) == len(flow_ts)
    for i in range(ipr_ts.size()):
        ice_packing = (ip_ts.value(i) == 1.)
        value = ipr_ts.value(i)
        if ice_packing:
            if prev is not None:
                assert value <= prev  # assert monotonic decreasing
            prev = value
        else:
            assert value == flow_ts.value(i)
    #
    #     u.assertEqual(ip_source_2.get(i).t, ts.get(i).t)
    #     if math.isnan(expected):
    #         u.assertTrue(math.isnan(ip_source_2.get(i).v))
    #     else:
    #         u.assertEqual(ip_source_2.get(i).v, expected)


def test_krls_ts():
    u = TimeSeriesUtilCase()
    t0 = utctime_now()
    ta = TimeAxis(t0, deltahours(1), 30*24)
    data = np.sin(np.linspace(0, 2*np.pi, ta.size()))
    ts_data = TimeSeries(ta, data, POINT_INSTANT_VALUE)

    ts = TimeSeries("a")
    ts_krls = ts.krls_interpolation(deltahours(3))

    ts_krls_blob = ts_krls.serialize()
    ts2_krls = TimeSeries.deserialize(ts_krls_blob)

    assert ts2_krls.needs_bind()
    fbi = ts2_krls.find_ts_bind_info()
    assert len(fbi) == 1
    fbi[0].ts.bind(ts_data)
    ts2_krls.bind_done()
    assert not ts2_krls.needs_bind()

    assert len(ts2_krls) == len(ts_data)
    for i in range(len(ts2_krls)):
        assert round(abs(ts2_krls.values[i] - ts_data.values[i]), 1) == 0


def test_ts_get_krls_predictor():
    u = TimeSeriesUtilCase()
    t0 = utctime_now()
    ta = TimeAxis(t0, deltahours(1), 30*24)
    data = np.sin(np.linspace(0, 2*np.pi, ta.size()))
    ts_data = TimeSeries(ta, data, POINT_INSTANT_VALUE)

    ts = TimeSeries("a")

    try:
        ts.get_krls_predictor()
        u.fail("should not be able to get predictor for unbound")
    except:
        pass

    fbi = ts.find_ts_bind_info()
    fbi[0].ts.bind(ts_data)
    ts.bind_done()

    pred = ts.get_krls_predictor(deltahours(3))

    ts_krls = pred.predict(ta)
    assert len(ts_krls) == len(ts_data)
    ts_mse = pred.mse_ts(ts_data)
    assert len(ts_mse) == len(ts_data)
    for i in range(len(ts_krls)):
        assert round(abs(ts_krls.values[i] - ts_data.values[i]), 1) == 0
        assert round(abs(ts_mse.values[i] - 0), 2) == 0
    assert round(abs(pred.predictor_mse(ts_data) - 0), 2) == 0


def test_average_outside_give_nan():
    u = TimeSeriesUtilCase()
    ta1 = TimeAxis(0, 10, 10)
    ta2 = TimeAxis(-10, 10, 21)
    tsa = TimeSeries(ta1, fill_value=1.0, point_fx=POINT_AVERAGE_VALUE)
    tsb = tsa.average(ta2)
    assert math.isnan(tsb.value(11))  # nan when a ends
    assert math.isnan(tsb.value(0))  # nan before first a
    tsa = TimeSeries(ta1, fill_value=1.0, point_fx=POINT_INSTANT_VALUE)
    tsb = tsa.average(ta2)
    assert math.isnan(tsb.value(10))  # notice we get one less due to linear-between, it ends at last point in tsa.
    assert math.isnan(tsb.value(0))


def test_integral_fine_resolution():
    u = TimeSeriesUtilCase()
    """ Case study for last-interval bug from python"""
    utc = Calendar()
    ta = TimeAxis(utc.time(2017, 10, 16), deltahours(24*7), 219)
    tf = TimeAxis(utc.time(2017, 10, 16), deltahours(3), 12264)
    src = TimeSeries(ta, fill_value=1.0, point_fx=POINT_AVERAGE_VALUE)
    ts = src.integral(tf)
    assert ts is not None
    for i in range(len(tf)):
        if not math.isclose(ts.value(i), 1.0*deltahours(3)):
            assert round(abs(ts.value(i) - 1.0*deltahours(3)), 7) == 0


def test_min_max_check_linear_fill():
    u = TimeSeriesUtilCase()
    ta = TimeAxis(0, 1, 5)
    ts_src = TimeSeries(ta, values=DoubleVector([1.0, -1.0, 2.0, float('nan'), 4.0]), point_fx=POINT_INSTANT_VALUE)
    ts_qac = ts_src.min_max_check_linear_fill(v_max=10.0, v_min=-10.0, dt_max=300)
    assert round(abs(ts_qac.value(3) - 3.0), 7) == 0
    ts_qac = ts_src.min_max_check_linear_fill(v_max=10.0, v_min=0.0, dt_max=300)
    assert round(abs(ts_qac.value(1) - 1.5), 7) == 0  # -1 out, replaced with linear between
    assert round(abs(ts_qac.value(3) - 3.0), 7) == 0
    ts_qac = ts_src.min_max_check_linear_fill(v_max=10.0, v_min=0.0, dt_max=0)
    assert not math.isfinite(ts_qac.value(3))  # should give nan, not allowed to fill in
    assert not math.isfinite(ts_qac.value(1))  # should give nan, not allowed to fill in


def test_min_max_check_linear_fill_w_max_gap():
    u = TimeSeriesUtilCase()
    """ there is a gap larger than allowed, so we would like to see nan there """
    ta = TimeAxis(UtcTimeVector([0, 1, 3, 6, 7]), 8.0)  # mind the gap 1..3..6, should give nan
    ts_src = TimeSeries(ta, values=DoubleVector([0.0, 1.0, 3.0, 6.0, 7.0]), point_fx=POINT_INSTANT_VALUE)
    ts_qac = ts_src.min_max_check_linear_fill(v_max=10.0, v_min=-10.0, dt_max=1)
    ts_v = ts_qac.values.to_numpy()
    assert len(ts_qac) == 5 + 2
    assert round(abs(ts_qac.value(0) - 0.0), 7) == 0
    assert round(abs(ts_qac.value(1) - 1.0), 7) == 0
    assert not math.isfinite(ts_qac(ts_qac.time(2)))
    assert not math.isfinite(ts_qac(ts_qac.time(3)))
    assert not math.isfinite(ts_qac(ts_qac.time(4)))
    assert not math.isfinite(ts_qac.value(2))
    assert not math.isfinite(ts_qac.value(3))
    assert not math.isfinite(ts_qac.value(4))

    assert round(abs(ts_qac.value(5) - 6.0), 7) == 0
    assert round(abs(ts_qac.value(6) - 7.0), 7) == 0

    assert round(abs(ts_v[0] - 0.0), 7) == 0
    assert round(abs(ts_v[1] - 1.0), 7) == 0
    assert not math.isfinite(ts_v[2])
    assert not math.isfinite(ts_v[3])
    assert not math.isfinite(ts_v[4])
    assert round(abs(ts_v[5] - 6.0), 7) == 0
    assert round(abs(ts_v[6] - 7.0), 7) == 0


def test_min_max_check_linear_fill_w_max_gap():
    u = TimeSeriesUtilCase()
    """ there is a gap larger than allowed, so we would like to see nan there, after dt-step.. """
    ta = TimeAxis(UtcTimeVector([0, 1, 3, 6, 7]), 8.0)  # mind the gap 1..3..6, should give nan afer 1 s
    ts_src = TimeSeries(ta, values=DoubleVector([0.0, 1.0, 3.0, 6.0, 7.0]), point_fx=POINT_AVERAGE_VALUE)
    ts_qac = ts_src.min_max_check_linear_fill(v_max=10.0, v_min=-10.0, dt_max=1)
    ts_v = ts_qac.values.to_numpy()
    assert len(ts_qac) == 5 + 2
    assert ts_qac.time(2) == time(2)
    assert ts_qac.time(3) == time(3)
    assert ts_qac.time(4) == time(4)
    assert ts_qac.time(5) == time(6)
    assert ts_qac.time(6) == time(7)
    assert round(abs(ts_qac.value(0) - 0.0), 7) == 0

    assert not math.isfinite(ts_qac(ts_qac.time(2)))
    assert math.isfinite(ts_qac(ts_qac.time(3)))
    assert not math.isfinite(ts_qac(ts_qac.time(4)))
    assert not math.isfinite(ts_qac.value(2))
    assert math.isfinite(ts_qac.value(3))
    assert not math.isfinite(ts_qac.value(4))
    assert round(abs(ts_qac.value(3) - 3.0), 7) == 0
    assert round(abs(ts_qac.value(5) - 6.0), 7) == 0
    assert round(abs(ts_qac.value(6) - 7.0), 7) == 0

    assert round(abs(ts_v[0] - 0.0), 7) == 0
    assert round(abs(ts_v[1] - 1.0), 7) == 0
    assert not math.isfinite(ts_v[2])
    assert math.isfinite(ts_v[3])
    assert not math.isfinite(ts_v[4])
    assert round(abs(ts_v[3] - 3.0), 7) == 0
    assert round(abs(ts_v[5] - 6.0), 7) == 0
    assert round(abs(ts_v[6] - 7.0), 7) == 0


def test_min_max_check_ts_fill():
    u = TimeSeriesUtilCase()
    ta = TimeAxis(0, 1, 5)
    ts_src = TimeSeries(ta, values=DoubleVector([1.0, -1.0, 2.0, float('nan'), 4.0]), point_fx=POINT_AVERAGE_VALUE)
    cts = TimeSeries(ta, values=DoubleVector([1.0, 1.8, 2.0, 2.0, 4.0]), point_fx=POINT_AVERAGE_VALUE)
    ts_qac = ts_src.min_max_check_ts_fill(v_max=10.0, v_min=-10.0, dt_max=300, cts=cts)
    assert round(abs(ts_qac.value(3) - 2.0), 7) == 0
    ts_qac = ts_src.min_max_check_ts_fill(v_max=10.0, v_min=0.0, dt_max=300, cts=cts)
    assert round(abs(ts_qac.value(1) - 1.8), 7) == 0  # -1 out, replaced with linear between
    assert round(abs(ts_qac.value(3) - 2.0), 7) == 0
    # ref dtss test for serialization testing


def test_qac_parameter():
    u = TimeSeriesUtilCase()
    q = QacParameter()
    assert q is not None
    ta = TimeAxis(0, 1, 5)
    ts_src = TimeSeries(ta, values=DoubleVector([1.0, -1.0, 2.0, float('nan'), 4.0]), point_fx=POINT_AVERAGE_VALUE)
    cts = TimeSeries(ta, values=DoubleVector([1.0, 1.8, 2.0, 2.0, 4.0]), point_fx=POINT_AVERAGE_VALUE)

    ts_qac = ts_src.quality_and_ts_correction(QacParameter(time(300), -10.0, 10.0, time(0), 0.0), cts=cts)
    assert round(abs(ts_qac.value(3) - 2.0), 7) == 0
    ts_qac = ts_src.quality_and_ts_correction(QacParameter(time(300), 0.0, 10.0, time(0), 0.0), cts=cts)
    assert round(abs(ts_qac.value(1) - 1.8), 7) == 0  # -1 out, replaced with linear between
    assert round(abs(ts_qac.value(3) - 2.0), 7) == 0


def test_merge_points():
    u = TimeSeriesUtilCase()
    a = TimeSeries()  # a empty at beginning, we allow that.
    tb = TimeAxis(0, 1, 5)
    b = TimeSeries(tb, values=DoubleVector([1.0, -1.0, 2.0, 3.0, 4.0]), point_fx=POINT_AVERAGE_VALUE)
    a.merge_points(b)  # now a should equal b
    c = TimeSeries(TimeAxis(UtcTimeVector([3, 10, 11]), t_end=12), fill_value=9.0, point_fx=POINT_AVERAGE_VALUE)
    a.merge_points(c)  # now a should have new values for t=3, plus new time-points 11 and 12
    assert len(a) == 7
    assert_array_almost_equal(a.values.to_numpy(), np.array([1.0, -1.0, 2.0, 9.0, 4.0, 9.0, 9.0]))
    assert_array_almost_equal(a.time_axis.time_points, np.array([0, 1, 2, 3, 4, 10, 11, 12]))
    xa = TimeSeries("some_unbound_ts")
    xa.merge_points(a)  # now it should be bound, and it's values are from a
    assert len(xa) == 7
    assert_array_almost_equal(xa.values.to_numpy(), np.array([1.0, -1.0, 2.0, 9.0, 4.0, 9.0, 9.0]))
    assert_array_almost_equal(xa.time_axis.time_points, np.array([0, 1, 2, 3, 4, 10, 11, 12]))
    d = TimeSeries(TimeAxis(UtcTimeVector([3, 10, 11]), t_end=12), fill_value=10.0, point_fx=POINT_AVERAGE_VALUE)
    xa.merge_points(d)  # now that xa is bound, also check we get updated
    assert len(xa) == 7
    assert_array_almost_equal(xa.values.to_numpy(), np.array([1.0, -1.0, 2.0, 10.0, 4.0, 10.0, 10.0]))
    assert_array_almost_equal(xa.time_axis.time_points, np.array([0, 1, 2, 3, 4, 10, 11, 12]))


def test_ts_bool():
    u = TimeSeriesUtilCase()
    a = TimeSeries()
    assert not a  # ok empty
    try:
        b = TimeSeries("something")
        x = bool(b)
        assert False, "Expected exception here"
    except RuntimeError as re:
        pass


def test_time_series_constructor_resolution_order():
    u = TimeSeriesUtilCase()
    ta = TimeAxis(0, 60, 60)

    # time-series can be constructed with a fill value
    ts_fill = TimeSeries(ta, 15., POINT_AVERAGE_VALUE)

    # time-series can be constructed with a double vector
    double_vec = DoubleVector([0.]*len(ta))
    ts_dvec = TimeSeries(ta, double_vec, POINT_AVERAGE_VALUE)

    # time-series can be constructed with a python list with numbers
    number_list = [0.]*len(ta)
    ts_dvec = TimeSeries(ta, number_list, POINT_AVERAGE_VALUE)
