import glob
import os
import platform
import shutil
import subprocess
from os import path

import sys
from setuptools import setup, find_packages

print('Building Shyft Open Source')

# VERSION should be set in a previous build step (ex: TeamCity)

if path.exists('VERSION'):
    VERSION = open('VERSION').read().strip()
    # Create the version.py file
    open('shyft/version.py', 'w').write(f'__version__ = "{VERSION}"\n')
else:
    from shyft.version import __version__

    VERSION = __version__

ext_s: str = '.pyd' if 'Windows' in platform.platform() else '.so'

# Allow build/setup time-series only part of shyft
ts_only: bool = False
ts_only_opt: str = '--ts-only'
if ts_only_opt in sys.argv:
    ts_only = True
    sys.argv.remove(ts_only_opt)  # have to remove it from opts to avoid setup.py complain

ext_names = ['shyft/api/_api' + ext_s,
             'shyft/energy_market/core/_core' + ext_s,
             'shyft/energy_market/ltm/_ltm' + ext_s,
             'shyft/energy_market/stm/_stm' + ext_s,
             'shyft/time_series/_time_series' + ext_s,
             'shyft/api/pt_gs_k/_pt_gs_k' + ext_s,
             'shyft/api/pt_hs_k/_pt_hs_k' + ext_s,
             'shyft/api/pt_hps_k/_pt_hps_k' + ext_s,
             'shyft/api/pt_ss_k/_pt_ss_k' + ext_s,
             'shyft/api/r_pm_gs_k/_r_pm_gs_k' + ext_s,
             'shyft/api/hbv_stack/_hbv_stack' + ext_s] if ts_only else ['shyft/time_series/_time_series' + ext_s]

needs_build_ext = not all([path.exists(ext_name) for ext_name in ext_names])

if needs_build_ext:
    print('One or more extension modules needs build, attempting auto build')
    if "Windows" in platform.platform():
        msbuild_2017 = r'C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\amd64\MSBuild.exe' if 'MSBUILD_2017_PATH' not in os.environ else \
            os.environ['MSBUILD_2017_PATH']
        if path.exists(msbuild_2017):
            msbuild = msbuild_2017
            cmd = [msbuild, '/p:Configuration=Release', '/p:Platform=x64', '/p:PlatformToolset=v141',
                   '/p:WindowsTargetPlatformVersion=10.0.16299.0', '/m']
        else:
            print("Sorry, but this setup only supports ms c++ installed to standard locations")
            print(" you can set MSBUILD_2017_PATH specific to your installation and restart.")
            exit()

        if '--rebuild' in sys.argv:
            cmd.append('/t:Rebuild')
            sys.argv.remove('--rebuild')

        p = subprocess.Popen(cmd,
                             universal_newlines=True,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT)

        for line in iter(p.stdout.readline, ''):
            print(line.rstrip())

        p.wait()
        if p.returncode != 0:
            print('\nMSBuild FAILED.')
            exit()

    elif "Linux" in platform.platform():
        try:
            # For Linux, use the cmake approach for compiling the extensions
            print(subprocess.check_output("sh build_api_cmake.sh", shell=True))
        except:
            print("Problems compiling shyft, try building with the build_api.sh "
                  "or build_api_cmake.sh (Linux only) script manually...")
            exit()
    else:
        print("Only windows and Linux supported")
        exit()
else:
    print('Extension modules are already built in place')

# Copy libraries needed to run Shyft
if "Windows" in platform.platform():
    lib_dir = os.getenv('SHYFT_DEPENDENCIES', '../shyft_dependencies')
    boost_dll = path.join(lib_dir, 'lib', '*.dll')
    files = glob.glob(boost_dll)
    files = [f for f in files if '-gd-' not in path.basename(f)]
    dest_dir = path.join(path.dirname(path.realpath(__file__)), 'shyft', 'lib')
    if not path.isdir(dest_dir):
        os.mkdir(dest_dir)
    for f in files:
        shutil.copy2(f, path.join(dest_dir, path.basename(f)))

if not ts_only:
    setup(
        name='shyft',
        version=VERSION,
        author='shyft-os',
        author_email='sigbjorn.helset@gmail.com',
        url='https://gitlab.com/shyft-os/shyft',
        description='An OpenSource toolbox providing tools for energy-market, hydrological forecasting and advanced time-series',
        license='LGPL v3',
        packages=[f'shyft.{p}' for p in find_packages('shyft')],
        package_data={'': ['*.so', '*.pyd', '../lib/*.dll', '../lib/*.s*']},
        entry_points={},
        requires=["numpy"],
        install_requires=["numpy"],
        tests_require=['pytest'],
        zip_safe=False,
        extras_require={
            'repositories': ['netcdf4', 'shapely', 'pyyaml', 'pyproj'],
            'notebooks': ['jupyter']
        }
    )
else:
    setup(
        name='shyft.time_series',
        version=VERSION,
        author='shyft-os',
        author_email='sigbjorn.helset@gmail.com',
        url='https://gitlab.com/shyft-os/shyft',
        description='An OpenSource toolbox providing tools for advanced time-series',
        license='LGPL v3',
        packages=['shyft.time_series'],
        package_data={'shyft.time_series': ['*.so', '*.pyd', '../lib/*.dll', '../lib/*.s*']},
        entry_points={},
        requires=["numpy"],
        install_requires=["numpy"],
        tests_require=['pytest'],
        zip_safe=False
    )
